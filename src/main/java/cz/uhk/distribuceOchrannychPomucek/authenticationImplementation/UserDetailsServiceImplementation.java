package cz.uhk.distribuceOchrannychPomucek.authenticationImplementation;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Implements UserDetailsService of Spring Security. Gets username of an active user.
 */
public class UserDetailsServiceImplementation implements UserDetailsService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final AppUser user = appUserRepository.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("Neplatné jméno uživatele!");
        }
        return new UserDetailsImplementation(user);
    }
}
