package cz.uhk.distribuceOchrannychPomucek.config;

import cz.uhk.distribuceOchrannychPomucek.authenticationImplementation.UserDetailsServiceImplementation;
import cz.uhk.distribuceOchrannychPomucek.failureHandler.LoginFailureHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Configures the features of the Spring security framework.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig {
    @Autowired
    private LoginFailureHandler loginFailureHandler;

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImplementation();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        final DaoAuthenticationProvider daoAuthProvider = new DaoAuthenticationProvider();
        daoAuthProvider.setUserDetailsService(userDetailsService());
        daoAuthProvider.setPasswordEncoder(passwordEncoder());
        return daoAuthProvider;
    }

    @Bean
    public SecurityFilterChain filterChain(final HttpSecurity http) throws Exception {
        http.formLogin().loginPage("/").usernameParameter("login").passwordParameter("password").successForwardUrl("/loginSuccess").failureForwardUrl("/loginFailure").failureHandler(loginFailureHandler).loginProcessingUrl("/doLogin").and().logout().logoutUrl("/logout").logoutSuccessUrl("/logoutSuccess").logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"));
        return http.build();
    }
}
