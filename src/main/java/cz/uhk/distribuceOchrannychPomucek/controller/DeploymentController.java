package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.UserInfo;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.RightRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.UserInfoRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Controller containing actions which should be used only during deployment of the application, for initializing basic stuff, like global admin account.
 */
@Controller
public class DeploymentController {

    private final RightRepository rightRepository;
    private final AppUserRepository appUserRepository;
    private final UserInfoRepository userInfoRepository;

    public DeploymentController(final RightRepository rightRepository, final AppUserRepository appUserRepository, final UserInfoRepository userInfoRepository) {
        this.rightRepository = rightRepository;
        this.appUserRepository = appUserRepository;
        this.userInfoRepository = userInfoRepository;
    }

    /**
     * Examines if an account with the global admin right already exists. If not, opens form for the user to create a new global admin account.
     *
     * @param redirect - Saves redirect message.
     * @return - Redirects user to the index action if a global admin account already exists. If not, then returns the create global admin account form.
     */
    @GetMapping("/createGlobalAdminAccount")
    public ModelAndView createGlobalAdminAccount(final RedirectAttributes redirect) {
        //Verify by database request if an global admin account already exists.
        if (appUserRepository.existsByRight(rightRepository.findById(1))) {
            redirect.addFlashAttribute("error", "Účet globálního administrátora už existuje!");
            return new ModelAndView("redirect:/");
        }
        final UserInfo userInfo = new UserInfo();
        return new ModelAndView("createGlobalAdminAccount").addObject(userInfo);
    }

    /**
     * Saves the new global admin account, if there are no validation and other errors and no global admin account already exists.
     *
     * @param userInfo      - Information about the user. Also contains the AppUser class reference with the required basic information.
     * @param result        - Result of the automated validation.
     * @param passwordAgain - Password inputted second time.
     * @param redirect      - Used to add redirect messages, if this instance is used.
     * @return - Either the global admin account creation form if there is any validation error. Or the index website either in case of successful or failed account creation.
     */
    @PostMapping("/saveGlobalAdminAccount")
    public ModelAndView saveGlobalAdminAccount(@Valid final UserInfo userInfo, final BindingResult result, @RequestParam final String passwordAgain, final RedirectAttributes redirect) {
        //Verify by database request if an global admin account already exists.
        if (appUserRepository.existsByRight(rightRepository.findById(1))) {
            redirect.addFlashAttribute("error", "Účet globálního administrátora už existuje!");
            return new ModelAndView("redirect:/");
        }
        if (result.hasErrors()) {
            return new ModelAndView("createGlobalAdminAccount");
        }
        if (appUserRepository.existsByLogin(userInfo.getUser().getLogin())) {
            redirect.addFlashAttribute("error", "Přihlašovací uživatelké jméno je už použito. Zadejte prosím jiné.");
            return new ModelAndView("createGlobalAdminAccount").addObject(userInfo);
        }
        if (userInfoRepository.existsByEmail(userInfo.getEmail())) {
            redirect.addFlashAttribute("error", "Daný email je už použit jiným uživatelem. Zadejte prosím jiný.");
            return new ModelAndView("createGlobalAdminAccount").addObject(userInfo);
        }
        if (passwordAgain == null || passwordAgain.isBlank()) {
            redirect.addFlashAttribute("error", "Prosím vyplňte položku formuláře heslo znova.");
            return new ModelAndView("createGlobalAdminAccount").addObject(userInfo);
        }
        if (!userInfo.getUser().getPassword().equals(passwordAgain)) {
            redirect.addFlashAttribute("error", "Zadal jste heslo pokaždé jinak. Prosím zadejte ho poprvé a poté znova stejně.");
            return new ModelAndView("createGlobalAdminAccount").addObject(userInfo);
        }
        final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        final AppUser newUser = new AppUser(userInfo.getUser().getLogin(), encoder.encode(userInfo.getUser().getPassword()), rightRepository.findById(1), true);
        try {
            final UserInfo newUserInfo = new UserInfo(userInfo.getName(), userInfo.getSurname(), userInfo.getEmail(), appUserRepository.save(newUser));
            userInfoRepository.save(newUserInfo);
        } catch (final Exception e) {
            redirect.addFlashAttribute("error", "Během vytváření účtu globálního administrátora došlo k neočekávané chybě.");
            e.printStackTrace();
            return new ModelAndView("redirect:/");
        }
        redirect.addFlashAttribute("success", "Účet globálního administrátora byl úspěšně vytvořen.");
        return new ModelAndView("redirect:/");

    }
}
