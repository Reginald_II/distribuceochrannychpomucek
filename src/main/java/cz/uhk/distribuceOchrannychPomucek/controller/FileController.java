package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentStatusChangeEvent;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.ProtectiveEquipmentStatusChangeEventRepository;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Controller working with files being sent to the users.
 */
@RestController
public class FileController {

    private final AppUserRepository appUserRepository;
    private final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository;

    public FileController(final AppUserRepository appUserRepository, final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository) {
        this.appUserRepository = appUserRepository;
        this.protectiveEquipmentStatusChangeEventRepository = protectiveEquipmentStatusChangeEventRepository;
    }

    /**
     * Converts region protective equipment status history into .csv files and sends them to the user for download.
     *
     * @param authentication                                    - Contains login name of the logged user sending the request.
     * @param response                                          - Contains output stream, which is required by the PrintWriter class.
     * @param protectiveEquipmentRequestStatusChangeEventTypeId - Id of the type of the history events. Optional.
     * @param dateFrom                                          - Date from which the history is going to be gained. Optional.
     * @param dateTo                                            - Date to which is the history going to be gained. Optional.
     * @throws IOException    - Exception potentially caused by the output stream during getting it from the response.
     * @throws ParseException - Exception potentially caused by errors during parsing date.
     */
    @PostMapping("/exportRegionProtectiveEquipmentStatusChangeEvents")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public void exportRegionProtectiveEquipmentStatusChangeEvents(final Authentication authentication, final HttpServletResponse response, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentRequestStatusChangeEventTypeId, @RequestParam(required = false) final String dateFrom, @RequestParam(required = false) final String dateTo) throws IOException, ParseException {
        final AppUser appUser = appUserRepository.findByLogin(authentication.getName());
        final Region userRegion = appUser.getRegion();
        final List<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEvents;
        //Saving check results whenever parameters were inputted or not for avoiding duplicate ifs.
        final boolean isAll = protectiveEquipmentRequestStatusChangeEventTypeId == 0;
        final boolean isDateBefore = (dateTo != null && !dateTo.isEmpty());
        final boolean isDateAfter = (dateFrom != null && !dateFrom.isEmpty());
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String sortByVariable = "timeOfEvent";
        //Gets the history from the database depending on the parameters.
        if (isAll) {
            if (!isDateBefore && !isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByRegion(userRegion, Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore && isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByRegionAndTimeOfEventAfterAndTimeOfEventBefore(userRegion, simpleDateFormat.parse(dateFrom), simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByRegionAndTimeOfEventBefore(userRegion, simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByRegionAndTimeOfEventAfter(userRegion, simpleDateFormat.parse(dateFrom), Sort.by(Sort.Direction.DESC, sortByVariable));
            } //If none of the conditions passes, gets all without date filtering.
            else {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByRegion(userRegion, Sort.by(Sort.Direction.DESC, sortByVariable));
            }
        } else {
            if (!isDateBefore && !isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByRegionAndProtectiveEquipmentStatusChangeEventTypeId(userRegion, protectiveEquipmentRequestStatusChangeEventTypeId, Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore && isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByRegionAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfterAndTimeOfEventBefore(userRegion, protectiveEquipmentRequestStatusChangeEventTypeId, simpleDateFormat.parse(dateFrom), simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByRegionAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventBefore(userRegion, protectiveEquipmentRequestStatusChangeEventTypeId, simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByRegionAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfter(userRegion, protectiveEquipmentRequestStatusChangeEventTypeId, simpleDateFormat.parse(dateFrom), Sort.by(Sort.Direction.DESC, sortByVariable));
            } //If none of the conditions passes, gets all without date filtering.
            else {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByRegionAndProtectiveEquipmentStatusChangeEventTypeId(userRegion, protectiveEquipmentRequestStatusChangeEventTypeId, Sort.by(Sort.Direction.DESC, sortByVariable));
            }
        }
        response.setContentType("text/csv");
        response.setCharacterEncoding("utf-8");
        final PrintWriter printWriter = new PrintWriter(response.getOutputStream());
        final boolean isUserAdmin = (appUser.getRight().getId() == 2);
        //Writes head line of the file.
        if (isUserAdmin) {
            printWriter.write("Id;Datum;Čas;Název;Odůvodnění;Uživatel provádějící akci;Číslo dokladu;Původní množství;Změnené množství;Nové množství");
        } else {
            printWriter.write("Id;Datum;Čas;Název;Odůvodnění;Číslo dokladu;Původní množství;Změnené množství;Nové množství");
        }
        printWriter.println();
        //Writes the actual history events.
        for (final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent : protectiveEquipmentStatusChangeEvents) {
            if (isUserAdmin) {
                printWriter.write(protectiveEquipmentStatusChangeEvent.toStringAdmin());
            } else {
                printWriter.write(protectiveEquipmentStatusChangeEvent.toString());
            }
            printWriter.println();
        }
        printWriter.close();
    }

    /**
     * Converts region protective equipment status history into .csv files and sends them to the user for download.
     *
     * @param authentication                                    - Contains login name of the logged user sending the request.
     * @param response                                          - Contains output stream, which is required by the PrintWriter class.
     * @param protectiveEquipmentRequestStatusChangeEventTypeId - Id of the type of the history events. Optional.
     * @param dateFrom                                          - Date from which the history is going to be gained. Optional.
     * @param dateTo                                            - Date to which is the history going to be gained. Optional.
     * @throws IOException    - Exception potentially caused by the output stream during getting it from the response.
     * @throws ParseException - Exception potentially caused by errors during parsing date.
     */
    @PostMapping("/exportOrganizationProtectiveEquipmentStatusChangeEvents")
    @Secured({"ROLE_OrganizationAdmin", "ROLE_OrganizationUser"})
    public void exportOrganizationProtectiveEquipmentStatusChangeEvents(final Authentication authentication, final HttpServletResponse response, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentRequestStatusChangeEventTypeId, @RequestParam(required = false) final String dateFrom, @RequestParam(required = false) final String dateTo) throws IOException, ParseException {
        final AppUser appUser = appUserRepository.findByLogin(authentication.getName());
        final Organization userOrganization = appUser.getOrganization();
        final List<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEvents;
        //Saving check results whenever parameters were inputted or not for avoiding duplicate ifs.
        final boolean isAll = protectiveEquipmentRequestStatusChangeEventTypeId == 0;
        final boolean isDateBefore = (dateTo != null && !dateTo.isEmpty());
        final boolean isDateAfter = (dateFrom != null && !dateFrom.isEmpty());
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String sortByVariable = "timeOfEvent";
        //Gets the history from the database depending on the parameters.
        if (isAll) {
            if (!isDateBefore && !isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByOrganization(userOrganization, Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore && isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByOrganizationAndTimeOfEventAfterAndTimeOfEventBefore(userOrganization, simpleDateFormat.parse(dateFrom), simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByOrganizationAndTimeOfEventBefore(userOrganization, simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByOrganizationAndTimeOfEventAfter(userOrganization, simpleDateFormat.parse(dateFrom), Sort.by(Sort.Direction.DESC, sortByVariable));
            } //If none of the conditions passes, gets all without date filtering.
            else {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findAllByOrganization(userOrganization, Sort.by(Sort.Direction.DESC, sortByVariable));
            }
        } else {
            if (!isDateBefore && !isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeId(userOrganization, protectiveEquipmentRequestStatusChangeEventTypeId, Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore && isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfterAndTimeOfEventBefore(userOrganization, protectiveEquipmentRequestStatusChangeEventTypeId, simpleDateFormat.parse(dateFrom), simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateBefore) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventBefore(userOrganization, protectiveEquipmentRequestStatusChangeEventTypeId, simpleDateFormat.parse(dateTo), Sort.by(Sort.Direction.DESC, sortByVariable));
            } else if (isDateAfter) {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfter(userOrganization, protectiveEquipmentRequestStatusChangeEventTypeId, simpleDateFormat.parse(dateFrom), Sort.by(Sort.Direction.DESC, sortByVariable));
            } //If none of the conditions passes, gets all without date filtering.
            else {
                protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventRepository.findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeId(userOrganization, protectiveEquipmentRequestStatusChangeEventTypeId, Sort.by(Sort.Direction.DESC, sortByVariable));
            }
        }
        response.setContentType("text/csv");
        response.setCharacterEncoding("utf-8");
        final PrintWriter printWriter = new PrintWriter(response.getOutputStream());
        final boolean isUserAdmin = (appUser.getRight().getId() == 2);
        //Writes head line of the file.
        if (isUserAdmin) {
            printWriter.write("Id;Datum;Čas;Název;Odůvodnění;Uživatel provádějící akci;Číslo dokladu;Původní množství;Změnené množství;Nové množství");
        } else {
            printWriter.write("Id;Datum;Čas;Název;Odůvodnění;Číslo dokladu;Původní množství;Změnené množství;Nové množství");
        }
        printWriter.println();
        //Writes the actual history events.
        for (final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent : protectiveEquipmentStatusChangeEvents) {
            if (isUserAdmin) {
                printWriter.write(protectiveEquipmentStatusChangeEvent.toStringAdmin());
            } else {
                printWriter.write(protectiveEquipmentStatusChangeEvent.toString());
            }
            printWriter.println();
        }
        printWriter.close();
    }
}
