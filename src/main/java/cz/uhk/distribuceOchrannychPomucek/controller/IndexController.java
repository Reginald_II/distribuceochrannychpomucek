package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Contains the index and login action.
 */
@Controller
public class IndexController {

    private final AppUserRepository appUserRepository;

    public IndexController(final AppUserRepository repository) {
        this.appUserRepository = repository;
    }

    /**
     * Index action.
     *
     * @return - Either index website if the user isn't logged in. Or if he is already authenticated, redirects him to the default page of his role.
     */
    @GetMapping("/")
    public String index() {
        //Checks if the user is authenticated under the default anonymous role by the framework.
        if (SecurityContextHolder.getContext().getAuthentication() == null || SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")) {
            return "index";
        }
        final AppUser user = appUserRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        final int rightId = user.getRight().getId();
        if (rightId == 1) {
            return "redirect:/showProtectiveEquipments";
        }
        if (rightId == 2 || rightId == 3) {
            return "redirect:/showRegion";
        }
        if (rightId > 3) {
            return "redirect:/showOrganization";
        }
        return "index";
    }

    /**
     * Gets called when user successfully logs in.
     *
     * @return - Redirects user to the default page of his role. Or back to the index action, if for some reason the user's role wouldn't fit any checks.
     */
    @PostMapping("/loginSuccess")
    public RedirectView loginSuccess() {
        final AppUser user = appUserRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user.getRight().getId() == 1) {
            return new RedirectView("/showProtectiveEquipments", true);
        }
        if (user.getRight().getId() == 2 || user.getRight().getId() == 3) {
            return new RedirectView("/showRegion", true);
        }
        if (user.getRight().getId() > 3) {
            return new RedirectView("/showOrganization", true);
        }
        return new RedirectView("/", true);
    }

    /**
     * Gets called when the user's login attempt fails.
     *
     * @param redirect       - Used to add redirect messages.
     * @param loginErrorCode - Login error code received from the Login failure handler class.
     * @param appUserId      - User's id. Optional.
     * @return - Redirects user back to the index action with an error message explaining the reason of the failure.
     */
    @GetMapping("/loginFailure")
    public RedirectView loginFailure(final RedirectAttributes redirect, @RequestParam final int loginErrorCode, @RequestParam(required = false, defaultValue = "0") final int appUserId) {
        if (loginErrorCode == 1) {
            redirect.addFlashAttribute("error", "Zadal jste špatně jméno nebo heslo. Zadejte vaše přihlašovací údaje prosím znova.");
        }
        if (loginErrorCode == 2) {
            if (appUserId > 0) {
                final AppUser user = appUserRepository.findById(appUserId);
                redirect.addFlashAttribute("error", "Váš účet je zablokován z důvodu.: " + user.getAppUserDisabledInfo().getReason() + ". Pro více informací prosím kontaktujte administrátora vašeho kraje nebo vaší organizace.");
            } else {
                redirect.addFlashAttribute("error", "Váš účet je zablokován.Pro více informací prosím kontaktujte administrátora vašeho kraje nebo vaší organizace.");
            }
        }
        return new RedirectView("/", true);
    }

    /**
     * Gets called on the success of the logout of the user.
     *
     * @param redirect - Used to add redirect messages.
     * @return - Redirects user back to the index action with the logout success message.
     */
    @GetMapping("/logoutSuccess")
    public RedirectView logoutSuccess(final RedirectAttributes redirect) {
        redirect.addFlashAttribute("success", "Odhlášení proběhlo úspěšně.");
        return new RedirectView("/", true);
    }
}
