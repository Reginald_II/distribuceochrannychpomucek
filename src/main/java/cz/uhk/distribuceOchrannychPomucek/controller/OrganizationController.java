package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.City;
import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.CityRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.OrganizationRepository;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Contains actions focused on working with organizations.
 */
@Controller
@Secured({"ROLE_RegionAdmin", "ROLE_OrganizationAdmin"})
public class OrganizationController {
    private final OrganizationRepository organizationRepository;
    private final CityRepository cityRepository;
    private final AppUserRepository appUserRepository;

    public OrganizationController(final OrganizationRepository organizationRepository, final CityRepository cityRepository, final AppUserRepository appUserRepository) {
        this.organizationRepository = organizationRepository;
        this.cityRepository = cityRepository;
        this.appUserRepository = appUserRepository;
    }

    /**
     * Shows information of the user's organization.
     *
     * @param authentication - Authentication information of the user.
     * @return - View with the information about the user's organization.
     */
    @GetMapping("/showOrganization")
    @Secured({"ROLE_OrganizationAdmin", "ROLE_OrganizationUser"})
    public ModelAndView showOrganization(final Authentication authentication) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        return new ModelAndView("showOrganization").addObject("organization", user.getOrganization());
    }

    /**
     * Opens form of creating new organization in a region.
     *
     * @return - View with the create organization form.
     */
    @GetMapping("/createOrganization")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView createOrganization() {
        final Organization organization = new Organization();
        return new ModelAndView("createOrganization").addObject(organization);
    }

    /**
     * Saves the new organization.
     *
     * @param organization   - Instance of the new organization, which is going to be saved.
     * @param result         - Result of the automated validation.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Contains authentication information of the user.
     * @return - Either create organization form if there is any validation error. Or redirects user to the action of showing organizations of his region in case of successful saving of the new organization.
     */
    @PostMapping("/saveCreatedOrganization")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView saveCreatedOrganization(@Valid final Organization organization, final BindingResult result, final RedirectAttributes redirect, final Authentication authentication) {
        if (result.hasErrors()) {
            return new ModelAndView("createOrganization").addObject(organization);
        }
        //Checks if the city of the organization already exists by its psc. If not, saves it to the database.
        final City city = organization.getCity();
        if (!cityRepository.existsByPsc(city.getPsc())) {
            cityRepository.save(city);
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Organization newOrganization = new Organization(organization.getName(), organization.getStreet(), city, user.getRegion());
        organizationRepository.save(newOrganization);
        redirect.addFlashAttribute("success", "Organizace " + newOrganization.getName() + " byla úspěšně vytvořena.");
        return new ModelAndView("redirect:/showRegionOrganizations");
    }

    /**
     * Gets the selected organization of the region for the region administrator to change its information.
     *
     * @param organizationId - Id of the organization.
     * @param redirect       -    Redirect attribues, where the error message in case of it is put.
     * @return - View of changing the region organization. Or redirects to the action showing region organizations in case of an error.
     */
    @GetMapping("/changeRegionOrganization")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView changeRegionOrganization(@RequestParam final int organizationId, final RedirectAttributes redirect, final Authentication authentication) {
        if (organizationId < 1) {
            redirect.addFlashAttribute("error", "Id organizace je neplatné.");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        final Organization organization = organizationRepository.findById(organizationId);
        if (organization == null) {
            redirect.addFlashAttribute("error", "Organizace, kterou chcete změnit, nebyla v databázi nalezena.");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getRegion().getId() != organization.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete změnit organizaci z cizího kraje.");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        return new ModelAndView("changeRegionOrganization").addObject(organization);
    }

    /**
     * Saves the changes of selected organization of the region made by the region admin.
     *
     * @param organization   -  Changed organization to be saved.
     * @param result         -      Result of the validation.
     * @param redirect       -     Redirect attributes used to store success and error messages.
     * @param authentication - Authentication token of the user, used to get his login.
     * @return - Either view of changing the region organization in case of validation error and redirect to the action of showing region organizations in case of the successful saving of the organization changes or the other errors.
     */
    @PostMapping("/saveChangedRegionOrganization")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView saveChangedRegionOrganization(@Valid final Organization organization, final BindingResult result, final RedirectAttributes redirect, final Authentication authentication) {
        if (result.hasErrors()) {
            return new ModelAndView("changeRegionOrganization").addObject(organization);
        }
        final City city = organization.getCity();
        if (!cityRepository.existsByPsc(city.getPsc())) {
            cityRepository.save(city);
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (organization.getRegion() == null || user.getRegion().getId() != organization.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se změnit údaje organizace cízáho kraje!");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        organizationRepository.save(organization);
        redirect.addFlashAttribute("success", "Organizace " + organization.getName() + " byla úspěšně vytvořena.");
        return new ModelAndView("redirect:/showRegionOrganizations");
    }

    /**
     * Opens form of changing user's organization.
     *
     * @param authentication - Contains authentication information of the user.
     * @return - The form for changing user's organization.
     */
    @GetMapping("/changeOrganization")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView changeOrganization(final Authentication authentication) {
        final Organization organization = appUserRepository.findByLogin(authentication.getName()).getOrganization();
        return new ModelAndView("changeOrganization").addObject(organization);
    }

    /**
     * Saves the changed user's organization.
     *
     * @param organization   - Instance of the changed organization, which is going to be used to update the existing row of the organization in the database.
     * @param result         - Result of the automated validation.
     * @param authentication - Contains authentication information of the user.
     * @param redirect       - Used to save redirect messages.
     * @return - Either change organization form in case of any validation error. Or in case of successful update redirects user to the action of showing his organization.
     */
    @PostMapping("/saveChangedOrganization")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView saveChangedOrganization(@Valid final Organization organization, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeOrganization").addObject(organization);
        }
        final City city = organization.getCity();
        if (!cityRepository.existsByPsc(city.getPsc())) {
            cityRepository.save(city);
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Organization changedOrganization = user.getOrganization();
        changedOrganization.updateOrganizationAttributes(organization);
        organizationRepository.save(changedOrganization);
        redirect.addFlashAttribute("success", "Informace o vaší organizaci byly úspěšně změněny.");
        return new ModelAndView("redirect:/showOrganization");
    }
}
