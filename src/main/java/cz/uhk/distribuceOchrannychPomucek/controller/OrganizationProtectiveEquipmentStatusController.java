package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.*;
import cz.uhk.distribuceOchrannychPomucek.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Contains actions for working with the organization protective equipment statuses.
 */
@Controller
@Secured({"ROLE_OrganizationAdmin", "ROLE_OrganizationUser"})
public class OrganizationProtectiveEquipmentStatusController {
    private final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository;
    private final ProtectiveEquipmentRepository protectiveEquipmentRepository;
    private final AppUserRepository appUserRepository;
    private final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository;
    private final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository;

    public OrganizationProtectiveEquipmentStatusController(final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository, final ProtectiveEquipmentRepository protectiveEquipmentRepository, final AppUserRepository appUserRepository, final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository, final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository) {
        this.protectiveEquipmentStatusRepository = protectiveEquipmentStatusRepository;
        this.protectiveEquipmentRepository = protectiveEquipmentRepository;
        this.appUserRepository = appUserRepository;
        this.protectiveEquipmentStatusChangeEventRepository = protectiveEquipmentStatusChangeEventRepository;
        this.protectiveEquipmentStatusChangeEventTypeRepository = protectiveEquipmentStatusChangeEventTypeRepository;
    }

    /**
     * Shows the information about the organization protective equipments.
     *
     * @param authentication          - Contains authentication info of the user.
     * @param protectiveEquipmentName - Used to filter the protective equipments by name. Optional parameter.
     * @param currentPage             - Used for the paging. Starts at 0.
     * @return Returns view showing paged protective equipments of an organization.
     */
    @GetMapping("/showOrganizationProtectiveEquipments")
    public ModelAndView showOrganizationProtectiveEquipments(final Authentication authentication, @RequestParam(required = false) final String protectiveEquipmentName, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final int protectiveEquipmentStatusesAmount = protectiveEquipmentStatusRepository.countAllByOrganization(user.getOrganization());
        //Checks if the protective equipment statuses exist. If not, creates the default status of each protective equipment in case that they weren't initialized already.
        if (protectiveEquipmentStatusesAmount < 1) {
            final List<ProtectiveEquipment> protectiveEquipments = protectiveEquipmentRepository.findAll();
            final List<ProtectiveEquipmentStatus> newProtectiveEquipmentStatuses = new ArrayList<>(protectiveEquipments.size());
            for (final ProtectiveEquipment protectiveEquipment : protectiveEquipments) {
                final ProtectiveEquipmentStatus protectiveEquipmentStatus = new ProtectiveEquipmentStatus(protectiveEquipment, user.getOrganization());
                newProtectiveEquipmentStatuses.add(protectiveEquipmentStatus);
            }
            protectiveEquipmentStatusRepository.saveAll(newProtectiveEquipmentStatuses);

        }
        //Checks if there new protective equipments. If yes, adds them to the protective equipment statuses.
        if (protectiveEquipmentStatusesAmount < protectiveEquipmentRepository.count()) {
            final List<ProtectiveEquipmentStatus> protectiveEquipmentStatuses = protectiveEquipmentStatusRepository.findByOrganization(user.getOrganization());
            final List<Integer> existingProtectiveEquipmentIds = new ArrayList<>(protectiveEquipmentStatuses.size());
            for (final ProtectiveEquipmentStatus protectiveEquipmentStatus : protectiveEquipmentStatuses) {
                existingProtectiveEquipmentIds.add(protectiveEquipmentStatus.getProtectiveEquipment().getId());
            }
            final List<ProtectiveEquipment> protectiveEquipments = protectiveEquipmentRepository.findAllByIdIsNotIn(existingProtectiveEquipmentIds);
            for (final ProtectiveEquipment protectiveEquipment : protectiveEquipments) {
                protectiveEquipmentStatuses.add(new ProtectiveEquipmentStatus(protectiveEquipment, user.getOrganization()));
            }
            protectiveEquipmentStatusRepository.saveAll(protectiveEquipmentStatuses);
        }
        final Page<ProtectiveEquipmentStatus> protectiveEquipmentStatusPage;
        final int numberOfPages;
        final int itemsAmountInPage = 5;
        if (protectiveEquipmentName == null || protectiveEquipmentName.isBlank()) {
            protectiveEquipmentStatusPage = protectiveEquipmentStatusRepository.findAllByOrganization(user.getOrganization(), PageRequest.of(currentPage, itemsAmountInPage, Sort.by(Sort.Direction.ASC, "protectiveEquipment.name")));

        } else {
            protectiveEquipmentStatusPage = protectiveEquipmentStatusRepository.findAllByOrganizationAndProtectiveEquipmentNameContaining(user.getOrganization(), protectiveEquipmentName, PageRequest.of(currentPage, itemsAmountInPage, Sort.by(Sort.Direction.ASC, "protectiveEquipment.name")));
        }
        numberOfPages = protectiveEquipmentStatusPage.getTotalPages();
        final List<ProtectiveEquipmentStatus> protectiveEquipmentStatuses = protectiveEquipmentStatusPage.getContent();
        //Creates a field counting pages, used in the view.
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        return new ModelAndView("showOrganizationProtectiveEquipments").addObject("protectiveEquipmentStatuses", protectiveEquipmentStatuses).addObject("pages", pages).addObject("currentPage", currentPage).addObject("protectiveEquipmentName", protectiveEquipmentName);
    }

    /**
     * Opens form for changing amount of organization protective equipment amount.
     *
     * @param protectiveEquipmentStatusId - Id of the protective equipment status which amount is going to be changed.
     * @param authentication              - Contains authentication info of the user.
     * @param redirect                    - Used to save redirect messages.
     * @return - Either redirects user back to showing his organization's protective equipments with error message if he tries to work with data which doesn't belong to his organization. Or returns the form for changing the amount.
     */
    @GetMapping("/changeOrganizationProtectiveEquipmentAmount")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView changeOrganizationProtectiveEquipmentAmount(@RequestParam final int protectiveEquipmentStatusId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        //Checks if the protective equipment status belongs to the user's organization.
        if (protectiveEquipmentStatus.getOrganization() == null || protectiveEquipmentStatus.getOrganization().getId() != user.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete získat počet ochranných pomůcek cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        return new ModelAndView("changeOrganizationProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus);
    }

    /**
     * Saves changed amount of an organization protective equipment to the database.
     *
     * @param protectiveEquipmentStatus - Contains the changed amount.
     * @param result                    - Contains the result of the automated validation.
     * @param comment                   - Comment of the user doing the change explaining it.
     * @param authentication            - Authentication information of the user.
     * @param redirect                  - Used to save redirect messages.
     * @return - Either form of changing the amount in case of either automatic or manual validation error. If the validation passes, redirects user to showing organization protective equipments action.
     */
    @PostMapping("/saveChangedOrganizationProtectiveEquipmentAmount")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView saveChangedOrganizationProtectiveEquipmentAmount(@Valid final ProtectiveEquipmentStatus protectiveEquipmentStatus, final BindingResult result, @RequestParam final String comment, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeOrganizationProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus);
        }
        if (comment == null || comment.isBlank()) {
            return new ModelAndView("changeOrganizationProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus).addObject("error", "Prosím vyplňte odůvodnění změny.");
        }
        if (comment.length() > 500) {
            return new ModelAndView("changeOrganizationProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus).addObject("error", "Odůvodnění smí mít maximálně 500 znaků. Prosím zkraťte ho.");
        }
        final ProtectiveEquipmentStatus changedProtectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatus.getId());
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        //Checks if the protective equipment status belongs to the user's organization.
        if (changedProtectiveEquipmentStatus.getOrganization() == null || changedProtectiveEquipmentStatus.getOrganization().getId() != user.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete získat počet ochranných pomůcek cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        final int originalAmount = changedProtectiveEquipmentStatus.getQuantity();
        final int newAmount = protectiveEquipmentStatus.getQuantity();
        changedProtectiveEquipmentStatus.setQuantity(protectiveEquipmentStatus.getQuantity());
        final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(1);
        final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent(new Date(), originalAmount, newAmount, comment, protectiveEquipmentStatusRepository.save(changedProtectiveEquipmentStatus), user, protectiveEquipmentStatusChangeEventType, user.getOrganization(), 0);
        protectiveEquipmentStatusChangeEventRepository.save(protectiveEquipmentStatusChangeEvent);
        redirect.addFlashAttribute("success", "Množství ochranné pomůcky " + changedProtectiveEquipmentStatus.getProtectiveEquipment().getName() + " bylo úspěšně změněno na.: " + changedProtectiveEquipmentStatus.getQuantity());
        return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
    }

    /**
     * Opens form for using protective equipments of an organization.
     *
     * @param protectiveEquipmentStatusId - Id of the status of the protective equipment, which is going to be used.
     * @param authentication              - Contains authentication information about the user.
     * @param redirect                    - Used to save redirect messages.
     * @return - Redirects user to showing organization protective equipments action in case of error. In case of success, returns the form for using organization protective equipments.
     */
    @GetMapping("/useOrganizationProtectiveEquipments")
    public ModelAndView useOrganizationProtectiveEquipments(@RequestParam final int protectiveEquipmentStatusId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        if (protectiveEquipmentStatus == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        if (protectiveEquipmentStatus.getQuantity() < 1) {
            redirect.addFlashAttribute("error", "Nemůžete vyskladnit ochranné pomůcky, když žádné nemáte.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getOrganization() == null || protectiveEquipmentStatus.getOrganization().getId() != user.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete vyskladnit cizí ochranné pomůcky!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent();
        return new ModelAndView("useOrganizationProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent);
    }

    /**
     * Saves the new amount of the organization protective equipments after their usage.
     *
     * @param protectiveEquipmentStatusChangeEvent - Contains the required data used for returning view for using organization protective equipments in case of a validation error.
     * @param result                               - Result of the automated validation.
     * @param amountUsed                           - Amounts of the used protective equipments.
     * @param protectiveEquipmentStatusId          - Id of the protective equipment status which is going to be changed.
     * @param billId                               - Id of the bill or other document.
     * @param authentication                       - Contains authentication info about the user.
     * @param redirect                             - Used to save redirect messages.
     * @return - Either the previous form in case of a validation error. Or redirects user back to the action of showing organization protective equipments in case of other errors or success.
     */
    @PostMapping("/saveUsedOrganizationProtectiveEquipments")
    public ModelAndView saveUsedOrganizationProtectiveEquipments(@Valid final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent, final BindingResult result, @RequestParam final int amountUsed, @RequestParam final int protectiveEquipmentStatusId, @RequestParam final long billId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        if (protectiveEquipmentStatus == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getOrganization() == null || protectiveEquipmentStatus.getOrganization().getId() != user.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete vyskladnit cizí ochranné pomůcky!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        if (result.hasErrors()) {
            return new ModelAndView("useOrganizationProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent);
        }
        final int originalAmount = protectiveEquipmentStatus.getQuantity();

        if (amountUsed < 1) {
            return new ModelAndView("useOrganizationProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Množství vyskladněných ochranných pomůcek musí být větší než 0. Prosím zadejte větší množství, v kladném čísle.");
        }
        final int newAmount = originalAmount - amountUsed;
        if (newAmount < 0) {
            return new ModelAndView("useOrganizationProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Nemůžete vyskladnit větší množství položek, než je ve vašem skladě. Zadejte prosím menší množství vyskladněných položek.");
        }
        if (billId < 1) {
            return new ModelAndView("useOrganizationProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Prosím vložte číslo dokladu.");
        }
        protectiveEquipmentStatus.setQuantity(newAmount);
        final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(3);
        final ProtectiveEquipmentStatusChangeEvent savedProtectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent(new Date(), originalAmount, newAmount, protectiveEquipmentStatusChangeEvent.getComment(), protectiveEquipmentStatusRepository.save(protectiveEquipmentStatus), user, protectiveEquipmentStatusChangeEventType, user.getOrganization(), billId);
        protectiveEquipmentStatusChangeEventRepository.save(savedProtectiveEquipmentStatusChangeEvent);
        redirect.addFlashAttribute("success", amountUsed + " " + protectiveEquipmentStatus.getProtectiveEquipment().getName() + " bylo úspěšně vyskladněno.");
        return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
    }
}
