package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipment;
import cz.uhk.distribuceOchrannychPomucek.repository.ProtectiveEquipmentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Contains actions of working with the general protective equipment definitions managed by global administrator.
 */
@Controller
@Secured("ROLE_GlobalAdmin")
public class ProtectiveEquipmentController {

    private final ProtectiveEquipmentRepository protectiveEquipmentRepository;

    public ProtectiveEquipmentController(final ProtectiveEquipmentRepository protectiveEquipmentRepository) {
        this.protectiveEquipmentRepository = protectiveEquipmentRepository;
    }

    /**
     * Shows protective equipment definitions.
     *
     * @param protectiveEquipmentName - Name of protective equipment for filtering result by name. Optional.
     * @param currentPage             - Current page of the paged result. Starts at 0.
     * @return - View showing the protective equipment definitions.
     */
    @GetMapping("/showProtectiveEquipments")
    public ModelAndView showProtectiveEquipments(@RequestParam(required = false) final String protectiveEquipmentName, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final Page<ProtectiveEquipment> protectiveEquipmentPages;
        final int numberOfPages;
        final int numberOfItemsInPage = 5;
        if (protectiveEquipmentName == null || protectiveEquipmentName.isBlank()) {
            protectiveEquipmentPages = protectiveEquipmentRepository.findAll(PageRequest.of(currentPage, numberOfItemsInPage, Sort.by(Sort.Direction.ASC, "name")));

        } else {
            protectiveEquipmentPages = protectiveEquipmentRepository.findAllByNameContaining(protectiveEquipmentName, PageRequest.of(currentPage, numberOfItemsInPage, Sort.by(Sort.Direction.ASC, "name")));
        }
        numberOfPages = protectiveEquipmentPages.getTotalPages();
        final List<ProtectiveEquipment> protectiveEquipments = protectiveEquipmentPages.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        return new ModelAndView("showProtectiveEquipments").addObject("protectiveEquipments", protectiveEquipments).addObject("pages", pages).addObject("currentPage", currentPage).addObject("protectiveEquipmentName", protectiveEquipmentName);
    }

    /**
     * Opens form of adding new protective equipment definition.
     *
     * @return - Form for adding new protective equipment definition.
     */
    @GetMapping("/addProtectiveEquipment")
    public ModelAndView addProtectiveEquipment() {
        final ProtectiveEquipment equipment = new ProtectiveEquipment();
        return new ModelAndView("addProtectiveEquipment").addObject("protectiveEquipment", equipment);
    }

    /**
     * Saves the new protective equipment definition.
     *
     * @param protectiveEquipment - Contains protective equipment information.
     * @param result              - Result of the automated validation.
     * @param redirect            - Used to save redirect messages.
     * @return - Returns previous form in case that there is any validation error. Or redirects user back to the action of showing protective equipments in case of success.
     */
    @PostMapping("/saveProtectiveEquipment")
    public ModelAndView saveNewProtectiveEquipment(@Valid final ProtectiveEquipment protectiveEquipment, final BindingResult result, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("addProtectiveEquipment").addObject("protectiveEquipment", protectiveEquipment);
        }
        if (protectiveEquipmentRepository.existsByName(protectiveEquipment.getName())) {
            return new ModelAndView("addProtectiveEquipment").addObject("protectiveEquipment", protectiveEquipment).addObject("error", "Ochranná pomůcka se jménem " + protectiveEquipment.getName() + " už existuje. Zadejte prosím jiné jméno.");
        }
        final ProtectiveEquipment savedProtectiveEquipment = new ProtectiveEquipment(protectiveEquipment.getName(), protectiveEquipment.getDescription());
        protectiveEquipmentRepository.save(savedProtectiveEquipment);
        redirect.addFlashAttribute("success", "Ochranná pomůcka " + savedProtectiveEquipment.getName() + " byla úspěšně uložena do databáze.");
        return new ModelAndView("redirect:/showProtectiveEquipments");
    }

    /**
     * Opens form for changing existing protective equipment definition.
     *
     * @param id       - Id of a protective equipment which is going to be changed.
     * @param redirect - Used to save redirect messages.
     * @return - Form for changing protective equipment. Or redirects to action of showing protective equipments in case of error.
     */
    @GetMapping("/changeProtectiveEquipment")
    public ModelAndView changeProtectiveEquipment(@RequestParam final int id, final RedirectAttributes redirect) {
        final ProtectiveEquipment protectiveEquipment = protectiveEquipmentRepository.findById(id);
        if (protectiveEquipment == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showProtectiveEquipments");
        }
        return new ModelAndView("changeProtectiveEquipment").addObject(protectiveEquipment);
    }

    /**
     * Updates the changed protective equipment in the database.
     *
     * @param equipment - Contains new information about the changed protective equipment.
     * @param result    - Result of the automated validation.
     * @param redirect  - Used to save redirect messages.
     * @return - Previous form in case of validation error. Or redirect user to the action of showing protective equipments in case of success.
     */
    @PostMapping("/saveChangedProtectiveEquipment")
    public ModelAndView saveChangedProtectiveEquipment(@Valid final ProtectiveEquipment equipment, final BindingResult result, final RedirectAttributes redirect) {
        if (equipment.getId() < 1) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showProtectiveEquipments");
        }
        if (result.hasErrors()) {
            return new ModelAndView("changeProtectiveEquipment").addObject(equipment);
        }
        final String success;
        if (protectiveEquipmentRepository.existsByName(equipment.getName())) {
            equipment.setName(protectiveEquipmentRepository.findById(equipment.getId()).getName());
            success = "Jméno ochranné pomůcky " + equipment.getName() + " už existuje. Proto byl změněn pouze její popis.";
        } else {
            success = "";
        }
        protectiveEquipmentRepository.save(equipment);
        redirect.addFlashAttribute("success", success + " Změněná ochranná pomůcka " + equipment.getName() + " byla úspěšně uložena do databáze.");
        return new ModelAndView("redirect:/showProtectiveEquipments");
    }
}
