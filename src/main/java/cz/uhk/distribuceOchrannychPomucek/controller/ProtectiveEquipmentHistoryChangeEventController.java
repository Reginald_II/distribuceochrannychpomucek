package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentStatusChangeEvent;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentStatusChangeEventType;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.ProtectiveEquipmentStatusChangeEventRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.ProtectiveEquipmentStatusChangeEventTypeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Contains actions working with the protective equipment history change events.
 */
@Controller
public class ProtectiveEquipmentHistoryChangeEventController {
    private final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository;
    private final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository;
    private final AppUserRepository appUserRepository;

    public ProtectiveEquipmentHistoryChangeEventController(final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository, final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository, final AppUserRepository appUserRepository) {
        this.protectiveEquipmentStatusChangeEventRepository = protectiveEquipmentStatusChangeEventRepository;
        this.protectiveEquipmentStatusChangeEventTypeRepository = protectiveEquipmentStatusChangeEventTypeRepository;
        this.appUserRepository = appUserRepository;
    }

    /**
     * Shows history of the protective equipment status changes of a region.
     *
     * @param authentication                                    - Contains authentication info of the user.
     * @param protectiveEquipmentRequestStatusChangeEventTypeId - Id of a type of change. Optional.
     * @param currentPage                                       - Current page of the paged result. Starts at 0.
     * @return - View showing the history of protective equipment status changes.
     */
    @GetMapping("/showRegionProtectiveEquipmentStatusHistory")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showRegionProtectiveEquipmentStatusHistory(final Authentication authentication, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentRequestStatusChangeEventTypeId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEventPage;
        final int pageItemsAmount = 5;
        if (protectiveEquipmentRequestStatusChangeEventTypeId < 1) {
            protectiveEquipmentStatusChangeEventPage = protectiveEquipmentStatusChangeEventRepository.findByRegion(user.getRegion(), PageRequest.of(currentPage, pageItemsAmount, Sort.by(Sort.Direction.DESC, "timeOfEvent")));
        } else {
            protectiveEquipmentStatusChangeEventPage = protectiveEquipmentStatusChangeEventRepository.findAllByRegionAndProtectiveEquipmentStatusChangeEventTypeId(user.getRegion(), protectiveEquipmentRequestStatusChangeEventTypeId, PageRequest.of(currentPage, pageItemsAmount, Sort.by(Sort.Direction.DESC, "timeOfEvent")));
        }
        final int numberOfPages = protectiveEquipmentStatusChangeEventPage.getTotalPages();
        final List<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentStatusChangeEventType> protectiveEquipmentStatusChangeEventTypes = protectiveEquipmentStatusChangeEventTypeRepository.findAll();
        return new ModelAndView("showRegionProtectiveEquipmentStatusHistory").addObject("pages", pages).addObject("currentPage", currentPage).addObject("protectiveEquipmentRequestStatusChangeEventTypeId", protectiveEquipmentRequestStatusChangeEventTypeId).addObject("protectiveEquipmentStatusChangeEventTypes", protectiveEquipmentStatusChangeEventTypes).addObject("protectiveEquipmentStatusChangeEvents", protectiveEquipmentStatusChangeEvents);
    }

    /**
     * Shows history of the protective equipment status changes of an organization.
     *
     * @param authentication                                    - Contains authentication info of the user.
     * @param protectiveEquipmentRequestStatusChangeEventTypeId - Id of a type of change. Optional.
     * @param currentPage                                       - Current page of the paged result. Starts at 0.
     * @return - View showing the history of protective equipment status changes.
     */
    @GetMapping("/showOrganizationProtectiveEquipmentStatusHistory")
    @Secured({"ROLE_OrganizationAdmin", "ROLE_OrganizationUser"})
    public ModelAndView showOrganizationProtectiveEquipmentStatusHistory(final Authentication authentication, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentRequestStatusChangeEventTypeId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEventPage;
        final int pageItemsAmount = 5;
        if (protectiveEquipmentRequestStatusChangeEventTypeId < 1) {
            protectiveEquipmentStatusChangeEventPage = protectiveEquipmentStatusChangeEventRepository.findByOrganization(user.getOrganization(), PageRequest.of(currentPage, pageItemsAmount, Sort.by(Sort.Direction.DESC, "timeOfEvent")));
        } else {
            protectiveEquipmentStatusChangeEventPage = protectiveEquipmentStatusChangeEventRepository.findAllByOrganizationAndProtectiveEquipmentStatusChangeEventTypeId(user.getOrganization(), protectiveEquipmentRequestStatusChangeEventTypeId, PageRequest.of(currentPage, pageItemsAmount, Sort.by(Sort.Direction.DESC, "timeOfEvent")));
        }
        final int numberOfPages = protectiveEquipmentStatusChangeEventPage.getTotalPages();
        final List<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEvents = protectiveEquipmentStatusChangeEventPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentStatusChangeEventType> protectiveEquipmentStatusChangeEventTypes = protectiveEquipmentStatusChangeEventTypeRepository.findAll();
        return new ModelAndView("showOrganizationProtectiveEquipmentStatusHistory").addObject("pages", pages).addObject("currentPage", currentPage).addObject("protectiveEquipmentRequestStatusChangeEventTypeId", protectiveEquipmentRequestStatusChangeEventTypeId).addObject("protectiveEquipmentStatusChangeEventTypes", protectiveEquipmentStatusChangeEventTypes).addObject("protectiveEquipmentStatusChangeEvents", protectiveEquipmentStatusChangeEvents);
    }

    /**
     * Opens form for exporting the protective equipment status change history data of a region into a .csv file.
     *
     * @return - Returns view with the form.
     */
    @GetMapping("/exportRegionProtectiveEquipmentStatusChangeEventsForm")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView exportRegionProtectiveEquipmentStatusChangeEventsForm() {
        final List<ProtectiveEquipmentStatusChangeEventType> protectiveEquipmentStatusChangeEventTypes = protectiveEquipmentStatusChangeEventTypeRepository.findAll();
        return new ModelAndView("exportRegionProtectiveEquipmentStatusChangeEventsForm").addObject("protectiveEquipmentStatusChangeEventTypes", protectiveEquipmentStatusChangeEventTypes);
    }

    /**
     * Opens form for exporting the protective equipment status change history data of a region into a .csv file.
     *
     * @return - Returns view with the form.
     */
    @GetMapping("/exportOrganizationProtectiveEquipmentStatusChangeEventsForm")
    @Secured({"ROLE_OrganizationAdmin", "ROLE_OrganizationUser"})
    public ModelAndView exportOrganizationProtectiveEquipmentStatusChangeEventsForm() {
        final List<ProtectiveEquipmentStatusChangeEventType> protectiveEquipmentStatusChangeEventTypes = protectiveEquipmentStatusChangeEventTypeRepository.findAll();
        return new ModelAndView("exportOrganizationProtectiveEquipmentStatusChangeEventsForm").addObject("protectiveEquipmentStatusChangeEventTypes", protectiveEquipmentStatusChangeEventTypes);
    }
}
