package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.*;
import cz.uhk.distribuceOchrannychPomucek.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Contains actions for working with the protective equipment requests.
 */
@Controller
@Secured({"ROLE_OrganizationAdmin", "ROLE_OrganizationUser"})
public class ProtectiveEquipmentRequestController {
    private final AppUserRepository appUserRepository;
    //Used for working with the protective equipment requests themselves.
    private final ProtectiveEquipmentRequestRepository protectiveEquipmentRequestRepository;
    //Used for working with separate protective equipments, which are parts of the requests by referencing them per each protective equipment there.
    private final RequestProtectiveEquipmentRepository requestProtectiveEquipmentRepository;
    private final ProtectiveEquipmentRepository protectiveEquipmentRepository;
    private final ProtectiveEquipmentRequestStatusRepository protectiveEquipmentRequestStatusRepository;
    private final UserInfoRepository userInfoRepository;
    private final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository;
    private final TransferProtectiveEquipmentRepository transferProtectiveEquipmentRepository;
    private final ProtectiveEquipmentTransferRepository protectiveEquipmentTransferRepository;
    private final ProtectiveEquipmentTransferStatusRepository protectiveEquipmentTransferStatusRepository;
    private final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository;
    private final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository;
    private final ProtectiveEquipmentTransferTypeRepository protectiveEquipmentTransferTypeRepository;

    public ProtectiveEquipmentRequestController(final AppUserRepository appUserRepository, final ProtectiveEquipmentRequestRepository protectiveEquipmentRequestRepository, final RequestProtectiveEquipmentRepository requestProtectiveEquipmentRepository, final ProtectiveEquipmentRepository protectiveEquipmentRepository, final ProtectiveEquipmentRequestStatusRepository protectiveEquipmentRequestStatusRepository, final UserInfoRepository userInfoRepository, final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository, final TransferProtectiveEquipmentRepository transferProtectiveEquipmentRepository, final ProtectiveEquipmentTransferRepository protectiveEquipmentTransferRepository, final ProtectiveEquipmentTransferStatusRepository protectiveEquipmentTransferStatusRepository, final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository, final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository, final ProtectiveEquipmentTransferTypeRepository protectiveEquipmentTransferTypeRepository) {
        this.appUserRepository = appUserRepository;
        this.protectiveEquipmentRequestRepository = protectiveEquipmentRequestRepository;
        this.requestProtectiveEquipmentRepository = requestProtectiveEquipmentRepository;
        this.protectiveEquipmentRepository = protectiveEquipmentRepository;
        this.protectiveEquipmentRequestStatusRepository = protectiveEquipmentRequestStatusRepository;
        this.userInfoRepository = userInfoRepository;
        this.protectiveEquipmentStatusRepository = protectiveEquipmentStatusRepository;
        this.transferProtectiveEquipmentRepository = transferProtectiveEquipmentRepository;
        this.protectiveEquipmentTransferRepository = protectiveEquipmentTransferRepository;
        this.protectiveEquipmentTransferStatusRepository = protectiveEquipmentTransferStatusRepository;
        this.protectiveEquipmentStatusChangeEventRepository = protectiveEquipmentStatusChangeEventRepository;
        this.protectiveEquipmentStatusChangeEventTypeRepository = protectiveEquipmentStatusChangeEventTypeRepository;
        this.protectiveEquipmentTransferTypeRepository = protectiveEquipmentTransferTypeRepository;
    }

    /**
     * Shows all protective equipment requests made by the organization.
     *
     * @param authentication                     - Authentication info of the user.
     * @param protectiveEquipmentRequestStatusId - Id of request status for filtering result by it. Optional parameter.
     * @param currentPage                        - Current page of the paged result. Starts at 0.
     * @return - View showing organization protective equipment requests.
     */
    @GetMapping("/showOrganizationProtectiveEquipmentRequests")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView showOrganizationProtectiveEquipmentRequests(final Authentication authentication, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentRequestStatusId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentRequest> protectiveEquipmentRequestsPage;
        final int itemsAmount = 5;
        if (protectiveEquipmentRequestStatusId < 1) {
            protectiveEquipmentRequestsPage = protectiveEquipmentRequestRepository.findAllByOrganization(user.getOrganization(), PageRequest.of(currentPage, itemsAmount, Sort.by(Sort.Direction.DESC, "id")));
        } else {
            protectiveEquipmentRequestsPage = protectiveEquipmentRequestRepository.findAllByOrganizationAndProtectiveEquipmentRequestStatusId(user.getOrganization(), protectiveEquipmentRequestStatusId, PageRequest.of(currentPage, itemsAmount, Sort.by(Sort.Direction.DESC, "id")));
        }
        final int numberOfPages = protectiveEquipmentRequestsPage.getTotalPages();
        final List<ProtectiveEquipmentRequest> protectiveEquipmentRequests = protectiveEquipmentRequestsPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentRequestStatus> protectiveEquipmentRequestStatuses = protectiveEquipmentRequestStatusRepository.findAll();
        return new ModelAndView("showOrganizationProtectiveEquipmentRequests").addObject("protectiveEquipmentRequests", protectiveEquipmentRequests).addObject("protectiveEquipmentRequestStatuses", protectiveEquipmentRequestStatuses).addObject("protectiveEquipmentRequestStatusId", protectiveEquipmentRequestStatusId).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Shows protective equipment requests of an organization made by the organization user calling the action.
     *
     * @param authentication                     - Authentication info of the user.
     * @param protectiveEquipmentRequestStatusId - Id of protective equipment request status. Used for filtering the result by the status. Optional parameter.
     * @param currentPage                        - Current page of the paged result. Starts at 0.
     * @return - View showing the protective equipment requests made by the calling user in his organization.
     */
    @GetMapping("/showOwnOrganizationProtectiveEquipmentRequests")
    public ModelAndView showOwnOrganizationProtectiveEquipmentRequests(final Authentication authentication, @RequestParam(required = false, defaultValue = "1") final int protectiveEquipmentRequestStatusId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentRequest> protectiveEquipmentRequestsPage;
        final int itemsAmount = 5;
        if (protectiveEquipmentRequestStatusId < 1) {
            protectiveEquipmentRequestsPage = protectiveEquipmentRequestRepository.findAllByAppUser(user, PageRequest.of(currentPage, itemsAmount, Sort.by(Sort.Direction.DESC, "id")));
        } else {
            protectiveEquipmentRequestsPage = protectiveEquipmentRequestRepository.findAllByAppUserAndProtectiveEquipmentRequestStatusId(user, protectiveEquipmentRequestStatusId, PageRequest.of(currentPage, itemsAmount, Sort.by(Sort.Direction.DESC, "id")));
        }
        final int numberOfPages = protectiveEquipmentRequestsPage.getTotalPages();
        final List<ProtectiveEquipmentRequest> protectiveEquipmentRequests = protectiveEquipmentRequestsPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentRequestStatus> protectiveEquipmentRequestStatuses = protectiveEquipmentRequestStatusRepository.findAll();
        return new ModelAndView("showOwnOrganizationProtectiveEquipmentRequests").addObject("protectiveEquipmentRequests", protectiveEquipmentRequests).addObject("protectiveEquipmentRequestStatuses", protectiveEquipmentRequestStatuses).addObject("protectiveEquipmentRequestStatusId", protectiveEquipmentRequestStatusId).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Creates new protective equipmnet request.
     *
     * @param authentication - Authentication information of the user.
     * @return - Redirects user to the action showing the new created request.
     */
    @GetMapping("/createNewProtectionEquipmentRequest")
    public RedirectView createNewProtectionEquipmentRequest(final Authentication authentication) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final ProtectiveEquipmentRequest newProtectiveEquipmentRequest = new ProtectiveEquipmentRequest(user.getOrganization(), user, protectiveEquipmentRequestStatusRepository.findById(1));
        final int newProtectiveEquipmentRequestId = protectiveEquipmentRequestRepository.save(newProtectiveEquipmentRequest).getId();
        return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + newProtectiveEquipmentRequestId, true);

    }

    /**
     * Shows information about the selected protective equipment request.
     *
     * @param protectiveEquipmentRequestId - Id of the selected protective equipment request.
     * @param authentication               - Authentication information of the user.
     * @param redirect                     - Used to save redirect messages.
     * @return - Redirects user to action of showing his protective equipment requests in case of an error. In case of success returns view showing the information of the protective equipment request.
     */
    @GetMapping("/showDetailedOrganizationProtectiveEquipmentRequest")
    public ModelAndView showDetailedOrganizationProtectiveEquipmentRequest(@RequestParam final int protectiveEquipmentRequestId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id požadavku na ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipmentRequests");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getOrganization() == null || user.getOrganization().getId() != protectiveEquipmentRequest.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit požadavky cizích organizací!");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipmentRequests");
        }
        final List<RequestProtectiveEquipment> requestProtectiveEquipments = requestProtectiveEquipmentRepository.findByProtectiveEquipmentRequest(protectiveEquipmentRequest);
        final ModelAndView model = new ModelAndView("showDetailedOrganizationProtectiveEquipmentRequest");
        model.addObject("protectiveEquipmentRequest", protectiveEquipmentRequest);
        model.addObject("requestProtectiveEquipments", requestProtectiveEquipments);
        return model;
    }

    /**
     * Shows information of protective equipment request made by other user in the organization to the admin.
     *
     * @param protectiveEquipmentRequestId - Id of the selected protective equipment request.
     * @param authentication               - Authentication information of the user.
     * @param redirect                     - Used to save redirect mesages.
     * @return - Redirects user to action of showing organization protective equipment requests in case of error. In case of success returns view showing the selected protective equipment request.
     */
    @GetMapping("/showDetailedOrganizationOtherProtectiveEquipmentRequest")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView showDetailedOrganizationOtherProtectiveEquipmentRequest(@RequestParam final int protectiveEquipmentRequestId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id požadavku na ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentRequests");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getOrganization() == null || user.getOrganization().getId() != protectiveEquipmentRequest.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit požadavky cizích organizací!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentRequests");
        }
        final List<RequestProtectiveEquipment> requestProtectiveEquipments = requestProtectiveEquipmentRepository.findByProtectiveEquipmentRequest(protectiveEquipmentRequest);
        final ModelAndView model = new ModelAndView("showDetailedOrganizationOtherProtectiveEquipmentRequest");
        model.addObject("protectiveEquipmentRequest", protectiveEquipmentRequest);
        model.addObject("requestProtectiveEquipments", requestProtectiveEquipments);
        return model;
    }

    /**
     * Adds a protective equipment in selected amount to a protective equipment request.
     *
     * @param protectiveEquipmentRequestId - Id of a protective equipment request to which is the protective equipment going to be added.
     * @param protectiveEquipmentId        - Id of the added protective equipment.
     * @param quantity                     - Quantity of added protective equipment in pieces.
     * @param authentication               - Authentication information of the user.
     * @param redirect                     - Used to save redirect messages.
     * @return - Redirects user either to the detailed protective equipment request action in case of error or to the action of showing all user's protective equipment requests in case that the detailed protective equipment request information can't be shown. In case of validation error returns the previous view of adding protective equipment to the request. In case of success  redirects user to action of showing the detailed protective equipment request, to which the new protective equipment was added.
     */
    @PostMapping("/saveAddedProtectiveEquipmentToRequest")
    public ModelAndView saveAddedProtectiveEquipmentToRequest(@RequestParam final int protectiveEquipmentRequestId, @RequestParam final int protectiveEquipmentId, @RequestParam final int quantity, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id žádosti o ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentRequest.getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete přidat ochrannou pomůcku k cizí žádost!");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete přidat ochrannou pomůcku k odeslané žádosti!");
            return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
        }
        final ProtectiveEquipment protectiveEquipment = protectiveEquipmentRepository.findById(protectiveEquipmentId);
        if (protectiveEquipment == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/addRequestProtectiveEquipment?protectiveEquipmentRequestId=" + protectiveEquipmentId);
        }
        if (quantity < 1) {
            redirect.addFlashAttribute("error", "Prosím zadejte množství ochranných pomůcek větší než 0.");
            return new ModelAndView("redirect:/addRequestProtectiveEquipment?protectiveEquipmentRequestId=" + protectiveEquipmentId);
        }
        if (requestProtectiveEquipmentRepository.existsByProtectiveEquipmentRequestAndProtectiveEquipment(protectiveEquipmentRequest, protectiveEquipment)) {
            final RequestProtectiveEquipment requestProtectiveEquipment = requestProtectiveEquipmentRepository.findByProtectiveEquipmentRequestAndProtectiveEquipment(protectiveEquipmentRequest, protectiveEquipment);
            requestProtectiveEquipment.addQuantity(quantity);
            requestProtectiveEquipmentRepository.save(requestProtectiveEquipment);
        } else {
            final RequestProtectiveEquipment requestProtectiveEquipment = new RequestProtectiveEquipment(quantity, quantity, protectiveEquipmentRequest, protectiveEquipment);
            requestProtectiveEquipmentRepository.save(requestProtectiveEquipment);
        }
        redirect.addFlashAttribute("success", "Ochranná pomůcka " + protectiveEquipment.getName() + " v množství " + quantity + " byla úspěšně přidána k žádosti.");
        return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
    }

    /**
     * Removes protective equipment from the request.
     *
     * @param requestProtectiveEquipmentId - Protective equipment in the request to be removed.
     * @param authentication               - Authentication information of the user.
     * @param redirect                     - Used to save redirect messages.
     * @return - Redirects user to either action of showing his own protective equipment requests in case of error working with invalid protective equipment request. Or in case of other errors or success redirects user to action of showing the detailed protective equipment request, from which the protective equipment was removed.
     */
    @GetMapping("/removeRequestProtectiveEquipmentFromRequest")
    public RedirectView removeRequestProtectiveEquipmentFromRequest(@RequestParam final int requestProtectiveEquipmentId, final Authentication authentication, final RedirectAttributes redirect) {
        final RequestProtectiveEquipment requestProtectiveEquipment = requestProtectiveEquipmentRepository.findById(requestProtectiveEquipmentId);
        if (requestProtectiveEquipment == null) {
            redirect.addFlashAttribute("Id ochranné pomůcky požadavku je neplatné.");
            return new RedirectView("/showOwnOrganizationProtectiveEquipments", true);
        }
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = requestProtectiveEquipment.getProtectiveEquipmentRequest();
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentRequest.getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete smazat ochrannou pomůcku z cizího požadavku!");
            return new RedirectView("/showOwnOrganizationProtectiveEquipments", true);
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete smazat ochrannou pomůcku z už odeslaného požadavku!");
            return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + requestProtectiveEquipment.getProtectiveEquipmentRequest().getId(), true);
        }
        final String protectiveEquipmentName = requestProtectiveEquipment.getProtectiveEquipment().getName();
        requestProtectiveEquipmentRepository.delete(requestProtectiveEquipment);
        redirect.addFlashAttribute("success", "Ochranná pomůcka " + protectiveEquipmentName + " byla úspěšně odebrána z vaší žádosti.");
        return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + requestProtectiveEquipment.getProtectiveEquipmentRequest().getId(), true);
    }

    /**
     * Opens form for changing quantity of protective equipment in a request.
     *
     * @param requestProtectiveEquipmentId - Id of the protective equipment in a request, which quantity is going to be changed.
     * @param authentication               - Authentication information of the user calling the action.
     * @param redirect                     - Used to save redirect messages.
     * @return - Redirects user to action of showing his protective equipment requests or showing the detailed protective equipment request in case of error. In case of success returns view with the form for changing the quantity.
     */
    @GetMapping("/changeRequestProtectiveEquipmentQuantity")
    public ModelAndView changeRequestProtectiveEquipmentQuantity(@RequestParam final int requestProtectiveEquipmentId, final Authentication authentication, final RedirectAttributes redirect) {
        final RequestProtectiveEquipment requestProtectiveEquipment = requestProtectiveEquipmentRepository.findById(requestProtectiveEquipmentId);
        if (requestProtectiveEquipment == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky v žádosti je neplatné.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (requestProtectiveEquipment.getProtectiveEquipmentRequest().getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete upravovat cizí žádost!");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        if (requestProtectiveEquipment.getProtectiveEquipmentRequest().getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete upravovat už odeslanou žádost!");
            return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + requestProtectiveEquipment.getProtectiveEquipmentRequest().getId());
        }
        return new ModelAndView("changeRequestProtectiveEquipmentQuantity").addObject(requestProtectiveEquipment);
    }

    /**
     * Updates the database row of the request protective equipment with the new value.
     *
     * @param requestProtectiveEquipment - Contains the information about the protective equipment in the request.
     * @param result                     - Result of the automated validation.
     * @param authentication             - Authentication information of the user calling the action.
     * @param redirect                   -Used to save redirect messages.
     * @return - Redirects user to the action of showing organization protective equipments or back to showing the detailed protective equipment request in case of error. In case of success redirects user to the action of showing the changed request.
     */
    @PostMapping("/saveChangedRequestProtectiveEquipmentQuantity")
    public ModelAndView saveChangedRequestProtectiveEquipmentQuantity(@Valid final RequestProtectiveEquipment requestProtectiveEquipment, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeRequestProtectiveEquipmentQuantity").addObject(requestProtectiveEquipment);
        }
        final RequestProtectiveEquipment savedRequestProtectiveEquipment = requestProtectiveEquipmentRepository.findById(requestProtectiveEquipment.getId());
        if (savedRequestProtectiveEquipment == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky v žádosti je neplatné.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (savedRequestProtectiveEquipment.getProtectiveEquipmentRequest().getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete upravovat cizí žádost!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipments");
        }
        if (savedRequestProtectiveEquipment.getProtectiveEquipmentRequest().getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete upravovat už odeslanou žádost!");
            return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + requestProtectiveEquipment.getProtectiveEquipmentRequest().getId());
        }
        savedRequestProtectiveEquipment.setQuantity(requestProtectiveEquipment.getQuantity());
        requestProtectiveEquipmentRepository.save(savedRequestProtectiveEquipment);
        redirect.addFlashAttribute("success", "Množství ochranných pomůcek " + savedRequestProtectiveEquipment.getProtectiveEquipment().getName() + " bylo úspěšně změněno na " + savedRequestProtectiveEquipment.getQuantity() + ".");
        return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + savedRequestProtectiveEquipment.getProtectiveEquipmentRequest().getId());
    }

    /**
     * Changes the protective equipment request to the waiting status - waiting to be approved or denied by a region officer.
     *
     * @param protectiveEquipmentRequestId - Id of the protective equipment request to be sent.
     * @param authentication               - Authentication information of the user calling the action.
     * @param redirect                     - Used to save redirect messages.
     * @return - Redirects user to the action of either showing the sent protective equipment request or back to the action of showing user's protective equipment requests, depending on the exact error or success.
     */
    @GetMapping("/sendProtectiveEquipmentRequest")
    public RedirectView sendProtectiveEquipmentRequest(final int protectiveEquipmentRequestId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id žádosti o ochranné pomůcky je neplatné.");
            return new RedirectView("/showOwnOrganizationProtectiveEquipments", true);
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentRequest.getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete odeslat cizí žádost!");
            return new RedirectView("/showOwnOrganizationProtectiveEquipments", true);
        }
        //Checks if the protective equipment request is already sent.
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Žádost už je odeslána!");
            return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId(), true);
        }
        if (protectiveEquipmentRequest.getMessage() == null || protectiveEquipmentRequest.getMessage().isBlank()) {
            redirect.addFlashAttribute("error", "Před odesláním prosím vyplňte zprávu požadavku.");
            return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId(), true);
        }
        //Checks if there is any protective equipment added to the request.
        if (!requestProtectiveEquipmentRepository.existsByProtectiveEquipmentRequest(protectiveEquipmentRequest)) {
            redirect.addFlashAttribute("error", "Před odesláním nejdříve musíte k požadavku přidat alespoň jednu ochrannou pomůcku ve vybraném množství.");
            return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId(), true);
        }
        protectiveEquipmentRequest.setProtectiveEquipmentRequestStatus(protectiveEquipmentRequestStatusRepository.findById(2));
        protectiveEquipmentRequestRepository.save(protectiveEquipmentRequest);
        redirect.addFlashAttribute("success", "Žádost ochranných pomůcek byla úspěšně odeslána.");
        return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId(), true);
    }

    /**
     * Deletes protective equipment request from the database.
     * Should process only the ones which weren't sent and so their status has id value 1.
     *
     * @param protectiveEquipmentRequestId - Id of the protective equipment request going to be deleted.
     * @param authentication               - Authentication information of the user calling the action.
     * @param redirect                     - Used to save redirect messages.
     * @return - Redirects user to the action of showing his protective equipment requests in case of success. In case of error either redirects user back to the action of showing his protective equipment requests or shows the detailed protective equipment request which the user tried and failed to delete.
     */
    @GetMapping("/deleteProtectiveEquipmentRequest")
    public RedirectView deleteProtectiveEquipmentRequest(@RequestParam final int protectiveEquipmentRequestId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id žádosti o ochranné pomůcky je neplatné.");
            return new RedirectView("/showOwnOrganizationProtectiveEquipments", true);
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentRequest.getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete smazat cizí žádost!");
            return new RedirectView("/showOwnOrganizationProtectiveEquipments", true);
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete smazat odeslaný požadavek!");
            return new RedirectView("/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId(), true);
        }
        final List<RequestProtectiveEquipment> requestProtectiveEquipments = requestProtectiveEquipmentRepository.findByProtectiveEquipmentRequest(protectiveEquipmentRequest);
        if (requestProtectiveEquipments != null && !requestProtectiveEquipments.isEmpty()) {
            requestProtectiveEquipmentRepository.deleteAll(requestProtectiveEquipments);
            requestProtectiveEquipments.clear();
        }
        protectiveEquipmentRequestRepository.delete(protectiveEquipmentRequest);
        redirect.addFlashAttribute("success", "Žádost byla úspěšně smazána.");
        return new RedirectView("/showOwnOrganizationProtectiveEquipmentRequests", true);

    }

    /**
     * Opens a form for adding or changing a message of the protective equipment request.
     *
     * @param protectiveEquipmentRequestId - Id of the protective equipment requst, which message is going to be added or changed.
     * @param authentication               - Authentication information of the user who is calling the action.
     * @param redirect                     - Used to save redirect messages.
     * @return - View with the form in case of success. In case of errors redirects user to action of showing is protective equipment requests or showing detailed information of the protective equipment request to which the user tried add or change its message.
     */
    @GetMapping("/writeProtectiveEquipmentRequestMessage")
    public ModelAndView writeProtectiveEquipmentRequestMessage(@RequestParam final int protectiveEquipmentRequestId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id žádosti o ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentRequest.getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete měnit zprávu cizí žádosti!");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete změnit zprávu odeslané žádosti!");
            return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
        }
        return new ModelAndView("writeProtectiveEquipmentRequestMessage").addObject(protectiveEquipmentRequest);
    }

    /**
     * Saves the protective equipment request message to the database.
     *
     * @param protectiveEquipmentRequest - Protective equipment request which message is going to be added or updated.
     * @param result                     - Result of the automated validation.
     * @param authentication             - Authentication information of the user calling the method.
     * @param redirect                   - Used to save redirect messages.
     * @return - Previous view with the form in case of validation error. Or redirects user to the action either showing the detailed protective equipment which message was added or changed or showing the user's protective equipment requests, depending on the success or error.
     */
    @PostMapping("/saveProtectiveEquipmentRequestMessage")
    public ModelAndView saveProtectiveEquipmentRequestMessage(@Valid final ProtectiveEquipmentRequest protectiveEquipmentRequest, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("writeProtectiveEquipmentRequestMessage").addObject(protectiveEquipmentRequest);
        }
        if (protectiveEquipmentRequest.getMessage() == null || protectiveEquipmentRequest.getMessage().isBlank()) {
            return new ModelAndView("writeProtectiveEquipmentRequestMessage").addObject(protectiveEquipmentRequest).addObject("error", "Prosím vyplňte zprávu.");
        }
        final ProtectiveEquipmentRequest changedProtectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequest.getId());
        if (changedProtectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id žádosti o ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (changedProtectiveEquipmentRequest.getAppUser().getId() != user.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete měnit zprávu cizí žádosti!");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        //Checks if the protective equipment request was already sent.
        if (changedProtectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete změnit zprávu odeslané žádosti!");
            return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
        }
        changedProtectiveEquipmentRequest.setMessage(protectiveEquipmentRequest.getMessage());
        protectiveEquipmentRequestRepository.save(changedProtectiveEquipmentRequest);
        redirect.addFlashAttribute("success", "Zpráva žádosti byla úspěšně uložena.");
        return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
    }

    /**
     * Opens list of protective equipments to be added to the protective equipment request.
     *
     * @param protectiveEquipmentName      - Filters search of protective equipments by the name. Optional parameter.
     * @param protectiveEquipmentRequestId - Id of the protective equipment request to which is a protective equipment going to be added.
     * @param redirect                     - Used to save redirect messages.
     * @param currentPage                  - Current page of the paged result. Starts at 0.
     * @param authentication               - Authentication information of the user calling the action
     * @return - View showing the protective equipments, which can be added to the request. In case of error redirects user to previous actions.
     */
    @GetMapping("/addRequestProtectiveEquipment")
    public ModelAndView addRequestProtectiveEquipment(@RequestParam(required = false) final String protectiveEquipmentName, @RequestParam final int protectiveEquipmentRequestId, final RedirectAttributes redirect, @RequestParam(required = false, defaultValue = "0") final int currentPage, final Authentication authentication) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id žádosti na ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getId() != protectiveEquipmentRequest.getAppUser().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete přidávat ochranné pomůcky k cizím žádostem!.");
            return new ModelAndView("redirect:/showOwnOrganizationProtectiveEquipments");
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Nemůžete přidávat ochranné pomůcky k odeslané žádosti.");
            return new ModelAndView("redirect:/showDetailedOrganizationProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
        }
        final Page<ProtectiveEquipment> protectiveEquipmentPages;
        final int numberOfPages;
        final int pageItemsAmount = 5;
        if (protectiveEquipmentName == null || protectiveEquipmentName.isBlank()) {
            protectiveEquipmentPages = protectiveEquipmentRepository.findAll(PageRequest.of(currentPage, pageItemsAmount, Sort.by(Sort.Direction.ASC, "name")));
        } else {
            protectiveEquipmentPages = protectiveEquipmentRepository.findAllByNameContaining(protectiveEquipmentName, PageRequest.of(currentPage, pageItemsAmount, Sort.by(Sort.Direction.ASC, "name")));
        }
        numberOfPages = protectiveEquipmentPages.getTotalPages();
        final List<ProtectiveEquipment> protectiveEquipments = protectiveEquipmentPages.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        return new ModelAndView("addRequestProtectiveEquipment").addObject("protectiveEquipments", protectiveEquipments).addObject("protectiveEquipmentRequestId", protectiveEquipmentRequestId).addObject("protectiveEquipmentName", protectiveEquipmentName).addObject("pages", pages).addObject("currentPage", currentPage);
    }

    /**
     * Shows protective equipment requests which were made by organizations of the region of the user calling this action
     *
     * @param authentication                     - Authentication information of the user who is calling this action.
     * @param protectiveEquipmentRequestStatusId - Filters the result by the status of the protective equipment requests. Optional parameter.
     * @param currentPage                        - Current page of the paged result. Starts at 0.
     * @return - View showing the protective equipment requests of the region.
     */
    @GetMapping("/showRegionProtectiveEquipmentRequests")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showRegionProtectiveEquipmentRequests(final Authentication authentication, @RequestParam(required = false, defaultValue = "2") final int protectiveEquipmentRequestStatusId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentRequest> protectiveEquipmentRequestsPage;
        final int itemsPerPage = 5;
        if (protectiveEquipmentRequestStatusId <= 1) {
            protectiveEquipmentRequestsPage = protectiveEquipmentRequestRepository.findAllByOrganizationRegionAndProtectiveEquipmentRequestStatusIdGreaterThan(user.getRegion(), 1, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.DESC, "id")));
        } else {
            protectiveEquipmentRequestsPage = protectiveEquipmentRequestRepository.findAllByOrganizationRegionAndProtectiveEquipmentRequestStatusId(user.getRegion(), protectiveEquipmentRequestStatusId, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.DESC, "id")));
        }
        final int numberOfPages = protectiveEquipmentRequestsPage.getTotalPages();
        final List<ProtectiveEquipmentRequest> protectiveEquipmentRequests = protectiveEquipmentRequestsPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentRequestStatus> protectiveEquipmentRequestStatuses = protectiveEquipmentRequestStatusRepository.findByIdBetween(2, 4);
        return new ModelAndView("showRegionProtectiveEquipmentRequests").addObject("protectiveEquipmentRequests", protectiveEquipmentRequests).addObject("protectiveEquipmentRequestStatuses", protectiveEquipmentRequestStatuses).addObject("protectiveEquipmentRequestStatusId", protectiveEquipmentRequestStatusId).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Shows detailed information about the selected protective equipment request.
     *
     * @param protectiveEquipmentRequestId - Id of the protective equipment request which information is going to be shown.
     * @param authentication               - Authentication information of the user calling this action.
     * @param redirect                     - Used to save redirect messages.
     * @return - View showing the detailed information of the protective equipment request. In case of error, redirects user to the previous action.
     */
    @GetMapping("/showDetailedRegionProtectiveEquipmentRequest")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showDetailedRegionProtectiveEquipmentRequest(@RequestParam final int protectiveEquipmentRequestId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id požadavku na ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getRegion().getId() != protectiveEquipmentRequest.getOrganization().getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit žádosti cizích krajů!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        final List<RequestProtectiveEquipment> requestProtectiveEquipments = requestProtectiveEquipmentRepository.findByProtectiveEquipmentRequest(protectiveEquipmentRequest);
        final UserInfo userInfo = userInfoRepository.findByUser(user);
        final HashMap<Integer, Integer> regionProtectionEquipmentQuantities = new HashMap<>();
        boolean hasEnoughProtectiveEquipments = true;
        for (final RequestProtectiveEquipment requestProtectiveEquipment : requestProtectiveEquipments) {
            final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findByProtectiveEquipmentAndRegion(requestProtectiveEquipment.getProtectiveEquipment(), user.getRegion());
            regionProtectionEquipmentQuantities.put(requestProtectiveEquipment.getId(), protectiveEquipmentStatus.getQuantity());
            final int protectiveEquipmentDifference = protectiveEquipmentStatus.getQuantity() - requestProtectiveEquipment.getApprovedQuantity();
            if (protectiveEquipmentDifference < 0 && hasEnoughProtectiveEquipments) {
                hasEnoughProtectiveEquipments = false;
            }
        }
        return new ModelAndView("showDetailedRegionProtectiveEquipmentRequest").addObject("protectiveEquipmentRequest", protectiveEquipmentRequest).addObject("requestProtectiveEquipments", requestProtectiveEquipments).addObject("userInfo", userInfo).addObject("regionProtectionEquipmentQuantities", regionProtectionEquipmentQuantities).addObject("hasEnoughProtectiveEquipments", hasEnoughProtectiveEquipments);
    }

    /**
     * Opens form for answering the protective equipment request.
     *
     * @param protectiveEquipmentRequestId  - Id of the protective equipment request which is going to be answered.
     * @param hasEnoughProtectiveEquipments - Tells if the region has enough protective equipments to be able to approve the answer or not.
     * @param authentication                - Authentication information of the user calling this action.
     * @param redirect                      - Used to save redirect messages.
     * @return - View with the form for answering the protective equipment request. In case of error, redirects user to the previous action.
     */
    @GetMapping("/answerProtectiveEquipmentRequest")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView answerProtectiveEquipmentRequest(@RequestParam final int protectiveEquipmentRequestId, @RequestParam final boolean hasEnoughProtectiveEquipments, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequestId);
        if (protectiveEquipmentRequest == null) {
            redirect.addFlashAttribute("error", "Id požadavku na ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getRegion().getId() != protectiveEquipmentRequest.getOrganization().getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit žádosti cizích krajů!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 2) {
            redirect.addFlashAttribute("error", "Nemůžete reagovat na žádost, která už byla přijata nebo zamítnuta!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        return new ModelAndView("answerProtectiveEquipmentRequest").addObject(protectiveEquipmentRequest).addObject("hasEnoughProtectiveEquipments", hasEnoughProtectiveEquipments);
    }

    @PostMapping("/saveAnsweredProtectiveEquipmentRequest")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView saveAnsweredProtectiveEquipmentRequest(@Valid final ProtectiveEquipmentRequest protectiveEquipmentRequest, final BindingResult result, @RequestParam final boolean hasEnoughProtectiveEquipments, @RequestParam final boolean approved, @RequestParam(required = false, defaultValue = "0") final long billId, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("answerProtectiveEquipmentRequest").addObject(protectiveEquipmentRequest).addObject("hasEnoughProtectiveEquipments", hasEnoughProtectiveEquipments);
        }
        if (protectiveEquipmentRequest.getRegionMessage() == null || protectiveEquipmentRequest.getRegionMessage().isBlank()) {
            return new ModelAndView("answerProtectiveEquipmentRequest").addObject(protectiveEquipmentRequest).addObject("hasEnoughProtectiveEquipments", hasEnoughProtectiveEquipments).addObject("error", "Prosím vyplňte zprávu.");
        }
        final ProtectiveEquipmentRequest answeredProtectiveEquipmentRequest = protectiveEquipmentRequestRepository.findById(protectiveEquipmentRequest.getId());
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (answeredProtectiveEquipmentRequest.getOrganization().getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete reagovat na žádosti cizích krajů!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        if (answeredProtectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 2) {
            redirect.addFlashAttribute("error", "Nemůžete reagovat na žádost, která už byla přijata nebo zamítnuta!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }

        answeredProtectiveEquipmentRequest.setRegionMessage(protectiveEquipmentRequest.getRegionMessage());
        if (approved) {
            if (billId < 1) {
                return new ModelAndView("answerProtectiveEquipmentRequest").addObject(protectiveEquipmentRequest).addObject("hasEnoughProtectiveEquipments", hasEnoughProtectiveEquipments).addObject("error", "Prosím vyplňte číslo dokladu.");
            }
            final ProtectiveEquipmentRequestStatus protectiveEquipmentRequestStatus = protectiveEquipmentRequestStatusRepository.findById(3);
            //Creates the new transfer of protective equipments.
            final ProtectiveEquipmentTransfer protectiveEquipmentTransfer = new ProtectiveEquipmentTransfer(user.getRegion(), answeredProtectiveEquipmentRequest.getOrganization(), protectiveEquipmentTransferStatusRepository.findById(1), protectiveEquipmentTransferTypeRepository.findById(1));
            protectiveEquipmentTransfer.setId(protectiveEquipmentTransferRepository.save(protectiveEquipmentTransfer).getId());
            final List<RequestProtectiveEquipment> requestProtectiveEquipments = requestProtectiveEquipmentRepository.findByProtectiveEquipmentRequest(protectiveEquipmentRequest);
            final List<ProtectiveEquipmentStatus> protectiveEquipmentStatuses = new ArrayList<>();
            final List<TransferProtectiveEquipment> transferProtectiveEquipmentsList = new ArrayList<>();
            final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(4);
            final List<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEvents = new ArrayList<>();
            for (final RequestProtectiveEquipment requestProtectiveEquipment : requestProtectiveEquipments) {
                final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findByProtectiveEquipmentAndRegion(requestProtectiveEquipment.getProtectiveEquipment(), user.getRegion());
                final int oldAmount = protectiveEquipmentStatus.getQuantity();
                final int quantity = oldAmount - requestProtectiveEquipment.getApprovedQuantity();
                if (quantity < 0) {
                    redirect.addFlashAttribute("error", "Nemůžete schválit žádost na ochranné pomůcky, pro kterou kraj nemá dostatek ochranných pomůcek!");
                    protectiveEquipmentTransferRepository.delete(protectiveEquipmentTransfer);
                    return new ModelAndView("redirect:/showDetailedRegionProtectiveEquipmentRequest?protectiveEquipmentRequestId" + answeredProtectiveEquipmentRequest);
                }
                protectiveEquipmentStatus.setQuantity(quantity);
                protectiveEquipmentStatusChangeEvents.add(new ProtectiveEquipmentStatusChangeEvent(new Date(), oldAmount, quantity, protectiveEquipmentStatus, user, protectiveEquipmentStatusChangeEventType, user.getRegion(), billId));
                protectiveEquipmentStatuses.add(protectiveEquipmentStatus);
                final TransferProtectiveEquipment transferProtectiveEquipment = new TransferProtectiveEquipment(requestProtectiveEquipment.getApprovedQuantity(), requestProtectiveEquipment.getProtectiveEquipment(), protectiveEquipmentTransfer);
                transferProtectiveEquipmentsList.add(transferProtectiveEquipment);
            }
            transferProtectiveEquipmentRepository.saveAll(transferProtectiveEquipmentsList);
            protectiveEquipmentStatusRepository.saveAll(protectiveEquipmentStatuses);
            answeredProtectiveEquipmentRequest.setProtectiveEquipmentRequestStatus(protectiveEquipmentRequestStatus);
            protectiveEquipmentRequestRepository.save(answeredProtectiveEquipmentRequest);
            protectiveEquipmentStatusChangeEventRepository.saveAll(protectiveEquipmentStatusChangeEvents);
            redirect.addFlashAttribute("success", "Požadavek byl úspěšně schválen a na základě něj byl vytvořen přesun ochranných pomůcek číslo " + protectiveEquipmentTransfer.getId() + ".");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        } else {
            final ProtectiveEquipmentRequestStatus protectiveEquipmentRequestStatus = protectiveEquipmentRequestStatusRepository.findById(4);
            answeredProtectiveEquipmentRequest.setProtectiveEquipmentRequestStatus(protectiveEquipmentRequestStatus);
            protectiveEquipmentRequestRepository.save(answeredProtectiveEquipmentRequest);
            redirect.addFlashAttribute("success", "Požadavek byl úspěšně zamítnut.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
    }

    /**
     * Opens form for changing aprroved quantity of protective equipment request.
     *
     * @param requestProtectiveEquipmentId - Id of the protective equipment in a request, which quantity is going to be changed.
     * @param authentication               - Authentication information of the user calling this action.
     * @param redirect                     - Used to save redirect messages.
     * @return - View with form for approving quantity. In case of error redirects user to a previous action.
     */
    @GetMapping("/changeRequestProtectiveEquipmentApprovedQuantity")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView changeRequestProtectiveEquipmentApprovedQuantity(@RequestParam final int requestProtectiveEquipmentId, final Authentication authentication, final RedirectAttributes redirect) {
        final RequestProtectiveEquipment requestProtectiveEquipment = requestProtectiveEquipmentRepository.findById(requestProtectiveEquipmentId);
        if (requestProtectiveEquipment == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky v požadavku je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = requestProtectiveEquipment.getProtectiveEquipmentRequest();
        if (protectiveEquipmentRequest.getOrganization().getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete přistupovat k požadavkům cizích krajů!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 2) {
            redirect.addFlashAttribute("error", "Nemůžete měnit schválené množství už u zpracované žádosti!");
            return new ModelAndView("redirect:/showDetailedRegionProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
        }
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findByProtectiveEquipmentAndRegion(requestProtectiveEquipment.getProtectiveEquipment(), user.getRegion());
        return new ModelAndView("changeRequestProtectiveEquipmentApprovedQuantity").addObject("requestProtectiveEquipment", requestProtectiveEquipment).addObject("protectiveEquipmentStatus", protectiveEquipmentStatus);
    }

    /**
     * Saves changed approved quantity to the database.
     *
     * @param requestProtectiveEquipment - Request protective equipment, which quantity is going to be saved.
     * @param result                     - Result of automated validation.
     * @param authentication             - Authentication information of the user calling this action.
     * @param redirect                   - Used to save redirect messages.
     * @return - Redirects user to the action of showing the detailed protective equipment request which approved quantity was changed. In case of validation error returns the previous view. In case of other errors redirects user to previous actions.
     */
    @PostMapping("/saveChangedRequestProtectiveEquipmentApprovedQuantity")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView saveChangedRequestProtectiveEquipmentApprovedQuantity(@Valid final RequestProtectiveEquipment requestProtectiveEquipment, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final ProtectiveEquipmentRequest protectiveEquipmentRequest = requestProtectiveEquipment.getProtectiveEquipmentRequest();
        if (protectiveEquipmentRequest.getOrganization().getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete přistupovat k požadavkům cizích krajů!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentRequests");
        }
        if (protectiveEquipmentRequest.getProtectiveEquipmentRequestStatus().getId() != 2) {
            redirect.addFlashAttribute("error", "Nemůžete měnit schválené množství už u zpracované žádosti!");
            return new ModelAndView("redirect:/showDetailedRegionProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
        }
        if (result.hasErrors()) {
            final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findByProtectiveEquipmentAndRegion(requestProtectiveEquipment.getProtectiveEquipment(), user.getRegion());
            return new ModelAndView("changeRequestProtectiveEquipmentApprovedQuantity").addObject("requestProtectiveEquipment", requestProtectiveEquipment).addObject("protectiveEquipmentStatus", protectiveEquipmentStatus);
        }
        final RequestProtectiveEquipment savedRequestProtectiveEquipment = requestProtectiveEquipmentRepository.findById(requestProtectiveEquipment.getId());
        if (requestProtectiveEquipment.getApprovedQuantity() > savedRequestProtectiveEquipment.getQuantity()) {
            final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findByProtectiveEquipmentAndRegion(requestProtectiveEquipment.getProtectiveEquipment(), user.getRegion());
            return new ModelAndView("changeRequestProtectiveEquipmentApprovedQuantity").addObject("requestProtectiveEquipment", requestProtectiveEquipment).addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("error", "Množství schválených ochranných pomůcek nemůže být větší než žádané množství. Prosím zmenšete ho.");
        }
        savedRequestProtectiveEquipment.setApprovedQuantity(requestProtectiveEquipment.getApprovedQuantity());
        requestProtectiveEquipmentRepository.save(savedRequestProtectiveEquipment);
        redirect.addFlashAttribute("success", savedRequestProtectiveEquipment.getApprovedQuantity() + "ochranných pomůcek typu" + savedRequestProtectiveEquipment.getProtectiveEquipment().getName() + " bylo úspěšně schváleno.");
        return new ModelAndView("redirect:/showDetailedRegionProtectiveEquipmentRequest?protectiveEquipmentRequestId=" + protectiveEquipmentRequest.getId());
    }
}
