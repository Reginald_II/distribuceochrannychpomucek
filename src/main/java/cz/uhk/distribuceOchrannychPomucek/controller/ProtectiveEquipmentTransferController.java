package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.*;
import cz.uhk.distribuceOchrannychPomucek.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Contains actions working with protective equipment transfers.
 */
@Controller
@Secured({"ROLE_RegionAdmin", "ROLE_RegionUser", "ROLE_OrganizationUser", "ROLE_OrganizationAdmin"})
public class ProtectiveEquipmentTransferController {
    final AppUserRepository appUserRepository;
    final ProtectiveEquipmentTransferRepository protectiveEquipmentTransferRepository;
    final TransferProtectiveEquipmentRepository transferProtectiveEquipmentRepository;
    final ProtectiveEquipmentTransferStatusRepository protectiveEquipmentTransferStatusRepository;
    final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository;
    final private ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository;
    final private ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository;

    public ProtectiveEquipmentTransferController(final AppUserRepository appUserRepository, final ProtectiveEquipmentTransferRepository protectiveEquipmentTransferRepository, final TransferProtectiveEquipmentRepository transferProtectiveEquipmentRepository, final ProtectiveEquipmentTransferStatusRepository protectiveEquipmentTransferStatusRepository, final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository, final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository, final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository) {
        this.appUserRepository = appUserRepository;
        this.protectiveEquipmentTransferRepository = protectiveEquipmentTransferRepository;
        this.transferProtectiveEquipmentRepository = transferProtectiveEquipmentRepository;
        this.protectiveEquipmentTransferStatusRepository = protectiveEquipmentTransferStatusRepository;
        this.protectiveEquipmentStatusRepository = protectiveEquipmentStatusRepository;
        this.protectiveEquipmentStatusChangeEventRepository = protectiveEquipmentStatusChangeEventRepository;
        this.protectiveEquipmentStatusChangeEventTypeRepository = protectiveEquipmentStatusChangeEventTypeRepository;
    }

    /**
     * Shows protective equipment transfers in the user's region.
     *
     * @param authentication                      - Authentication information of the user calling this action.
     * @param protectiveEquipmentTransferStatusId - Used to limit the result by the status of transfers. Optional parameter.
     * @param currentPage                         - Current page of the paged result. Starts at 0.
     * @return - View showing the protective equipment transfers.
     */
    @GetMapping("/showRegionProtectiveEquipmentTransfers")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showRegionProtectiveEquipmentTransfers(final Authentication authentication, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentTransferStatusId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentTransfer> protectiveEquipmentTransferPage;
        final int itemsPerPage = 5;
        if (protectiveEquipmentTransferStatusId < 1) {
            protectiveEquipmentTransferPage = protectiveEquipmentTransferRepository.findByRegion(user.getRegion(), PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.DESC, "id")));
        } else {
            protectiveEquipmentTransferPage = protectiveEquipmentTransferRepository.findByRegionAndProtectiveEquipmentTransferStatusId(user.getRegion(), protectiveEquipmentTransferStatusId, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.DESC, "id")));
        }
        final int numberOfPages = protectiveEquipmentTransferPage.getTotalPages();
        final List<ProtectiveEquipmentTransfer> protectiveEquipmentTransfers = protectiveEquipmentTransferPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentTransferStatus> protectiveEquipmentTransfersStatuses = protectiveEquipmentTransferStatusRepository.findAll();
        return new ModelAndView("showRegionProtectiveEquipmentTransfers").addObject("protectiveEquipmentTransfersStatuses", protectiveEquipmentTransfersStatuses).addObject("protectiveEquipmentTransfers", protectiveEquipmentTransfers).addObject("currentPage", currentPage).addObject("pages", pages).addObject("protectiveEquipmentTransferStatusId", protectiveEquipmentTransferStatusId);
    }

    /**
     * Shows detailed information of the protective equipment transfer of the region.
     *
     * @param protectiveEquipmentTransferId - Id of the transfer which information are going to be shown.
     * @param authentication                - Authentication information of the user calling this action.
     * @param redirect                      - Used to save redirect messages.
     * @return - View with the information about the selected transfer. In case of error, redirects user to the previous action.
     */
    @GetMapping("/showRegionDetailedProtectiveEquipmentTransfer")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showRegionDetailedProtectiveEquipmentTransfer(@RequestParam final int protectiveEquipmentTransferId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentTransfer protectiveEquipmentTransfer = protectiveEquipmentTransferRepository.findById(protectiveEquipmentTransferId);
        if (protectiveEquipmentTransfer == null) {
            redirect.addFlashAttribute("error", "Id přesunu ochranných pomůcek je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentTransfers");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentTransfer.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit přesun ochranných pomůcek cizího kraje!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipmentTransfers");
        }
        final List<TransferProtectiveEquipment> transferProtectiveEquipments = transferProtectiveEquipmentRepository.findByProtectiveEquipmentTransfer(protectiveEquipmentTransfer);
        return new ModelAndView("showRegionDetailedProtectiveEquipmentTransfer").addObject("protectiveEquipmentTransfer", protectiveEquipmentTransfer).addObject("transferProtectiveEquipments", transferProtectiveEquipments);
    }

    /**
     * Sets the status of the selected protective equipment transfer to sent.
     *
     * @param protectiveEquipmentTransferId - Id of the protective equipment transfer which is going to be sent.
     * @param authentication                - Authentication information of the user calling this action.
     * @param redirect                      - Used to save redirect messages.
     * @return - Redirects user to the actionn of showing region protective equipment transfers.
     */
    @GetMapping("/sendRegionProtectiveEquipmentTransfer")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public RedirectView sendRegionProtectiveEquipmentTransfer(@RequestParam final int protectiveEquipmentTransferId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentTransfer protectiveEquipmentTransfer = protectiveEquipmentTransferRepository.findById(protectiveEquipmentTransferId);
        if (protectiveEquipmentTransfer == null) {
            redirect.addFlashAttribute("error", "Id přesunu ochranných pomůcek je neplatné.");
            return new RedirectView("/showRegionProtectiveEquipmentTransfers", true);
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentTransfer.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit přesun ochranných pomůcek cizího kraje!");
            return new RedirectView("/showRegionProtectiveEquipmentTransfers", true);
        }
        if (protectiveEquipmentTransfer.getProtectiveEquipmentTransferStatus().getId() != 1) {
            redirect.addFlashAttribute("error", "Ochranné pomůcky jsou už odeslány!");
            return new RedirectView("/showRegionProtectiveEquipmentTransfers", true);
        }
        protectiveEquipmentTransfer.setProtectiveEquipmentTransferStatus(protectiveEquipmentTransferStatusRepository.findById(2));
        protectiveEquipmentTransferRepository.save(protectiveEquipmentTransfer);
        redirect.addFlashAttribute("success", "Ochranné pomůcky byly úspěšně odeslány.");
        return new RedirectView("/showRegionProtectiveEquipmentTransfers", true);
    }

    /**
     * Shows protective equipment transfers of the user's organization.
     *
     * @param authentication                      - Authentication information of the user.
     * @param protectiveEquipmentTransferStatusId - Id of the transfer status used to filter the result. Optional parameter.
     * @param currentPage                         - Current page of the paged result. Starts at 0.
     * @return - View with the list of the transfers.
     */
    @GetMapping("/showOrganizationProtectiveEquipmentTransfers")
    @Secured({"ROLE_OrganizationUser", "ROLE_OrganizationAdmin"})
    public ModelAndView showOrganizationProtectiveEquipmentTransfers(final Authentication authentication, @RequestParam(required = false, defaultValue = "0") final int protectiveEquipmentTransferStatusId, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<ProtectiveEquipmentTransfer> protectiveEquipmentTransfersPage;
        final int itemsAmount = 5;
        if (protectiveEquipmentTransferStatusId < 1) {
            protectiveEquipmentTransfersPage = protectiveEquipmentTransferRepository.findByOrganization(user.getOrganization(), PageRequest.of(currentPage, itemsAmount, Sort.by(Sort.Direction.DESC, "id")));
        } else {
            protectiveEquipmentTransfersPage = protectiveEquipmentTransferRepository.findByOrganizationAndProtectiveEquipmentTransferStatusId(user.getOrganization(), protectiveEquipmentTransferStatusId, PageRequest.of(currentPage, itemsAmount, Sort.by(Sort.Direction.DESC, "id")));
        }
        final int numberOfPages = protectiveEquipmentTransfersPage.getTotalPages();
        final List<ProtectiveEquipmentTransfer> protectiveEquipmentTransfers = protectiveEquipmentTransfersPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<ProtectiveEquipmentTransferStatus> protectiveEquipmentTransferStatuses = protectiveEquipmentTransferStatusRepository.findAll();
        return new ModelAndView("showOrganizationProtectiveEquipmentTransfers").addObject("protectiveEquipmentTransfers", protectiveEquipmentTransfers).addObject("protectiveEquipmentTransferStatuses", protectiveEquipmentTransferStatuses).addObject("protectiveEquipmentTransferStatusId", protectiveEquipmentTransferStatusId).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Shows detailed information about selected protective equipment transfer in the organization.
     *
     * @param protectiveEquipmentTransferId - Id of the protective equipment transfer which information are going to be shown.
     * @param authentication                - Authentication information of the user.
     * @param redirect                      - Used to save redirect messages.
     * @return - View showing the detailed information about the selected transfer. In case of error, redirects user to the previous action.
     */
    @GetMapping("/showOrganizationDetailedProtectiveEquipmentTransfer")
    @Secured({"ROLE_OrganizationUser", "ROLE_OrganizationAdmin"})
    public ModelAndView showOrganizationDetailedProtectiveEquipmentTransfer(@RequestParam final int protectiveEquipmentTransferId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentTransfer protectiveEquipmentTransfer = protectiveEquipmentTransferRepository.findById(protectiveEquipmentTransferId);
        if (protectiveEquipmentTransfer == null) {
            redirect.addFlashAttribute("error", "Id přesunu ochranných pomůcek je neplatné.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentTransfers");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentTransfer.getOrganization().getId() != user.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit přesun ochranných pomůcek cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentTransfers");
        }
        final List<TransferProtectiveEquipment> transferProtectiveEquipments = transferProtectiveEquipmentRepository.findByProtectiveEquipmentTransfer(protectiveEquipmentTransfer);
        return new ModelAndView("showOrganizationDetailedProtectiveEquipmentTransfer").addObject("protectiveEquipmentTransfer", protectiveEquipmentTransfer).addObject("transferProtectiveEquipments", transferProtectiveEquipments);
    }

    /**
     * Changes the status of the protective equipment transfer to received. Also updates the protective equipment status of the receiving organization and creates history event based on the receiving.
     *
     * @param protectiveEquipmentTransferId - Protective equipment transfer to be received.
     * @param billId                        - Id of the bill or similar document.
     * @param authentication                - Authentication information of the user.
     * @param redirect                      - Used to save redirect messages.
     * @return - Redirects user to other action.
     */
    @PostMapping("/receiveOrganizationProtectiveEquipmentTransfer")
    @Secured({"ROLE_OrganizationUser", "ROLE_OrganizationAdmin"})
    public ModelAndView receiveOrganizationProtectiveEquipmentTransfer(@RequestParam final int protectiveEquipmentTransferId, @RequestParam final long billId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentTransfer protectiveEquipmentTransfer = protectiveEquipmentTransferRepository.findById(protectiveEquipmentTransferId);
        if (protectiveEquipmentTransfer == null) {
            redirect.addFlashAttribute("error", "Id přesunu ochranných pomůcek je neplatné.");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentTransfers");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentTransfer.getOrganization().getId() != user.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit přesun ochranných pomůcek cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentTransfers");
        }
        if (protectiveEquipmentTransfer.getProtectiveEquipmentTransferStatus().getId() != 2) {
            redirect.addFlashAttribute("error", "Ochranné pomůcky už jsou převzaty!");
            return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentTransfers");
        }
        final List<TransferProtectiveEquipment> transferProtectiveEquipments = transferProtectiveEquipmentRepository.findByProtectiveEquipmentTransfer(protectiveEquipmentTransfer);
        if (billId < 1) {
            return new ModelAndView("showOrganizationDetailedProtectiveEquipmentTransfer").addObject("protectiveEquipmentTransfer", protectiveEquipmentTransfer).addObject("transferProtectiveEquipments", transferProtectiveEquipments).addObject("error", "Prosím vyplňte číslo dokladu spojené s přijetím dodávky.");
        }
        final List<ProtectiveEquipmentStatus> protectiveEquipmentStatuses = new ArrayList<>();
        final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(5);
        final List<ProtectiveEquipmentStatusChangeEvent> protectiveEquipmentStatusChangeEvents = new ArrayList<>();
        //Saves the changes by receiving the transfer, like protective equipment status quantity of the receiving organization, into its instances. Also saves the history events.
        for (final TransferProtectiveEquipment transferProtectiveEquipment : transferProtectiveEquipments) {
            final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findByProtectiveEquipmentAndOrganization(transferProtectiveEquipment.getProtectiveEquipment(), user.getOrganization());
            final int oldAmount = protectiveEquipmentStatus.getQuantity();
            protectiveEquipmentStatus.addAmount(transferProtectiveEquipment.getQuantity());
            protectiveEquipmentStatuses.add(protectiveEquipmentStatus);
            protectiveEquipmentStatusChangeEvents.add(new ProtectiveEquipmentStatusChangeEvent(new Date(), oldAmount, protectiveEquipmentStatus.getQuantity(), protectiveEquipmentStatus, user, protectiveEquipmentStatusChangeEventType, user.getOrganization(), billId));
        }
        protectiveEquipmentStatusRepository.saveAll(protectiveEquipmentStatuses);
        protectiveEquipmentTransfer.setProtectiveEquipmentTransferStatus(protectiveEquipmentTransferStatusRepository.findById(3));
        protectiveEquipmentTransferRepository.save(protectiveEquipmentTransfer);
        protectiveEquipmentStatusChangeEventRepository.saveAll(protectiveEquipmentStatusChangeEvents);
        redirect.addFlashAttribute("success", "Ochranné pomůcky byly úspěšně přijaty.");
        return new ModelAndView("redirect:/showOrganizationProtectiveEquipmentTransfers");
    }
}
