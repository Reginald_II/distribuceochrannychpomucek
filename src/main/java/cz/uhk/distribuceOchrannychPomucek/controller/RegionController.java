package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.CityRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.OrganizationRepository;
import cz.uhk.distribuceOchrannychPomucek.repository.RegionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Contains actions working with regions.
 */
@Controller
@Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin", "ROLE_RegionUser"})
public class RegionController {

    private final RegionRepository regionRepository;
    private final CityRepository cityRepository;
    private final AppUserRepository appUserRepository;
    private final OrganizationRepository organizationRepository;

    public RegionController(final RegionRepository regionRepository, final CityRepository cityRepository, final AppUserRepository appUserRepository, final OrganizationRepository organizationRepository) {
        this.regionRepository = regionRepository;
        this.cityRepository = cityRepository;
        this.appUserRepository = appUserRepository;
        this.organizationRepository = organizationRepository;
    }

    /**
     * Shows information about the user's region.
     *
     * @param authentication - Authentication information of the user.
     * @return - View showing the region information.
     */
    @GetMapping("/showRegion")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showRegion(final Authentication authentication) {
        final Region region = appUserRepository.findByLogin(authentication.getName()).getRegion();
        return new ModelAndView("showRegion").addObject("region", region);
    }

    /**
     * Shows detailed information about a selected region by global admin.
     *
     * @param regionId - Id of the region which information are going to be shown.
     * @param redirect - Used to save redirect messages.
     * @return - View with the region information. In case of error, redirects user to the previous action.
     */
    @GetMapping("/showDetailedRegion")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView showDetailedRegion(@RequestParam final int regionId, final RedirectAttributes redirect) {
        final Region region = regionRepository.findById(regionId);
        if (region == null) {
            redirect.addFlashAttribute("error", "Id kraje je neplatné.");
            return new ModelAndView("redirect:/showRegions");
        }
        return new ModelAndView("showDetailedRegion").addObject("region", region);
    }

    /**
     * Shows all regions.
     *
     * @param name            - Filters regions by their name or by the name of their capital city. Optional parameter.
     * @param searchSelection - Defines if the regions are going to be filtered by their name (1) or by the name of their capital city (2).
     * @param currentPage     - Current page of the paging result. Starts at 0.
     * @return - View showing the regions.
     */
    @GetMapping("/showRegions")
    @Secured({"ROLE_GlobalAdmin"})
    public ModelAndView showRegions(@RequestParam(required = false) final String name, @RequestParam(required = false, defaultValue = "1") final int searchSelection, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final Page<Region> regionsPage;
        final int numberOfPages;
        final int itemsPerPage = 5;
        if (name == null || name.isBlank()) {
            regionsPage = regionRepository.findAll(PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "name")));

        } else {
            if (searchSelection == 1) {
                regionsPage = regionRepository.findAllByNameContaining(name, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "name")));

            } else if (searchSelection == 2) {
                regionsPage = regionRepository.findAllByCityNameContaining(name, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "name")));
            }
            //In case that there is no selection chosen for some reason, find all instead.
            else {
                regionsPage = regionRepository.findAll(PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "name")));
            }
        }
        numberOfPages = regionsPage.getTotalPages();
        final List<Region> regions = regionsPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        return new ModelAndView("showRegions").addObject("regions", regions).addObject("pages", pages).addObject("currentPage", currentPage).addObject("protectiveEquipmentName", name).addObject("searchSelection", searchSelection).addObject("name", name);
    }

    /**
     * Opens form for creating a new region.
     *
     * @return - View with the form.
     */
    @GetMapping("/createRegion")
    @Secured({"ROLE_GlobalAdmin"})
    public ModelAndView createRegion() {
        final Region region = new Region();
        return new ModelAndView("createRegion").addObject(region);
    }

    /**
     * Saves new region to the database.
     *
     * @param region   - Region to be saved.
     * @param result   - Result of the automated validation.
     * @param redirect - Used to save redirect messages.
     * @return - In case of validation error returns the previous view. In csae of success, redirects user to other action.
     */
    @PostMapping("/saveNewRegion")
    @Secured({"ROLE_GlobalAdmin"})
    public ModelAndView saveNewRegion(@Valid final Region region, final BindingResult result, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("createRegion").addObject(region);
        }
        if (!cityRepository.existsByPsc(region.getCity().getPsc())) {
            cityRepository.save(region.getCity());
        }
        regionRepository.save(region);
        redirect.addFlashAttribute("success", "Kraj " + region.getName() + " byl úspěšně uložen do databáze");
        return new ModelAndView("redirect:/showRegions");
    }

    /**
     * Opens form for changing the user's region information.
     *
     * @param authentication - Authentication information of the user.
     * @return - View with the form.
     */
    @GetMapping("/changeRegion")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView changeRegion(final Authentication authentication) {
        final Region region = regionRepository.findById(appUserRepository.findByLogin(authentication.getName()).getRegion().getId());
        return new ModelAndView("changeRegion").addObject(region);
    }

    /**
     * Updates the changed region in the database.
     *
     * @param region   - Region with the changes to be updated.
     * @param result   - Result of the automated validation.
     * @param redirect - Used to save redirect messages.
     * @return - In case of validation error returns the previous view with the form. In case of success redirects user to different action.
     */
    @PostMapping("/saveChangedRegion")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView saveChangedRegion(@Valid final Region region, final BindingResult result, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeRegion").addObject(region);
        }
        if (region.getId() < 1) {
            redirect.addFlashAttribute("error", "Chybí platné ID kraje!");
            return new ModelAndView("redirect:/showRegion");
        }
        if (!cityRepository.existsByPsc(region.getCity().getPsc())) {
            cityRepository.save(region.getCity());
        }
        regionRepository.save(region);
        redirect.addFlashAttribute("success", "Kraj " + region.getName() + " byl úspěšně změněn.");
        return new ModelAndView("redirect:/showRegion");
    }

    /**
     * Opens form for changing region by global admin.
     *
     * @param regionId - Id of the region which information are going to be changed.
     * @param redirect - Used to save redirect messages.
     * @return - View with the form. In case of error, redirects user to the previous action.
     */
    @GetMapping("/changeOtherRegion")
    @Secured({"ROLE_GlobalAdmin"})
    public ModelAndView changeOtherRegion(@RequestParam final int regionId, final RedirectAttributes redirect) {
        final Region region = regionRepository.findById(regionId);
        if (region == null) {
            redirect.addFlashAttribute("error", "Id kraje je neplatné.");
            return new ModelAndView("redirect:/showRegions");
        }
        return new ModelAndView("changeOtherRegion").addObject(region);
    }

    /**
     * Updates the changed region in the database.
     *
     * @param region   - Region which is going to be updated.
     * @param result   - Result of the automated validation.
     * @param redirect - Used to save redirect messages.
     * @return - Redirects user to other action. In case of validation error, returns the previous view with the form.
     */
    @PostMapping("/saveOtherChangedRegion")
    @Secured({"ROLE_GlobalAdmin"})
    public ModelAndView saveOtherChangedRegion(@Valid final Region region, final BindingResult result, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeOtherRegion").addObject(region);
        }
        if (region.getId() < 1) {
            redirect.addFlashAttribute("error", "Chybí platné ID kraje!");
            return new ModelAndView("redirect:/showRegions");
        }
        if (!cityRepository.existsByPsc(region.getCity().getPsc())) {
            cityRepository.save(region.getCity());
        }
        regionRepository.save(region);
        redirect.addFlashAttribute("success", "Kraj " + region.getName() + " byl úspěšně změněn.");
        return new ModelAndView("redirect:/showRegions");
    }

    /**
     * Shows organizations of the selected region.
     *
     * @param authentication  - Authentication information of the user.
     * @param name            - Organization name or name of the capital city used for filtering the result. Optional parameter.
     * @param searchSelection - Tells if the user wishes to filter the organizations by their name (1) or by the name of their capital city (2).
     * @param currentPage     - Current page of the paging system. Starts at 0.
     * @return - View showing the organizations.
     */
    @GetMapping("/showRegionOrganizations")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showRegionOrganizations(final Authentication authentication, @RequestParam(required = false) final String name, @RequestParam(required = false, defaultValue = "1") final int searchSelection, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final Region userRegion = appUserRepository.findByLogin(authentication.getName()).getRegion();
        final Page<Organization> organizationsPage;
        final int numberOfPages;
        final int itemsPerPage = 5;
        if (name == null || name.isBlank()) {
            organizationsPage = organizationRepository.findAllByRegion(userRegion, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "city.name")));

        } else {
            if (searchSelection == 1) {
                organizationsPage = organizationRepository.findAllByRegionAndNameContaining(userRegion, name, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "city.name")));

            } else if (searchSelection == 2) {
                organizationsPage = organizationRepository.findAllByRegionAndCityNameContaining(userRegion, name, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "city.name")));
            }
            //In case that there is no selection chosen for some reason, find all instead.
            else {
                organizationsPage = organizationRepository.findAllByRegion(userRegion, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "city.name")));
            }
        }
        numberOfPages = organizationsPage.getTotalPages();
        final List<Organization> organizations = organizationsPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        return new ModelAndView("showRegionOrganizations").addObject("organizations", organizations).addObject("pages", pages).addObject("currentPage", currentPage).addObject("searchSelection", searchSelection).addObject("name", name);
    }

    /**
     * Shows detailed information of the selected organization.
     *
     * @param organizationId - Id of the organization which information are going to be shown.
     * @param authentication - Authentication information of the user.
     * @param redirect       - Used to save redirect messages.
     * @return - View with the information about the organization. In case of error redirects user to the previous action.
     */
    @GetMapping("/showDetailedOrganizationInfo")
    @Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
    public ModelAndView showDetailedOrganizationInfo(@RequestParam final int organizationId, final Authentication authentication, final RedirectAttributes redirect) {
        final Organization organization = organizationRepository.findById(organizationId);
        if (organization == null) {
            redirect.addFlashAttribute("error", "Id organizace je neplatné.");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getRegion() == null || user.getRegion().getId() != organization.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit podrobné informace cizí organizace.");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        return new ModelAndView("showDetailedOrganizationInfo").addObject("organization", organization);
    }
}
