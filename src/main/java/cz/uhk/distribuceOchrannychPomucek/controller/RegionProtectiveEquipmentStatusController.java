package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.*;
import cz.uhk.distribuceOchrannychPomucek.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Contains actions working with protective equipment statuses of regions.
 */
@Controller
@Secured({"ROLE_RegionAdmin", "ROLE_RegionUser"})
public class RegionProtectiveEquipmentStatusController {
    final private ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository;
    final private ProtectiveEquipmentRepository protectiveEquipmentRepository;
    final private AppUserRepository appUserRepository;
    final private ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository;
    final private ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository;
    final private OrganizationRepository organizationRepository;
    final private ProtectiveEquipmentTransferRepository protectiveEquipmentTransferRepository;
    final private TransferProtectiveEquipmentRepository transferProtectiveEquipmentRepository;
    final private ProtectiveEquipmentTransferStatusRepository protectiveEquipmentTransferStatusRepository;
    final private ProtectiveEquipmentTransferTypeRepository protectiveEquipmentTransferTypeRepository;

    public RegionProtectiveEquipmentStatusController(final ProtectiveEquipmentStatusRepository protectiveEquipmentStatusRepository, final ProtectiveEquipmentRepository protectiveEquipmentRepository, final AppUserRepository appUserRepository, final ProtectiveEquipmentStatusChangeEventRepository protectiveEquipmentStatusChangeEventRepository, final ProtectiveEquipmentStatusChangeEventTypeRepository protectiveEquipmentStatusChangeEventTypeRepository, final OrganizationRepository organizationRepository, final ProtectiveEquipmentTransferRepository protectiveEquipmentTransferRepository, final TransferProtectiveEquipmentRepository transferProtectiveEquipmentRepository, final ProtectiveEquipmentTransferStatusRepository protectiveEquipmentTransferStatusRepository, final ProtectiveEquipmentTransferTypeRepository protectiveEquipmentTransferTypeRepository) {
        this.protectiveEquipmentStatusRepository = protectiveEquipmentStatusRepository;
        this.protectiveEquipmentRepository = protectiveEquipmentRepository;
        this.appUserRepository = appUserRepository;
        this.protectiveEquipmentStatusChangeEventRepository = protectiveEquipmentStatusChangeEventRepository;
        this.protectiveEquipmentStatusChangeEventTypeRepository = protectiveEquipmentStatusChangeEventTypeRepository;
        this.organizationRepository = organizationRepository;
        this.protectiveEquipmentTransferRepository = protectiveEquipmentTransferRepository;
        this.transferProtectiveEquipmentRepository = transferProtectiveEquipmentRepository;
        this.protectiveEquipmentTransferStatusRepository = protectiveEquipmentTransferStatusRepository;
        this.protectiveEquipmentTransferTypeRepository = protectiveEquipmentTransferTypeRepository;
    }

    /**
     * Shows protective equipments of the user's region. Also checks and adds the protective equipment statuses to the region if they are defined, but not in the region's list.
     *
     * @param authentication          - Authentication information of the user.
     * @param protectiveEquipmentName - Filters protective equipments by their name. Optional parameter.
     * @param currentPage             - Current page of the paged result. Starts at 0.
     * @return - View showing the region protective equipments.
     */
    @GetMapping("/showRegionProtectiveEquipments")
    public ModelAndView showRegionProtectiveEquipments(final Authentication authentication, @RequestParam(required = false) final String protectiveEquipmentName, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final int protectiveEquipmentStatusesAmount = protectiveEquipmentStatusRepository.countAllByRegion(user.getRegion());
        //Checks if the protective equipment statuses exist. If not, creates the default status of each protective equipment in case that they weren't initialized already.
        if (protectiveEquipmentStatusesAmount < 1) {
            final List<ProtectiveEquipment> protectiveEquipments = protectiveEquipmentRepository.findAll();
            final List<ProtectiveEquipmentStatus> newProtectiveEquipmentStatuses = new ArrayList<>(protectiveEquipments.size());
            for (final ProtectiveEquipment protectiveEquipment : protectiveEquipments) {
                final ProtectiveEquipmentStatus protectiveEquipmentStatus = new ProtectiveEquipmentStatus(protectiveEquipment, user.getRegion());
                newProtectiveEquipmentStatuses.add(protectiveEquipmentStatus);
            }
            protectiveEquipmentStatusRepository.saveAll(newProtectiveEquipmentStatuses);
        }
        //Checks if there new protective equipments. If yes, adds them to the protective equipment statuses.
        if (protectiveEquipmentStatusesAmount < protectiveEquipmentRepository.count()) {
            final List<ProtectiveEquipmentStatus> protectiveEquipmentStatuses = protectiveEquipmentStatusRepository.findByRegion(user.getRegion());
            final List<Integer> existingProtectiveEquipmentIds = new ArrayList<>(protectiveEquipmentStatuses.size());
            for (final ProtectiveEquipmentStatus protectiveEquipmentStatus : protectiveEquipmentStatuses) {
                existingProtectiveEquipmentIds.add(protectiveEquipmentStatus.getProtectiveEquipment().getId());
            }
            final List<ProtectiveEquipment> protectiveEquipments = protectiveEquipmentRepository.findAllByIdIsNotIn(existingProtectiveEquipmentIds);
            for (final ProtectiveEquipment protectiveEquipment : protectiveEquipments) {
                protectiveEquipmentStatuses.add(new ProtectiveEquipmentStatus(protectiveEquipment, user.getRegion()));
            }
            protectiveEquipmentStatusRepository.saveAll(protectiveEquipmentStatuses);
        }
        final Page<ProtectiveEquipmentStatus> protectiveEquipmentStatusPage;
        final int numberOfPages;
        final int itemsPerPage = 5;
        if (protectiveEquipmentName == null || protectiveEquipmentName.isBlank()) {
            protectiveEquipmentStatusPage = protectiveEquipmentStatusRepository.findAllByRegion(user.getRegion(), PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "protectiveEquipment.name")));

        } else {
            protectiveEquipmentStatusPage = protectiveEquipmentStatusRepository.findAllByRegionAndProtectiveEquipmentNameContaining(user.getRegion(), protectiveEquipmentName, PageRequest.of(currentPage, itemsPerPage, Sort.by(Sort.Direction.ASC, "protectiveEquipment.name")));
        }
        numberOfPages = protectiveEquipmentStatusPage.getTotalPages();
        final List<ProtectiveEquipmentStatus> protectiveEquipmentStatuses = protectiveEquipmentStatusPage.getContent();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        return new ModelAndView("showRegionProtectiveEquipments").addObject("protectiveEquipmentStatuses", protectiveEquipmentStatuses).addObject("pages", pages).addObject("currentPage", currentPage).addObject("protectiveEquipmentName", protectiveEquipmentName);
    }

    /**
     * Opens form for changing the amount of selected protective equipment.
     *
     * @param protectiveEquipmentStatusId - Id of a protective equipment status, which amount is going to be changed.
     * @param authentication              - Authentication information of the user.
     * @param redirect                    - Used to save redirect messages.
     * @return - View with the form.
     */
    @GetMapping("/changeRegionProtectiveEquipmentAmount")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView changeRegionProtectiveEquipmentAmount(@RequestParam final int protectiveEquipmentStatusId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getRegion() == null || protectiveEquipmentStatus.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se získat počet ochranných pomůcek cizího kraje nebo organizace!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        return new ModelAndView("changeRegionProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus);
    }

    /**
     * Updates the changed protective equipment quantity in the database. Also saves the event in the database too.
     *
     * @param protectiveEquipmentStatus - Contains the changed quantity and other required information.
     * @param result                    - Result of the automated validation.
     * @param eventComment              - Explanation of the user's reason for changing the amount. This is going to be saved separately as a history event.
     * @param authentication            - Authentication information of the user.
     * @param redirect                  - Used to save redirect messages.
     * @return - In case of validation error return the previous view with the form. Otherwise redirects user to another action.
     */
    @PostMapping("/saveChangedRegionProtectiveEquipmentAmount")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView saveChangedRegionProtectiveEquipmentAmount(@Valid final ProtectiveEquipmentStatus protectiveEquipmentStatus, final BindingResult result, @RequestParam final String eventComment, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeRegionProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus);
        }
        if (eventComment == null || eventComment.isBlank()) {
            return new ModelAndView("changeRegionProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus).addObject("error", "Prosím vyplňte důvod změny.");
        }
        if (eventComment.length() > 500) {
            return new ModelAndView("changeRegionProtectiveEquipmentAmount").addObject(protectiveEquipmentStatus).addObject("error", "Odůvodnění změny smí mít maximálně 500 znaků. Prosím, zkraťte ho.");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final ProtectiveEquipmentStatus changedProtectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatus.getId());
        if (changedProtectiveEquipmentStatus == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        if (changedProtectiveEquipmentStatus.getRegion() == null || changedProtectiveEquipmentStatus.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se získat počet ochranných pomůcek cizího kraje nebo organizace!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        final int originalAmount = changedProtectiveEquipmentStatus.getQuantity();
        final int newAmount = protectiveEquipmentStatus.getQuantity();
        changedProtectiveEquipmentStatus.setQuantity(protectiveEquipmentStatus.getQuantity());
        final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(1);
        final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent(new Date(), originalAmount, newAmount, eventComment, protectiveEquipmentStatusRepository.save(changedProtectiveEquipmentStatus), user, protectiveEquipmentStatusChangeEventType, user.getRegion(), 0);
        protectiveEquipmentStatusChangeEventRepository.save(protectiveEquipmentStatusChangeEvent);
        redirect.addFlashAttribute("success", "Množství ochranné pomůcky " + changedProtectiveEquipmentStatus.getProtectiveEquipment().getName() + " bylo úspěšně změněno na.: " + changedProtectiveEquipmentStatus.getQuantity());
        return new ModelAndView("redirect:/showRegionProtectiveEquipments");
    }

    /**
     * Opens form for adding amount to the protective equipment.
     *
     * @param protectiveEquipmentStatusId - Id of the protective equipment status, which amount is going to be changed.
     * @param authentication              - Authentication information of the user.
     * @param redirect                    - Used to save redirect messages.
     * @return - Returns view with the form.
     */
    @GetMapping("/addProtectiveEquipmentAmount")
    public ModelAndView addProtectiveEquipmentAmount(@RequestParam final int protectiveEquipmentStatusId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getRegion() == null || protectiveEquipmentStatus.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se pracovat s ochrannou pomůckou cizího kraje nebo organizace!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        return new ModelAndView("addProtectiveEquipmentAmount").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus);
    }

    /**
     * Saves added protective equipment amount to the database. Also saves the corresponding history events.
     *
     * @param protectiveEquipmentStatusId - Protective equipment status, which quantity is going to be changed.
     * @param protectiveEquipmentAmount   - Amount which is going to be added to the quantity of the protective equipment status.
     * @param comment                     - Comment of the user explaining the reason of the addition. Is going to be saved separately, into a history event.
     * @param billId                      - Id of the bill or similar document corresponding to this addition.
     * @param authentication              - Authentication information of the user.
     * @param redirect                    - Used to save redirect messages.
     * @return - In case of validation error returns the previous view with the form. Otherwise redirects user to the other action.
     */
    @PostMapping("/saveAddedProtectiveEquipmentAmount")
    public ModelAndView saveAddedProtectiveEquipmentAmount(@RequestParam("id") final int protectiveEquipmentStatusId, @RequestParam final int protectiveEquipmentAmount, @RequestParam final String comment, @RequestParam final long billId, final Authentication authentication, final RedirectAttributes redirect) {
        if (protectiveEquipmentStatusId < 1) {
            redirect.addFlashAttribute("error", "ID stavu ochranné pomůcky je neplatné!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        if (protectiveEquipmentAmount < 1) {
            redirect.addFlashAttribute("error", "Počet přidaných položek musí být alespoň 1. Zadejte prosím vyšší počet.");
            return new ModelAndView("addProtectiveEquipmentAmount").addObject("protectiveEquipmentStatus", protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId)).addObject("error", "Číslo změny nesmí být 0!");
        }
        if (comment == null || comment.isBlank()) {
            return new ModelAndView("addProtectiveEquipmentAmount").addObject("protectiveEquipmentStatus", protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId)).addObject("error", "Prosím vyplňte odůvodnění.");
        }
        if (comment.length() > 500) {
            return new ModelAndView("addProtectiveEquipmentAmount").addObject("protectiveEquipmentStatus", protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId)).addObject("error", "Odůvodnění smí mít maximálně 500 znaků. Prosím zkraťte ho.");
        }
        if (billId < 1) {
            return new ModelAndView("addProtectiveEquipmentAmount").addObject("protectiveEquipmentStatus", protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId)).addObject("error", "Prosím zadejte číslo dokladu.");
        }
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getRegion() == null || protectiveEquipmentStatus.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se pracovat s ochrannou pomůckou cizího kraje nebo organizace!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        final int originalAmount = protectiveEquipmentStatus.getQuantity();
        if (!protectiveEquipmentStatus.addAmount(protectiveEquipmentAmount)) {
            return new ModelAndView("addProtectiveEquipmentAmount").addObject("protectiveEquipmentStatus", protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId)).addObject("error", "Počet ochranných pomůcek po odečtení nesmí být v záporném čísle (menší než 0). Prosím, zmenšete počet odečtených ochranných pomůcek.");
        }
        final int newAmount = protectiveEquipmentStatus.getQuantity();
        final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(2);
        final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent(new Date(), originalAmount, newAmount, comment, protectiveEquipmentStatusRepository.save(protectiveEquipmentStatus), user, protectiveEquipmentStatusChangeEventType, user.getRegion(), billId);
        protectiveEquipmentStatusChangeEventRepository.save(protectiveEquipmentStatusChangeEvent);
        redirect.addFlashAttribute("success", protectiveEquipmentAmount + " ochranných pomůcek " + protectiveEquipmentStatus.getProtectiveEquipment().getName() + " bylo přidáno na sklad. Současný počet je.: " + protectiveEquipmentStatus.getQuantity());
        return new ModelAndView("redirect:/showRegionProtectiveEquipments");
    }

    /**
     * Opens form for using region protective equipments.
     *
     * @param protectiveEquipmentStatusId - Id of the protective equipment status which is going to be used.
     * @param authentication              - Authentication information of the user.
     * @param redirect                    - Used to save redirect messages.
     * @return - View with the form. In csae of error, redirects user to other action.
     */
    @GetMapping("/useRegionProtectiveEquipments")
    public ModelAndView useRegionProtectiveEquipments(@RequestParam final int protectiveEquipmentStatusId, final Authentication authentication, final RedirectAttributes redirect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        if (protectiveEquipmentStatus == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        if (protectiveEquipmentStatus.getQuantity() < 1) {
            redirect.addFlashAttribute("error", "Nemáte žádné ochranné pomůcky tohoto typu na skladě, a tudíž je nemůžete vyskladnit.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getRegion() == null || protectiveEquipmentStatus.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete vyskladnit cizí ochranné pomůcky!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent();
        final List<Organization> organizations = organizationRepository.findAllByRegion(user.getRegion(), Sort.by(Sort.Direction.DESC, "name"));
        return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("organizations", organizations);
    }

    /**
     * Saves the changed protective equipments into the database. Also creates new history event corresponding ot the change.
     *
     * @param protectiveEquipmentStatusChangeEvent - Contains information about the change.
     * @param result                               - Result of the automated validation.
     * @param amountUsed                           - Amount of used protective equipments.
     * @param protectiveEquipmentStatusId          - Id of the used protective equipments, which are going to be updated in the database.
     * @param billId                               Id of the bill or similar document corresponding with the usage.
     * @param authentication                       - Authentication of the user.
     * @param redirect                             - Used to save redirect mesages.
     * @param typeOfUsing                          - Tells how are the protective equipments used.
     * @param organizationIdSelect                 - Selected Id of the organisation in case that the type of usage is sending protective equipments to an organisation outside of the request. Optional parameter, unless the corresponding type of usage is selected.
     * @return - In case of validation error returns the previous form. Otherwise redirects user to other action.
     */
    @PostMapping("/saveUsedRegionProtectiveEquipments")
    public ModelAndView saveUsedRegionProtectiveEquipments(@Valid final ProtectiveEquipmentStatusChangeEvent protectiveEquipmentStatusChangeEvent, final BindingResult result, @RequestParam final int amountUsed, @RequestParam final int protectiveEquipmentStatusId, @RequestParam final long billId, final Authentication authentication, final RedirectAttributes redirect, @RequestParam final int typeOfUsing, @RequestParam(required = false, defaultValue = "0") final int organizationIdSelect) {
        final ProtectiveEquipmentStatus protectiveEquipmentStatus = protectiveEquipmentStatusRepository.findById(protectiveEquipmentStatusId);
        if (protectiveEquipmentStatus == null) {
            redirect.addFlashAttribute("error", "Id ochranné pomůcky je neplatné.");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (protectiveEquipmentStatus.getRegion() == null || protectiveEquipmentStatus.getRegion().getId() != user.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete vyskladnit cizí ochranné pomůcky!");
            return new ModelAndView("redirect:/showRegionProtectiveEquipments");
        }
        if (result.hasErrors()) {
            return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent);
        }
        final int originalAmount = protectiveEquipmentStatus.getQuantity();

        if (amountUsed < 1) {
            return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Množství vyskladněných ochranných pomůcek musí být větší než 0. Prosím zadejte větší množství, v kladném čísle.");
        }
        final int newAmount = originalAmount - amountUsed;
        if (newAmount < 0) {
            return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Nemůžete vyskladnit větší množství položek, než je ve vašem skladě. Zadejte prosím menší množství vyskladněných položek.");
        }
        if (billId < 1) {
            return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Prosím zadejte číslo dokladu.");
        }
        final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType;
        if (typeOfUsing == 1) {
            protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(3);
        } else if (typeOfUsing == 2) {
            if (organizationIdSelect < 1) {
                return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Prosím vyberte organizace, které chcete ochranné pomůcky poslat.");
            }
            //Aside of getting the right type, create the new protective equipment transfer for the used amount.
            protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(6);
            final Organization organization = organizationRepository.findById(organizationIdSelect);
            if (organization == null) {
                return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Vybraná organizace neexistuje. Vyberte ji prosím znova, nebo vyberte jinou.");
            }
            if (organization.getRegion().getId() != user.getRegion().getId()) {
                return new ModelAndView("useRegionProtectiveEquipments").addObject("protectiveEquipmentStatus", protectiveEquipmentStatus).addObject("protectiveEquipmentStatusChangeEvent", protectiveEquipmentStatusChangeEvent).addObject("error", "Nemůžete vyskladnit organizaci cizího kraje ochranné pomůcky!");
            }
            final ProtectiveEquipmentTransfer protectiveEquipmentTransfer = new ProtectiveEquipmentTransfer(user.getRegion(), organization, protectiveEquipmentTransferStatusRepository.findById(1), protectiveEquipmentTransferTypeRepository.findById(2));
            final TransferProtectiveEquipment transferProtectiveEquipment = new TransferProtectiveEquipment(amountUsed, protectiveEquipmentStatus.getProtectiveEquipment(), protectiveEquipmentTransferRepository.save(protectiveEquipmentTransfer));
            transferProtectiveEquipmentRepository.save(transferProtectiveEquipment);
        } else if (typeOfUsing == 3) {
            protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(7);
        }// In case that the typeOfUsing doesn't have valid value, consider it as internal use
        else {
            protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventTypeRepository.findById(3);
        }
        protectiveEquipmentStatus.setQuantity(newAmount);
        final ProtectiveEquipmentStatusChangeEvent savedProtectiveEquipmentStatusChangeEvent = new ProtectiveEquipmentStatusChangeEvent(new Date(), originalAmount, newAmount, protectiveEquipmentStatusChangeEvent.getComment(), protectiveEquipmentStatusRepository.save(protectiveEquipmentStatus), user, protectiveEquipmentStatusChangeEventType, user.getRegion(), billId);
        protectiveEquipmentStatusChangeEventRepository.save(savedProtectiveEquipmentStatusChangeEvent);
        redirect.addFlashAttribute("success", amountUsed + " " + protectiveEquipmentStatus.getProtectiveEquipment().getName() + " bylo úspěšně vyskladněno.");
        return new ModelAndView("redirect:/showRegionProtectiveEquipments");
    }
}
