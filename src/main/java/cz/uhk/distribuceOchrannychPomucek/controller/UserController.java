package cz.uhk.distribuceOchrannychPomucek.controller;

import cz.uhk.distribuceOchrannychPomucek.model.*;
import cz.uhk.distribuceOchrannychPomucek.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.thymeleaf.util.StringUtils;

import javax.validation.Valid;
import java.util.List;

/**
 * Contains actions working with the user accounts, represented by the AppUser and UserInfo class.
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class UserController {
    private final UserInfoRepository userInfoRepository;
    private final AppUserRepository appUserRepository;
    private final RegionRepository regionRepository;
    private final RightRepository rightRepository;

    private final OrganizationRepository organizationRepository;
    private final AppUserDisabledInfoRepository appUserDisabledInfoRepository;

    public UserController(final UserInfoRepository userInfoRepository, final AppUserRepository appUserRepository, final RegionRepository regionRepository, final RightRepository rightRepository, final OrganizationRepository organizationRepository, final AppUserDisabledInfoRepository appUserDisabledInfoRepository) {
        this.userInfoRepository = userInfoRepository;
        this.appUserRepository = appUserRepository;
        this.regionRepository = regionRepository;
        this.rightRepository = rightRepository;
        this.organizationRepository = organizationRepository;
        this.appUserDisabledInfoRepository = appUserDisabledInfoRepository;
    }

    /**
     * Shows the information about the user calling this action.
     *
     * @param authentication - Authentication information of the user.
     * @return - View with the user information.
     */
    @GetMapping("/showUserInfo")
    public ModelAndView showUserInfo(final Authentication authentication) {
        final UserInfo userInfo = userInfoRepository.findByUser(appUserRepository.findByLogin(authentication.getName()));
        return new ModelAndView("showUserInfo").addObject("userInfo", userInfo);
    }

    /**
     * Opens form for changing the information about the user calling this method.
     * Is restricted for only admin users for security reasons.
     *
     * @param authentication - Authentication information of the users.
     * @return - View with the form for changing the user's information.
     */
    @GetMapping("/changeUserInfo")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin", "ROLE_OrganizationAdmin"})
    public ModelAndView changeUserInfo(final Authentication authentication) {
        final UserInfo userInfo = userInfoRepository.findByUser(appUserRepository.findByLogin(authentication.getName()));
        return new ModelAndView("changeUserInfo").addObject(userInfo);
    }

    /**
     * Updates the changed user information in the database.
     *
     * @param userInfo       - Changed user information to be updated.
     * @param result         - Result of the automated validation.
     * @param authentication - Authentication information of the user.
     * @param redirect       - Used to save redirect messages.
     * @return - In case of validation error returns the previous view with the form. Otherwise redirects user to other action.
     */
    @PostMapping("/saveChangedUserInfo")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin", "ROLE_OrganizationAdmin"})
    public ModelAndView saveChangedUserInfo(@Valid final UserInfo userInfo, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeUserInfo").addObject(userInfo);
        }

        final UserInfo newUserInfo = userInfoRepository.findByUser(appUserRepository.findByLogin(authentication.getName()));
        if (userInfoRepository.existsByEmailAndIdNot(userInfo.getEmail(), newUserInfo.getId())) {
            return new ModelAndView("changeUserInfo").addObject(userInfo).addObject("error", "Zadaný email už je použit jiným uživatelem. Zadejte prosím jiný.");
        }
        newUserInfo.setName(userInfo.getName());
        newUserInfo.setSurname(userInfo.getSurname());
        newUserInfo.setEmail(userInfo.getEmail());
        userInfoRepository.save(newUserInfo);
        redirect.addFlashAttribute("success", "Vaše uživatelské údaje byly úspěšně změněny.");
        return new ModelAndView("redirect:/showUserInfo");
    }

    /**
     * Opens form for changing the login (user name) of the user calling this action.
     * Restricted to only admin users for security reasons.
     *
     * @param authentication - Authentication information of the user.
     * @return - View with the form.
     */
    @GetMapping("/changeUserLogin")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin", "ROLE_OrganizationAdmin"})
    public ModelAndView changeUserLogin(final Authentication authentication) {
        return new ModelAndView("changeUserLogin").addObject("currentLogin", "Současné uživatelské jméno: " + authentication.getName());
    }

    /**
     * Updates the user's login in the database with the new one. Then logouts the user out of the system to avoid session desynchronisation problems and other errors caused by this update.
     * The logout is done by redirecting the user to the logout action.
     *
     * @param newLogin       - New login of the user.
     * @param authentication - Authentication information of the user.
     * @return - In case of validation error returns user back to the previous view with the form. In case of success redirects user to the logout action, which logs the user off the system.
     */
    @PostMapping("/saveChangedUserLogin")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin", "ROLE_OrganizationAdmin"})
    public ModelAndView saveChangedUserLogin(@RequestParam final String newLogin, final Authentication authentication) {
        if (newLogin == null || newLogin.isBlank()) {
            return new ModelAndView("changeUserLogin").addObject("error", "Prosím vyplňte vaše nové uživatelské jméno.");
        }
        if (newLogin.length() > 50) {
            return new ModelAndView("changeUserLogin").addObject("error", "Nové uživatelské jméno smí mít maximálně 50 znaků. Prosím zkraťte ho.");
        }
        if (appUserRepository.existsByLogin(newLogin)) {
            return new ModelAndView("changeUserLogin").addObject("error", "Uživatelské jméno už existuje. Zadejte prosím jiné.");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        user.setLogin(newLogin);
        appUserRepository.save(user);
        return new ModelAndView("redirect:/logout");
    }

    /**
     * Opens form for changing the password of the user calling this action.
     *
     * @return - View with the form.
     */
    @GetMapping("/changeUserPassword")
    public String changeUserPassword() {
        return "changeUserPassword";
    }

    /**
     * Updates the user's password in the database.
     *
     * @param currentPassword  - Input of supposed current password by the user.
     * @param newPassword      - Input of the new password.
     * @param newPasswordAgain - Input of the new password again, to verify that the user remembers it or that he didn't mistyped.
     * @param authentication   - Authentication information of the user.
     * @param redirect         - Used to save redirect messages.
     * @return - In case of validation error returns the previous view with the form. Otherwise redirects user to another action.
     */
    @PostMapping("/saveChangedUserPassword")
    public ModelAndView saveChangedUserPassword(@RequestParam final String currentPassword, @RequestParam final String newPassword, @RequestParam final String newPasswordAgain, final Authentication authentication, final RedirectAttributes redirect) {
        if (currentPassword == null || currentPassword.isBlank() || newPassword == null || newPassword.isBlank() || newPasswordAgain == null || newPasswordAgain.isBlank()) {
            return new ModelAndView("changeUserPassword").addObject("error", "Prosím vyplňte všechny položky formuláře.");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(currentPassword, user.getPassword())) {
            return new ModelAndView("changeUserPassword").addObject("error", "Zadal jste špatně vaše současné heslo. Zadejte ho prosím znova.");
        }
        if (newPassword.length() > 500) {
            return new ModelAndView("changeUserPassword").addObject("error", "Nové heslo smí mít maximálně 500 znaků. Prosím, zkraťte ho.");
        }
        if (!newPassword.equals(newPasswordAgain)) {
            return new ModelAndView("changeUserPassword").addObject("error", "Nové heslo a nové heslo znova jsou zadány jinak. Zadejte vaše nové heslo do těchto dvou položek formuláře prosím stejně.");
        }
        user.setPassword(encoder.encode(newPassword));
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Vaše heslo bylo úspěšně změněno.");
        return new ModelAndView("redirect:/showUserInfo");
    }

    /**
     * Shows all users who are registered in the system.
     *
     * @param name        - Filters search of the user by name. Optional parameter.
     * @param surname     - Filters search of the user by surname. Optional parameter.
     * @param currentPage - Current page of the paged result. Starts at 0.
     * @return - View with the list of all users.
     */
    @GetMapping("/showUsers")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView showUsers(@RequestParam(required = false) final String name, @RequestParam(required = false) final String surname, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final Page<UserInfo> userInfoPage;
        //Checks if the name and surname values are filled and saves the result in the booleans.
        final boolean isNameNull = (name == null || name.isBlank());
        final boolean isSurnameNull = (surname == null || surname.isBlank());
        final int numberOfPages;
        final int itemsPerPage = 5;
        if (isSurnameNull && isNameNull) {
            userInfoPage = userInfoRepository.findAll(PageRequest.of(currentPage, itemsPerPage));
        } else if (!isSurnameNull && !isNameNull) {
            userInfoPage = userInfoRepository.findByNameContainingAndSurnameContaining(name, surname, PageRequest.of(currentPage, itemsPerPage));
        } else if (!isNameNull) {
            userInfoPage = userInfoRepository.findByNameContaining(name, PageRequest.of(currentPage, itemsPerPage));
        } else {
            userInfoPage = userInfoRepository.findBySurnameContaining(surname, PageRequest.of(currentPage, itemsPerPage));
        }
        numberOfPages = userInfoPage.getTotalPages();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<UserInfo> userInfos = userInfoPage.getContent();
        return new ModelAndView("showUsers").addObject("userInfos", userInfos).addObject("name", name).addObject("surname", surname).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Shows detailed information of the selected user.
     *
     * @param userInfoId     - Id of the user whose information are going to be shown.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - View with the user information. In case of error, redirects user to other action.
     */
    @GetMapping("/showDetailedUserInfo")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView showDetailedUserInfo(@RequestParam final int userInfoId, final RedirectAttributes redirect, final Authentication authentication) {
        final UserInfo userInfo = userInfoRepository.findById(userInfoId);
        if (userInfo == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new ModelAndView("redirect:/showUsers");
        }
        if (userInfo.getUser().getLogin().equals(authentication.getName())) {
            return new ModelAndView("redirect:/showUserInfo");
        }
        return new ModelAndView("showDetailedUserInfo").addObject("userInfo", userInfo);
    }

    /**
     * Opens forms for disabling selected user, which, when saved in another action, makes him unable to login into the system.
     *
     * @param userId         - Id of the user to be blocked.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - View with the form. In case of error, redirects user to other action.
     */
    @GetMapping("/disableUser")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView disableUser(@RequestParam final int userId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser currentUser = appUserRepository.findByLogin(authentication.getName());
        final AppUser userToDisable = appUserRepository.findById(userId);
        if (userToDisable == null) {
            redirect.addFlashAttribute("error", "Vybraný účet neexistuje (jeho identifikační číslo je neplatné).");
            return new ModelAndView("redirect:/showUsers");
        }
        if (userToDisable.getId() == currentUser.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat váš vlastní účet.");
            return new ModelAndView("redirect:/showUsers");
        }
        if (!userToDisable.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel " + userToDisable.getLogin() + " už je zablokovaný.");
            return new ModelAndView("redirect:/showUsers");
        }
        return new ModelAndView("disableUser").addObject("user", userToDisable).addObject("appUserDisabledInfo", new AppUserDisabledInfo());
    }

    /**
     * Updates the disabled status of the user in the database, which makes user unable to login into the system.
     *
     * @param appUserDisabledInfo - Contains information about the disabled user.
     * @param result              - Result of the automated validation.
     * @param userId              - Id of the user who is going to be disabled.
     * @param redirect            - Used to save redirect messages.
     * @param authentication      - Authentication information of the user calling this method.
     * @return - In case of validation error returns the previous view with the form. Otherwise redirects user to other action.
     */
    @PostMapping("/saveDisabledUser")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView saveDisabledUser(@Valid final AppUserDisabledInfo appUserDisabledInfo, final BindingResult result, @RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new ModelAndView("redirect:/showUsers");
        }
        if (result.hasErrors()) {
            return new ModelAndView("disableUser").addObject("user", user).addObject("appUserDisabledInfo", appUserDisabledInfo);
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat váš vlastní účet.");
            return new ModelAndView("redirect:/showUsers");
        }
        if (!user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel " + user.getLogin() + " už je zablokovaný.");
            return new ModelAndView("redirect:/showUsers");
        }
        final AppUserDisabledInfo savedAppUserDisabledInfo = new AppUserDisabledInfo(appUserDisabledInfo.getReason(), admin);
        //Checks if the previous entry of disabled user already exists. If yes, the entry is updated. If not, new entry is inserted into the database.
        if (user.getAppUserDisabledInfo() != null) {
            savedAppUserDisabledInfo.setId(user.getAppUserDisabledInfo().getId());
            appUserDisabledInfoRepository.save(savedAppUserDisabledInfo);
        } else {
            savedAppUserDisabledInfo.setId(appUserDisabledInfoRepository.save(savedAppUserDisabledInfo).getId());
        }
        user.setAppUserDisabledInfo(savedAppUserDisabledInfo);
        user.setEnabled(false);
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Uživatel " + user.getLogin() + " byl úspěšně zablokován.");
        return new ModelAndView("redirect:/showUsers");
    }

    /**
     * Enables the selected user, which allows him to login into the system.
     *
     * @param userId         - Id of the user who is going to be enabled.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - Redirects user to other action.
     */
    @GetMapping("/enableUser")
    @Secured({"ROLE_GlobalAdmin"})
    public RedirectView enableUser(@RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser userToEnable = appUserRepository.findById(userId);
        final AppUser currentUser = appUserRepository.findByLogin(authentication.getName());
        if (userToEnable == null) {
            redirect.addFlashAttribute("error", "Vybraný účet neexistuje (jeho identifikační číslo je neplatné).");
            return new RedirectView("/showUsers", true);
        }
        if (userToEnable.getId() == currentUser.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete odblokovat váš vlastní účet.");
            return new RedirectView("/showUsers", true);

        }
        if (userToEnable.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel " + userToEnable.getLogin() + " není zablokován.");
            return new RedirectView("/showUsers", true);
        }
        userToEnable.setEnabled(true);
        appUserRepository.save(userToEnable);
        redirect.addFlashAttribute("success", "Uživatel " + userToEnable.getLogin() + " byl úspěšně odblokován.");
        return new RedirectView("/showUsers", true);
    }

    /**
     * Opens form for changing information of the selected user.
     *
     * @param userId         - Id of the user whose information is going to be changed.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Information of the user calling this action.
     * @return - View with the form. In case of error redirects user to other action.
     */
    @GetMapping("/changeOtherUserInfo")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin"})
    public ModelAndView changeOtherUserInfo(@RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser user = appUserRepository.findById(userId);
        final AppUser adminUser = appUserRepository.findByLogin(authentication.getName());
        if (user == null) {
            redirect.addFlashAttribute("error", "Vybraný účet neexistuje (jeho identifikační číslo je neplatné).");
            if (adminUser.getRight().getId() == 1) {
                return new ModelAndView("redirect:/showUsers");
            } else {
                return new ModelAndView("redirect:/showRegionUsers");
            }
        }
        if (adminUser.getRight().getId() == 2 && !isRegionAdminSameRegionAsUser(adminUser, user)) {
            redirect.addFlashAttribute("error", "Nemůžete upravit údaje uživatele z cizího kraje!");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        final UserInfo userInfo = userInfoRepository.findByUser(user);
        return new ModelAndView("changeOtherUserInfo").addObject(userInfo);
    }

    /**
     * Updates the changed user in the database.
     *
     * @param userInfo       - Information of the user to be updated.
     * @param result         - Result of the automated validation.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - In case of validation error returns previous view with the form. Otherwise redirects user to other action.
     */
    @PostMapping("/saveOtherChangedUserInfo")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin"})
    public ModelAndView saveOtherChangedUserInfo(@Valid final UserInfo userInfo, final BindingResult result, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser adminUser = appUserRepository.findByLogin(authentication.getName());
        final UserInfo changedUserInfo = userInfoRepository.findById(userInfo.getId());
        if (changedUserInfo == null) {
            redirect.addFlashAttribute("error", "Id uživatelských informací je neplatné.");
            if (adminUser.getRight().getId() == 1) {
                return new ModelAndView("redirect:/showUsers");
            } else {
                return new ModelAndView("redirect:/showRegionUsers");
            }
        }
        if (result.hasErrors()) {
            return new ModelAndView("changeOtherUserInfo").addObject(userInfo);
        }
        final AppUser changedUser = userInfoRepository.findById(changedUserInfo.getId()).getUser();
        if (adminUser.getRight().getId() == 2 && !isRegionAdminSameRegionAsUser(adminUser, changedUser)) {
            redirect.addFlashAttribute("error", "Nemůžete změnit uživatele z jiného kraje!");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        if (userInfoRepository.existsByEmailAndIdNot(userInfo.getEmail(), userInfo.getId())) {
            return new ModelAndView("changeOtherUserInfo").addObject(userInfo).addObject("error", "Zadaný email už je použit jiným uživatelem. Zadejte prosím jiný.");
        }
        changedUserInfo.updateUserInfo(userInfo);
        userInfoRepository.save(changedUserInfo);
        redirect.addFlashAttribute("success", "Údaje uživatele " + changedUserInfo.getUser().getLogin() + " byly úspěšně změněny.");
        if (adminUser.getRight().getId() == 1) {
            return new ModelAndView("redirect:/showUsers");
        } else {
            return new ModelAndView("redirect:/showRegionUsers");
        }
    }

    /**
     * Opens form for creating a new admin of a region.
     *
     * @return - View with the form.
     */
    @GetMapping("/createNewRegionAdmin")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView createNewRegionAdmin() {
        final UserInfo userInfo = new UserInfo();
        final List<Region> regions = regionRepository.findAll();
        return new ModelAndView("createNewRegionAdmin").addObject(userInfo).addObject("regions", regions);
    }

    /**
     * Saves new region admin in the database.
     *
     * @param userInfo - Information about the new admin.
     * @param result   - Result of the automated validation.
     * @param redirect - Used to save redirect messages.
     * @return - In case of validation error returns the previous view with the form. In other cases, redirects user to other action.
     */
    @PostMapping("/saveNewRegionAdmin")
    @Secured("ROLE_GlobalAdmin")
    public ModelAndView saveNewRegionAdmin(@Valid final UserInfo userInfo, final BindingResult result, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            final List<Region> regions = regionRepository.findAll();
            return new ModelAndView("createNewRegionAdmin").addObject(userInfo).addObject(regions);
        }
        if (userInfo.getUser().getRegion() == null || userInfo.getUser().getRegion().getId() < 1 || !regionRepository.existsById(userInfo.getUser().getRegion().getId())) {
            final List<Region> regions = regionRepository.findAll();
            return new ModelAndView("createNewRegionAdmin").addObject(userInfo).addObject(regions).addObject("error", "Vybraný kraj je neplatný z důvodu neplatnosti jeho identifikačního čísla.");
        }
        if (appUserRepository.existsByLogin(userInfo.getUser().getLogin())) {
            final List<Region> regions = regionRepository.findAll();
            return new ModelAndView("createNewRegionAdmin").addObject(userInfo).addObject(regions).addObject("error", "Zadané uživatelské jméno už existuje. Zadejte prosím jiné.");
        }
        if (userInfoRepository.existsByEmail(userInfo.getEmail())) {
            final List<Region> regions = regionRepository.findAll();
            return new ModelAndView("createNewRegionAdmin").addObject(userInfo).addObject(regions).addObject("error", "Zadaný email je už použit jiným uživatelem. Zadejte prosím jiný.");
        }
        final String password = StringUtils.randomAlphanumeric(30);
        final AppUser newUser = new AppUser(userInfo.getUser().getLogin(), new BCryptPasswordEncoder().encode(password), rightRepository.findById(2), true);
        final Region region = regionRepository.findById(userInfo.getUser().getRegion().getId());
        newUser.setRegion(region);
        final UserInfo newUserInfo = new UserInfo(userInfo.getName(), userInfo.getSurname(), userInfo.getEmail(), appUserRepository.save(newUser));
        userInfoRepository.save(newUserInfo);
        redirect.addFlashAttribute("success", "Administrátor kraje " + region.getName() + ", s uživatelským jménem " + newUser.getLogin() + " byl úspěšně vytvořen. Jeho heslo je: " + password);
        return new ModelAndView("redirect:/showUsers");
    }

    /**
     * Resets password of the selected user to a randomly generated one.
     * Note that while in this, simplified version, the new password is shown to the creating admin, as this breaks security, the real solution would be to send the generated password to the created user by e-mail to his e-mail address.
     * Or even better, send the user a link to input a new password.
     *
     * @param userId         - Id of the user whose password is going to be reset.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - Redirects user to other action.
     */
    @GetMapping("/resetUserPassword")
    @Secured({"ROLE_GlobalAdmin", "ROLE_RegionAdmin"})
    public RedirectView resetUserPassword(@RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (userId < 1) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné!");
            if (admin.getRight().getId() == 1) {
                return new RedirectView("/showUsers", true);
            } else {
                return new RedirectView("/showRegionUsers", true);
            }
        }
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné!");
            if (admin.getRight().getId() == 1) {
                return new RedirectView("/showUsers", true);
            } else {
                return new RedirectView("/showRegionUsers", true);
            }
        }
        if (admin.getRight().getId() == 2 && !isRegionAdminSameRegionAsUser(admin, user)) {
            redirect.addFlashAttribute("error", "Nemůžete resetovat heslo uživatele z cizího kraje!");
            if (admin.getRight().getId() == 1) {
                return new RedirectView("/showUsers", true);
            } else {
                return new RedirectView("/showRegionUsers", true);
            }
        }
        final String password = StringUtils.randomAlphanumeric(30);
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        appUserRepository.save(user);
        redirect.addFlashAttribute("Heslo uživatele " + user.getLogin() + " bylo úspěšně resetováno na: " + password);
        if (admin.getRight().getId() == 1) {
            return new RedirectView("/showUsers", true);
        } else {
            return new RedirectView("/showRegionUsers", true);
        }
    }

    /**
     * Opens form for creating a new region user.
     *
     * @return - View with the form.
     */
    @Secured("ROLE_RegionAdmin")
    @GetMapping("/createNewRegionUser")
    public ModelAndView createNewRegionUser() {
        final UserInfo userInfo = new UserInfo();
        final List<Right> rights = rightRepository.findByIdBetween(2, 3);
        return new ModelAndView("createNewRegionUser").addObject(userInfo).addObject("rights", rights);
    }

    /**
     * Saves the new region user into the database.
     *
     * @param userInfo       - Information about the new user.
     * @param result         - Result of the automated validation.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - In case of validation error returns the previous view with form. In other cases redirects user to other action.
     */
    @Secured("ROLE_RegionAdmin")
    @PostMapping("/saveNewRegionUser")
    public ModelAndView saveNewRegionUser(@Valid final UserInfo userInfo, final BindingResult result, final RedirectAttributes redirect, final Authentication authentication) {
        if (result.hasErrors()) {
            final List<Right> rights = rightRepository.findByIdBetween(2, 3);
            return new ModelAndView("createNewRegionUser").addObject(userInfo).addObject(rights);
        }
        if (appUserRepository.existsByLogin(userInfo.getUser().getLogin())) {
            final List<Right> rights = rightRepository.findByIdBetween(2, 3);
            return new ModelAndView("createNewRegionUser").addObject(userInfo).addObject(rights).addObject("error", "Zadané uživatelské jméno už existuje. Zadejte prosím jiné.");
        }
        if (userInfoRepository.existsByEmail(userInfo.getEmail())) {
            final List<Right> rights = rightRepository.findByIdBetween(2, 3);
            return new ModelAndView("createNewRegionUser").addObject(userInfo).addObject(rights).addObject("error", "Zadaný email je už použit jiným uživatelem. Zadejte prosím jiný.");
        }
        if (userInfo.getUser().getRight() == null) {
            final List<Right> rights = rightRepository.findByIdBetween(2, 3);
            return new ModelAndView("createNewRegionUser").addObject(userInfo).addObject(rights).addObject("error", "Chybí oprávnění uživatele. Zvolte ho prosím ve formuláři.");
        }
        if (userInfo.getUser().getRight().getId() < 2 || userInfo.getUser().getRight().getId() > 3) {
            final List<Right> rights = rightRepository.findByIdBetween(2, 3);
            return new ModelAndView("createNewRegionUser").addObject(userInfo).addObject(rights).addObject("error", "Můžete uživateli nastavit pouze oprávnění týkající se kraje! (Administrátor kraje anebo uživatel kraje.");
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final String password = StringUtils.randomAlphanumeric(30);
        final AppUser newUser = new AppUser(userInfo.getUser().getLogin(), new BCryptPasswordEncoder().encode(password), rightRepository.findById(userInfo.getUser().getRight().getId()), true, admin.getRegion());
        final UserInfo newUserInfo = new UserInfo(userInfo.getName(), userInfo.getSurname(), userInfo.getEmail(), appUserRepository.save(newUser));
        userInfoRepository.save(newUserInfo);
        redirect.addFlashAttribute("success", "Uživatel " + newUser.getLogin() + " s rolí " + newUser.getRight().getDescription() + "a heslem " + password + " byl úspěšně vytvořen.");
        return new ModelAndView("redirect:/showRegionUsers");
    }

    /**
     * Shows users of the region of the admin.
     *
     * @param authentication - Authentication information of the user calling this action.
     * @param name           - Filters the result by name of a user. Optional parameter.
     * @param surname        - Filters the result by surname of a user. Optional parameter.
     * @param currentPage    - Current page of the paged result. Starts at 0.
     * @return - View with the users.
     */
    @GetMapping("/showRegionUsers")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView showRegionUsers(final Authentication authentication, @RequestParam(required = false) final String name, @RequestParam(required = false) final String surname, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        final Page<UserInfo> userInfoPage;
        final boolean isNameNull;
        final boolean isSurnameNull;
        //Amount of instances to be loaded into one page.
        final int pageItemsAmount = 5;
        if (name == null || name.isBlank()) {
            isNameNull = true;
        } else {
            isNameNull = false;
        }
        if (surname == null || surname.isBlank()) {
            isSurnameNull = true;
        } else {
            isSurnameNull = false;
        }
        final int numberOfPages;
        if (isSurnameNull && isNameNull) {
            userInfoPage = userInfoRepository.findByUserRegion(user.getRegion(), PageRequest.of(currentPage, pageItemsAmount));

        } else if (!isSurnameNull && !isNameNull) {
            userInfoPage = userInfoRepository.findByUserRegionAndNameContainingAndSurnameContaining(user.getRegion(), name, surname, PageRequest.of(currentPage, pageItemsAmount));
        } else if (!isNameNull) {
            userInfoPage = userInfoRepository.findByUserRegionAndNameContaining(user.getRegion(), name, PageRequest.of(currentPage, pageItemsAmount));
        } else {
            userInfoPage = userInfoRepository.findByUserRegionAndSurnameContaining(user.getRegion(), surname, PageRequest.of(currentPage, pageItemsAmount));
        }
        numberOfPages = userInfoPage.getTotalPages();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<UserInfo> userInfos = userInfoPage.getContent();
        return new ModelAndView("showRegionUsers").addObject("userInfos", userInfos).addObject("name", name).addObject("surname", surname).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Opens form for disabling the selected user in the admin's region to make him unable to log in into the system.
     *
     * @param userId         - Id of the user to be disabled.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - View with the form. In case of error, redirects user to other action.
     */
    @GetMapping("/disableRegionUser")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView disableRegionUser(@RequestParam final int userId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Vybraný účet neexistuje (jeho identifikační číslo je neplatné).");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        if ((user.getOrganization() == null && user.getRegion().getId() != admin.getRegion().getId()) || (user.getRegion() == null && user.getOrganization().getRegion().getId() != admin.getRegion().getId())) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat účet cizího kraje.");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat váš vlastní účet.");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        if (!user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel " + user.getLogin() + " už je zablokovaný.");
            if (user.getOrganization() != null) {
                return new ModelAndView("redirect:/showRegionOrganizationUsers?organizationId=" + user.getOrganization().getId());
            } else {
                return new ModelAndView("redirect:/showRegionUsers");
            }
        }
        if (user.getRight().getId() == 2) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat jiné administrátory kraje!");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        return new ModelAndView("disableRegionUser").addObject("user", user).addObject("appUserDisabledInfo", new AppUserDisabledInfo());
    }

    /**
     * Updates the disabled user in the database. Also creates a new entry about the disabled user information or updates the existing one, if it already exists.
     *
     * @param appUserDisabledInfo - Information about the disabled user.
     * @param result              - Result of the automated validation.
     * @param userId              - Id of the user to be disabled.
     * @param redirect            - Used to save redirect messages.
     * @param authentication      - Authentication information of the user calling this action.
     * @return - In case of validation error returns user to the previous view with the disabling form. In other cases redirects user to other action.
     */
    @PostMapping("/saveRegionDisabledUser")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView saveRegionDisabledUser(@Valid final AppUserDisabledInfo appUserDisabledInfo, final BindingResult result, @RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new ModelAndView("redirect:/showUsers");
        }
        if (result.hasErrors()) {
            return new ModelAndView("disableRegionUser").addObject("user", user).addObject("appUserDisabledInfo", appUserDisabledInfo);
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if ((user.getOrganization() == null && user.getRegion().getId() != admin.getRegion().getId()) || (user.getRegion() == null && user.getOrganization().getRegion().getId() != admin.getRegion().getId())) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat účet cizího kraje.");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat váš vlastní účet.");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        if (!user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel " + user.getLogin() + " už je zablokovaný.");
            if (user.getOrganization() != null) {
                return new ModelAndView("redirect:/showRegionOrganizationUsers?organizationId=" + user.getOrganization().getId());
            } else {
                return new ModelAndView("redirect:/showRegionUsers");
            }
        }
        if (user.getRight().getId() == 2) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat jiné administrátory kraje!");
            return new ModelAndView("redirect:/showRegionUsers");
        }
        final AppUserDisabledInfo savedAppUserDisabledInfo = new AppUserDisabledInfo(appUserDisabledInfo.getReason(), admin);

        if (user.getAppUserDisabledInfo() != null) {
            savedAppUserDisabledInfo.setId(user.getAppUserDisabledInfo().getId());
            appUserDisabledInfoRepository.save(savedAppUserDisabledInfo);
        } else {
            savedAppUserDisabledInfo.setId(appUserDisabledInfoRepository.save(savedAppUserDisabledInfo).getId());
        }
        user.setAppUserDisabledInfo(savedAppUserDisabledInfo);
        user.setEnabled(false);
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Uživatel " + user.getLogin() + " byl úspěšně zablokován.");
        if (user.getOrganization() != null) {
            return new ModelAndView("redirect:/showRegionOrganizationUsers?organizationId=" + user.getOrganization().getId());
        } else {
            return new ModelAndView("redirect:/showRegionUsers");
        }
    }

    /**
     * Enables user of the admin's region. Which allows user again to be able to log in into the system.
     *
     * @param userId         - Id of the user to be enabled.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - Redirects user to other action.
     */
    @GetMapping("/enableRegionUser")
    @Secured({"ROLE_RegionAdmin"})
    public RedirectView enableRegionUser(@RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser user = appUserRepository.findById(userId);
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (user == null) {
            redirect.addFlashAttribute("error", "Vybraný účet neexistuje (jeho identifikační číslo je neplatné).");
            return new RedirectView("/showRegionUsers", true);
        }
        if ((user.getOrganization() == null && user.getRegion().getId() != admin.getRegion().getId()) || (user.getRegion() == null && user.getOrganization().getRegion().getId() != admin.getRegion().getId())) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat účet cizího kraje.");
            return new RedirectView("/showRegionUsers", true);
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete odblokovat váš vlastní účet.");
            return new RedirectView("/showRegionUsers", true);
        }
        if (user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel " + user.getLogin() + " není zablokován.");
            if (user.getOrganization() != null) {
                return new RedirectView("/showRegionOrganizationUsers?organizationId=" + user.getOrganization().getId(), true);
            } else {
                return new RedirectView("/showRegionUsers", true);
            }
        }
        if (user.getRight().getId() == 2) {
            redirect.addFlashAttribute("error", "Nemůžete odblokovat jiné administrátory kraje!");
            return new RedirectView("/showRegionUsers", true);
        }
        user.setEnabled(true);
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Uživatel " + user.getLogin() + " byl úspěšně odblokován.");
        if (user.getOrganization() != null) {
            return new RedirectView("/showRegionOrganizationUsers?organizationId=" + user.getOrganization().getId(), true);
        } else {
            return new RedirectView("/showRegionUsers", true);
        }
    }

    /**
     * Opens form for creating a new user in the admin's organization.
     *
     * @return - View with the form.
     */
    @GetMapping("/createOrganizationUser")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView createOrganizationUser() {
        final UserInfo userInfo = new UserInfo();
        final List<Right> rights = rightRepository.findByIdBetween(4, 5);
        return new ModelAndView("createOrganizationUser").addObject(userInfo).addObject("rights", rights);
    }

    /**
     * Saves the new organization user into the database.
     *
     * @param userInfo       - Information about the new user.
     * @param result         - Result of the automated validation
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - In case of validation error returns the previosu view with the form. In ther cases redirects user to other action.
     */
    @PostMapping("/saveCreatedOrganizationUser")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView saveCreatedOrganizationUser(@Valid final UserInfo userInfo, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            final List<Right> rights = rightRepository.findByIdBetween(4, 5);
            return new ModelAndView("createOrganizationUser").addObject(userInfo).addObject(rights);
        }
        if (appUserRepository.existsByLogin(userInfo.getUser().getLogin())) {
            final List<Right> rights = rightRepository.findByIdBetween(4, 5);
            return new ModelAndView("createOrganizationUser").addObject(userInfo).addObject(rights).addObject("error", "Zadané uživatelské jméno už existuje. Zadejte prosím jiné.");
        }
        if (userInfoRepository.existsByEmail(userInfo.getEmail())) {
            final List<Right> rights = rightRepository.findByIdBetween(4, 5);
            return new ModelAndView("createOrganizationUser").addObject(userInfo).addObject(rights).addObject("error", "Zadaný email je už použit jiným uživatelem. Zadejte prosím jiný.");
        }
        if (userInfo.getUser().getRight() == null) {
            final List<Right> rights = rightRepository.findByIdBetween(4, 5);
            return new ModelAndView("createOrganizationUser").addObject(userInfo).addObject(rights).addObject("error", "Chybí oprávnění uživatele. Zvolte ho prosím ve formuláři.");
        }
        if (userInfo.getUser().getRight().getId() < 4 || userInfo.getUser().getRight().getId() > 5) {
            final List<Right> rights = rightRepository.findByIdBetween(4, 5);
            return new ModelAndView("createOrganizationUser").addObject(userInfo).addObject(rights).addObject("error", "Můžete uživateli nastavit pouze oprávnění týkající se organizace! (Administrátor organizace, anebo uživatel organizace.");
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final String password = StringUtils.randomAlphanumeric(30);
        final AppUser newUser = new AppUser(userInfo.getUser().getLogin(), new BCryptPasswordEncoder().encode(password), rightRepository.findById(userInfo.getUser().getRight().getId()), true, admin.getOrganization());
        final UserInfo newUserInfo = new UserInfo(userInfo.getName(), userInfo.getSurname(), userInfo.getEmail(), appUserRepository.save(newUser));
        userInfoRepository.save(newUserInfo);
        redirect.addFlashAttribute("success", "Uživatel " + newUser.getLogin() + " s rolí " + newUser.getRight().getDescription() + "a heslem " + password + " byl úspěšně vytvořen.");
        return new ModelAndView("redirect:/showOrganizationUsers");
    }

    /**
     * Opens form for creating a new organization admin.
     *
     * @param authentication - Authentication information of the user calling this action.
     * @param organizationId - Id of the organization for which the new admin is going to be created.
     * @param redirect       - Used to save redirect messages.
     * @return - View with the form.
     */
    @GetMapping("/createOrganizationAdminUser")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView createOrganizationAdminUser(final Authentication authentication, @RequestParam final int organizationId, final RedirectAttributes redirect) {
        final Organization organization = organizationRepository.findById(organizationId);
        if (organization == null) {
            redirect.addFlashAttribute("error", "ID organizace je neplatné.");
            return new ModelAndView("redirect:/showRegion");
        }
        final AppUser user = appUserRepository.findByLogin(authentication.getName());
        if (user.getRegion().getId() != organization.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se vytvořit administrátora organizace v cizím kraji!.");
            return new ModelAndView("redirect:/showRegion");
        }
        final UserInfo newUserInfo = new UserInfo();
        final AppUser newUser = new AppUser();
        newUser.setOrganization(organization);
        newUserInfo.setUser(newUser);
        return new ModelAndView("createOrganizationAdminUser").addObject("userInfo", newUserInfo).addObject("organization", organization);
    }

    /**
     * Saves the new admin of the selected organization into the database.
     *
     * @param userInfo       - Information about the new admin.
     * @param result         - Result of the automated validation.
     * @param redirect       - Used to save redirect messages.
     * @param authentication - Authentication information of the user calling this action.
     * @return - In case of validation error returns the previous view with the form. In other cases redirects user to other action.
     */
    @PostMapping("/saveOrganizationAdminUser")
    @Secured("ROLE_RegionAdmin")
    public ModelAndView saveOrganizationAdminUser(@Valid final UserInfo userInfo, final BindingResult result, final RedirectAttributes redirect, final Authentication authentication) {
        if (result.hasErrors()) {
            return new ModelAndView("createOrganizationAdminUser").addObject(userInfo).addObject("organization", userInfo.getUser().getOrganization());
        }
        if (appUserRepository.existsByLogin(userInfo.getUser().getLogin())) {
            return new ModelAndView("createOrganizationAdminUser").addObject(userInfo).addObject("organization", userInfo.getUser().getOrganization()).addObject("error", "Zadané uživatelské jméno už existuje. Zadejte prosím jiné.");
        }
        if (userInfoRepository.existsByEmail(userInfo.getEmail())) {
            return new ModelAndView("createOrganizationAdminUser").addObject(userInfo).addObject("organization", userInfo.getUser().getOrganization()).addObject("error", "Zadaný email je už použit jiným uživatelem. Zadejte prosím jiný.");
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (userInfo.getUser().getOrganization().getRegion().getId() != admin.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Snažíte se vytvořit účet administrátora organizaci cizího kraje!");
            return new ModelAndView("redirect:/showRegion");
        }
        final String password = StringUtils.randomAlphanumeric(30);
        final AppUser newUser = new AppUser(userInfo.getUser().getLogin(), new BCryptPasswordEncoder().encode(password), rightRepository.findById(4), true, userInfo.getUser().getOrganization());
        final UserInfo newUserInfo = new UserInfo(userInfo.getName(), userInfo.getSurname(), userInfo.getEmail(), appUserRepository.save(newUser));
        userInfoRepository.save(newUserInfo);
        redirect.addFlashAttribute("success", "Uživatel " + newUser.getLogin() + " s rolí " + newUser.getRight().getDescription() + "a heslem " + password + " organizace " + newUser.getOrganization().getName() + " byl úspěšně vytvořen.");
        return new ModelAndView("redirect:/showRegionOrganizationUsers?organizationId=" + newUserInfo.getUser().getOrganization().getId());
    }

    /**
     * Shows users of the selected organization.
     * @param organizationId - Id of the seleced organization, which users are going to be shown.
     * @param name - Filters users by name. Optional parameter.
     * @param surname - Filters users by surname. Optional parameter.
     * @param currentPage - Current page of the paged result. Starts at 0.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect - Used to save redirect messages.
     * @return - View showing the users. In case of error redirects user to other action.
     */
    @GetMapping("/showRegionOrganizationUsers")
    @Secured({"ROLE_RegionAdmin"})
    public ModelAndView showRegionOrganizationUsers(@RequestParam final int organizationId, @RequestParam(required = false) final String name, @RequestParam(required = false) final String surname, @RequestParam(required = false, defaultValue = "0") final int currentPage, final Authentication authentication, final RedirectAttributes redirect) {
        final Organization organization = organizationRepository.findById(organizationId);
        if (organization == null) {
            redirect.addFlashAttribute("error", "Zadané ID organizace je neplatné.");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (admin.getRegion().getId() != organization.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete si zobrazit organizaci cizího kraje!");
            return new ModelAndView("redirect:/showRegionOrganizations");
        }
        final Page<UserInfo> userInfoPage;
        final boolean isNameNull;
        final boolean isSurnameNull;
        //Amount of instances to be loaded into one page.
        final int pageItemsAmount = 5;
        if (name == null || name.isBlank()) {
            isNameNull = true;
        } else {
            isNameNull = false;
        }
        if (surname == null || surname.isBlank()) {
            isSurnameNull = true;
        } else {
            isSurnameNull = false;
        }
        final int numberOfPages;
        if (isSurnameNull && isNameNull) {
            userInfoPage = userInfoRepository.findAllByUserOrganization(organization, PageRequest.of(currentPage, pageItemsAmount));
        } else if (!isSurnameNull && !isNameNull) {
            userInfoPage = userInfoRepository.findByUserOrganizationAndNameContainingAndSurnameContaining(organization, name, surname, PageRequest.of(currentPage, pageItemsAmount));
        } else if (!isNameNull) {
            userInfoPage = userInfoRepository.findByUserOrganizationAndNameContaining(organization, name, PageRequest.of(currentPage, pageItemsAmount));
        } else {
            userInfoPage = userInfoRepository.findByUserOrganizationAndSurnameContaining(organization, surname, PageRequest.of(currentPage, pageItemsAmount));
        }
        numberOfPages = userInfoPage.getTotalPages();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<UserInfo> userInfos = userInfoPage.getContent();
        return new ModelAndView("showRegionOrganizationUsers").addObject("userInfos", userInfos).addObject("name", name).addObject("surname", surname).addObject("currentPage", currentPage).addObject("pages", pages).addObject("organization", organization);
    }

    /**
     * Shows users of the organization of the admin.
     * @param authentication - Authentication information of the user calling this action.
     * @param name - Filters users by name. Optional parameter.
     * @param surname - Filters users by surname. Optional parameter.
     * @param currentPage - Current page of the paged result. Starts at 0.
     * @return - View showing the organization users.
     */
    @GetMapping("/showOrganizationUsers")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView showOrganizationUsers(final Authentication authentication, @RequestParam(required = false) final String name, @RequestParam(required = false) final String surname, @RequestParam(required = false, defaultValue = "0") final int currentPage) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final Page<UserInfo> userInfoPage;
        final boolean isNameNull;
        final boolean isSurnameNull;
        //Amount of instances to be loaded into one page.
        final int pageItemsAmount = 5;
        if (name == null || name.isBlank()) {
            isNameNull = true;
        } else {
            isNameNull = false;
        }
        if (surname == null || surname.isBlank()) {
            isSurnameNull = true;
        } else {
            isSurnameNull = false;
        }
        final int numberOfPages;
        if (isSurnameNull && isNameNull) {
            userInfoPage = userInfoRepository.findAllByUserOrganization(admin.getOrganization(), PageRequest.of(currentPage, pageItemsAmount));
        } else if (!isSurnameNull && !isNameNull) {
            userInfoPage = userInfoRepository.findByUserOrganizationAndNameContainingAndSurnameContaining(admin.getOrganization(), name, surname, PageRequest.of(currentPage, pageItemsAmount));
        } else if (!isNameNull) {
            userInfoPage = userInfoRepository.findByUserOrganizationAndNameContaining(admin.getOrganization(), name, PageRequest.of(currentPage, pageItemsAmount));
        } else {
            userInfoPage = userInfoRepository.findByUserOrganizationAndSurnameContaining(admin.getOrganization(), surname, PageRequest.of(currentPage, pageItemsAmount));
        }
        numberOfPages = userInfoPage.getTotalPages();
        final int[] pages = new int[numberOfPages];
        for (int i = 0; i < pages.length; i++) {
            pages[i] = i;
        }
        final List<UserInfo> userInfos = userInfoPage.getContent();
        return new ModelAndView("showOrganizationUsers").addObject("userInfos", userInfos).addObject("name", name).addObject("surname", surname).addObject("currentPage", currentPage).addObject("pages", pages);
    }

    /**
     * Opens form for disabling user of the admin's organization.
     *
     * @param userId         - Id of the selected user to be disabled.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - View with the form. In case of error, redirects user to other action.
     */
    @GetMapping("/disableOrganizationUser")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView disableOrganizationUser(@RequestParam final int userId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getOrganization() == null) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat uživatele kraje!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getOrganization().getId() != admin.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat uživatele cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat sebe!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getRight().getId() < 5) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat jiné administrátory!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (!user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel už je zablokovaný.");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        return new ModelAndView("disableOrganizationUser").addObject("user", user).addObject("appUserDisabledInfo", new AppUserDisabledInfo());
    }

    /**
     * Updates disabled user in the database. Also creates new disabled user info entry or updates the existing one.
     *
     * @param appUserDisabledInfo - Information about the user who is going to be disabled.
     * @param result              - Result of the automated validation.
     * @param userId              - Id of the user who is going to be disabled.
     * @param redirect            - Used to save redirect messages.
     * @param authentication      - Authentication information of the user calling this action.
     * @return - In case of validation error returns the previous view with the form. Otherwise redirects user to other action.
     */
    @PostMapping("/saveDisabledOrganizationUser")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView saveDisabledOrganizationUser(@Valid final AppUserDisabledInfo appUserDisabledInfo, final BindingResult result, @RequestParam final int userId, final RedirectAttributes redirect, final Authentication authentication) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (result.hasErrors()) {
            return new ModelAndView("disableOrganizationUser").addObject("user", user).addObject("appUserDisabledInfo", appUserDisabledInfo);
        }
        if (user.getOrganization() == null) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat uživatele kraje!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getOrganization().getId() != admin.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat uživatele cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat sebe!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (user.getRight().getId() < 5) {
            redirect.addFlashAttribute("error", "Nemůžete zablokovat jiné administrátory!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        if (!user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel už je zablokovaný.");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        final AppUserDisabledInfo savedAppUserDisabledInfo = new AppUserDisabledInfo(appUserDisabledInfo.getReason(), admin);
        if (user.getAppUserDisabledInfo() != null) {
            savedAppUserDisabledInfo.setId(user.getAppUserDisabledInfo().getId());
            appUserDisabledInfoRepository.save(savedAppUserDisabledInfo);
        } else {
            savedAppUserDisabledInfo.setId(appUserDisabledInfoRepository.save(savedAppUserDisabledInfo).getId());
        }
        user.setAppUserDisabledInfo(savedAppUserDisabledInfo);
        user.setEnabled(false);
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Uživatel " + user.getLogin() + " byl úspěšně zablokován.");
        return new ModelAndView("redirect:/showOrganizationUsers");
    }

    /**
     * Enable user of the admin's organization.
     *
     * @param userId         - Id of the user who is going to be enabled.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - Redirects user to other action.
     */
    @GetMapping("/enableOrganizationUser")
    @Secured("ROLE_OrganizationAdmin")
    public RedirectView enableOrganizationUser(@RequestParam final int userId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new RedirectView("/showOrganizationUsers", true);
        }
        if (user.getOrganization().getId() != admin.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete odblokovat uživatele cizí organizace!");
            return new RedirectView("/showOrganizationUsers", true);
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete odblokovat sebe!");
            return new RedirectView("/showOrganizationUsers", true);
        }
        if (user.getRight().getId() < 5) {
            redirect.addFlashAttribute("error", "Nemůžete odblokovat jiné administrátory!");
            return new RedirectView("/showOrganizationUsers", true);
        }
        if (user.isEnabled()) {
            redirect.addFlashAttribute("error", "Uživatel není zablokován.");
            return new RedirectView("/showOrganizationUsers", true);
        }
        user.setEnabled(true);
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Uživatel " + user.getLogin() + " byl úspěšně odblokován.");
        return new RedirectView("/showOrganizationUsers", true);
    }

    /**
     * Opens form for changing user's information by organization admin.
     *
     * @param userId         - Id of the user whose information is going to be changed.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - View with the form. In case of error redirects user to other action.
     */
    @GetMapping("/changeOrganizationUserInfo")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView changeOrganizationUserInfo(@RequestParam final int userId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (user.getOrganization() == null || user.getOrganization().getId() != admin.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete měnit údaje uživatele cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        final UserInfo userInfo = userInfoRepository.findByUser(user);
        return new ModelAndView("changeOrganizationUserInfo").addObject(userInfo);
    }

    /**
     * Updates the changed information of the user in the database.
     *
     * @param userInfo       - Changed information about the selected user.
     * @param result         - Result of the automated validation.
     * @param authentication -Authentication information of the user.
     * @param redirect       - Used to save redirect messages.
     * @return - In case of validation error return the previous view with the form. Otherwise redirects user to other action.
     */
    @PostMapping("/saveChangedOrganizationUserInfo")
    @Secured("ROLE_OrganizationAdmin")
    public ModelAndView saveChangedOrganizationUserInfo(@Valid final UserInfo userInfo, final BindingResult result, final Authentication authentication, final RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("changeOrganizationUserInfo").addObject(userInfo);
        }
        final UserInfo changedUsedInfo = userInfoRepository.findById(userInfo.getId());
        if (!userInfo.getEmail().equals(changedUsedInfo.getEmail()) && userInfoRepository.existsByEmail(userInfo.getEmail())) {
            redirect.addFlashAttribute("error", "Zadaná emailová adresa je už použita jiným uživatelem. Zadejte prosím jinou.");
            return new ModelAndView("changeOrganizationUserInfo").addObject(userInfo);
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (changedUsedInfo.getUser().getOrganization() == null || changedUsedInfo.getUser().getOrganization().getId() != admin.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete změnit informace o uživateli cizí organizace!");
            return new ModelAndView("redirect:/showOrganizationUsers");
        }
        changedUsedInfo.updateUserInfo(userInfo);
        userInfoRepository.save(changedUsedInfo);
        redirect.addFlashAttribute("success", "Údaje uživatele " + changedUsedInfo.getUser().getLogin() + " byly úspěšně změněny.");
        return new ModelAndView("redirect:/showOrganizationUsers");
    }

    /**
     * Resets password of the selected used by the randomly generated one.
     *
     * @param userId         - Id of the user whose password is going to be reset.
     * @param organizationId - Id of the organization to which the selected user belongs.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - Redirects user to other action.
     */
    @GetMapping("/resetRegionOrganizationUserPassword")
    @Secured("ROLE_RegionAdmin")
    public RedirectView resetRegionOrganizationUserPassword(@RequestParam final int userId, @RequestParam final int organizationId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            final String error = "Id uživatele je neplatné.";
            redirect.addFlashAttribute("error", error);
            final Organization organization = organizationRepository.findById(organizationId);
            if (organization == null) {
                redirect.addFlashAttribute("error", error + " Id organizace je také neplatné. Vracím vás zpět na přehled organizací vašeho kraje.");
                return new RedirectView("/showRegionOrganizations", true);
            }
            if (organization.getRegion().getId() != admin.getOrganization().getRegion().getId()) {
                redirect.addFlashAttribute("error", error + " A nemůžete měnit údaje uživatelů organizací cizího kraje! Vracím vás zpět na přehled organizací vašeho kraje.");
                return new RedirectView("/showRegionOrganizations", true);
            }
            return new RedirectView("showRegionOrganizationUsers?organizationId=" + organizationId, true);
        }
        if (user.getOrganization().getRegion().getId() != admin.getRegion().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete resetovat heslo uživateli cizího kraje!");
            return new RedirectView("/showRegionOrganizations", true);
        }
        final String password = StringUtils.randomAlphanumeric(30);
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Heslo uživatele " + user.getLogin() + " bylo úspěšně resetováno na.: " + password);
        return new RedirectView("/showRegionOrganizationUsers?organizationId=" + organizationId, true);
    }

    /**
     * Resets password of the selected used by the randomly generated one.
     *
     * @param userId         - Id of the user whose password is going to be reset.
     * @param authentication - Authentication information of the user calling this action.
     * @param redirect       - Used to save redirect messages.
     * @return - Redirects user to other action.
     */
    @GetMapping("/resetOrganizationUserPassword")
    @Secured("ROLE_OrganizationAdmin")
    public RedirectView resetOrganizationUserPassword(@RequestParam final int userId, final Authentication authentication, final RedirectAttributes redirect) {
        final AppUser user = appUserRepository.findById(userId);
        if (user == null) {
            redirect.addFlashAttribute("error", "Id uživatele je neplatné.");
            return new RedirectView("/showOrganizationUsers", true);
        }
        final AppUser admin = appUserRepository.findByLogin(authentication.getName());
        if (user.getOrganization() == null || user.getOrganization().getId() != admin.getOrganization().getId()) {
            redirect.addFlashAttribute("error", "Nemůžete resetovat heslo uživatele cizí organizace!");
            return new RedirectView("/showOrganizationUsers", true);
        }
        if (user.getId() == admin.getId()) {
            redirect.addFlashAttribute("error", "Nemůžete resetovat svoje vlastní heslo.");
            return new RedirectView("/showOrganizationUsers", true);
        }
        final String password = StringUtils.randomAlphanumeric(30);
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        appUserRepository.save(user);
        redirect.addFlashAttribute("success", "Heslo uživatele " + user.getLogin() + " bylo úspěšně resetováno na.: " + password);
        return new RedirectView("/showOrganizationUsers", true);
    }

    /**
     * Checks if the administrator of the region is in the same one as the other user.
     *
     * @param regionAdminUser Region admin.
     * @param otherUser       Other user which is going to be compared with the admin one.
     * @return True of the admin is the admin of an region and is from the same region as the other user. Otherwise, returns false.
     */
    private boolean isRegionAdminSameRegionAsUser(final AppUser regionAdminUser, final AppUser otherUser) {
        return regionAdminUser.getRight().getId() == 2 && regionAdminUser.getRegion().getId() == otherUser.getRegion().getId();
    }
}
