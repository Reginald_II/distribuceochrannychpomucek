package cz.uhk.distribuceOchrannychPomucek.failureHandler;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Implements custom handling of an authentication failure.
 */
@Component
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException exception) throws IOException, ServletException {
        final String redirectURL;
        //Checks if the failure was caused by the account being disabled.
        if (exception instanceof DisabledException) {
            final String login = request.getParameter("login");
            final AppUser appUser = appUserRepository.findByLogin(login);
            redirectURL = "/loginFailure?loginErrorCode=" + 2 + "&appUserId=" + appUser.getId();
        } else {
            redirectURL = "/loginFailure?loginErrorCode=1";
        }
        super.setDefaultFailureUrl(redirectURL);
        super.onAuthenticationFailure(request, response, exception);
    }
}
