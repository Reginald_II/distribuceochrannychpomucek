package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Class representing user of the application. Contains information and references used to authenticate and authorize the user. Also references the organization or region of the user, if he is assigned to one.
 */
@Entity
@Table(name = "app_user")
public class AppUser implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "login")
    @NotBlank(message = "Prosím vyplňte vaše přihlašovací jméno.")
    @Size(max = 50, message = "Přihlašovací jméno smí mít maximálně 50 znaků!")
    private String login;

    @Column(name = "password")
    @NotBlank(message = "Prosím vyplňte vaše heslo!")
    @Size(max = 500, message = "Heslo smí mít maximálně 500 znaků.")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @ManyToOne
    @JoinColumn(name = "right_id")
    private Right right;

    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;

    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;

    @ManyToOne
    @JoinColumn(name = "disabled_info_id")
    private AppUserDisabledInfo appUserDisabledInfo;

    public AppUser() {
    }

    /**
     * Constructor for creating a new global admin user.
     *
     * @param login
     * @param password
     * @param right
     * @param enabled
     */
    public AppUser(final String login, final String password, final Right right, final boolean enabled) {
        this.login = login;
        this.password = password;
        this.right = right;
        this.enabled = enabled;
    }

    /**
     * Constructor for creating a new region user.
     *
     * @param login
     * @param password
     * @param right
     * @param enabled
     * @param region
     */
    public AppUser(final String login, final String password, final Right right, final boolean enabled, final Region region) {
        this.login = login;
        this.password = password;
        this.right = right;
        this.enabled = enabled;
        this.region = region;
    }

    /**
     * Constructor for creating new organization user.
     *
     * @param login
     * @param password
     * @param right
     * @param enabled
     * @param organization
     */
    public AppUser(final String login, final String password, final Right right, final boolean enabled, final Organization organization) {
        this.login = login;
        this.password = password;
        this.right = right;
        this.enabled = enabled;
        this.organization = organization;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(final Organization organization) {
        this.organization = organization;
    }

    public Right getRight() {
        return right;
    }

    public void setRight(Right right) {
        this.right = right;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    public AppUserDisabledInfo getAppUserDisabledInfo() {
        return appUserDisabledInfo;
    }

    public void setAppUserDisabledInfo(final AppUserDisabledInfo appUserDisabledInfo) {
        this.appUserDisabledInfo = appUserDisabledInfo;
    }


}
