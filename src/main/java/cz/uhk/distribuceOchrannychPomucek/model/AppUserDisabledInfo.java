package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Contains information about disabled user.
 */
@Entity
@Table(name = "app_user_disabled_info")
public class AppUserDisabledInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "reason", nullable = false)
    @NotBlank(message = "Prosím vyplňte důvod blokace uživatele.")
    @Size(max = 500, message = "Důvod blokace smí mít maximálně 500 znaků. Prosím zkraťte ho.")
    private String reason;

    //The banning admin.
    @ManyToOne
    @JoinColumn(name = "app_user_id")
    private AppUser user;

    public AppUserDisabledInfo() {
    }

    /**
     * Creates new instance of this class, with the reason and the admin who is disabling the other user.
     *
     * @param reason - Reason of the disabling action.
     * @param user   - Admin who is doing the action.
     */
    public AppUserDisabledInfo(final String reason, final AppUser user) {
        this.reason = reason;
        this.user = user;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(final AppUser user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * Updates the ban information with the new one from the method parameter
     *
     * @param appUserDisabledInfo Instance containing new info, which is going to overwrite the old one.
     */
    public void updateInfo(final AppUserDisabledInfo appUserDisabledInfo) {
        this.reason = appUserDisabledInfo.getReason();
        this.user = appUserDisabledInfo.getUser();
    }
}
