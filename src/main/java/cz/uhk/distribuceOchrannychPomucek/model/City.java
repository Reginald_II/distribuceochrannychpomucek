package cz.uhk.distribuceOchrannychPomucek.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A model class which contains information about a city.
 *
 * @author Artur Hamza
 */
@Entity
@Table(name = "city")
public class City implements Serializable {
    @Id
    @Column(name = "psc", length = 5, nullable = false)
    @Min(value = 1, message = "Prosím vyplňte PSČ! Pokud jste ho zadal s mezerami, zadejte ho prosím znova bez mezer!")
    @Max(value = 99999, message = "PSČ nesmí přesahovat hodnotu 999 99!")
    private int psc;
    @Column(name = "name", length = 50, nullable = false)
    @NotBlank(message = "Prosím vyplňte název města!")
    @Size(max = 100, message = "Název města nesmí přesahovat 100 znaků!")
    private String name;

    public City() {
    }

    public City(final int psc, final String name) {
        this.psc = psc;
        this.name = name;
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
