package cz.uhk.distribuceOchrannychPomucek.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Contains information about organizations.
 */
@Entity
@Table(name = "organization")
public class Organization implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;


    @Column(name = "name", nullable = false, unique = true)
    @Length(max = 100, message = "Název organizace smí mít maximálně 100 znaků!")
    @NotBlank(message = "Název organizace musí být vyplněný!")
    private String name;

    @Column(name = "street", nullable = false)
    @Length(max = 100, message = "Jméno ulice smí mít maximálně 100 znaků!")
    @NotBlank(message = "Název ulice nesmí být prázdný!")
    private String street;

    @ManyToOne
    @JoinColumn(name = "city_psc")
    private City city;

    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;

    public Organization() {
    }

    public Organization(final String name, final String street, final City city, final Region region) {
        this.name = name;
        this.street = street;
        this.city = city;
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }

    public City getCity() {
        return city;
    }

    public void setCity(final City city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    /**
     * Updates the attributes which don't reference organization, nor are id of the organization to avoid unwanted reference changes or new row creating in the database.
     *
     * @param updateInfo Instance of Organization containing the info to update.
     */
    public void updateOrganizationAttributes(final Organization updateInfo) {
        name = updateInfo.getName();
        city = updateInfo.getCity();
        street = updateInfo.getStreet();

    }
}
