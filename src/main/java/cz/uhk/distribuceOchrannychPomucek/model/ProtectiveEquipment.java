package cz.uhk.distribuceOchrannychPomucek.model;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Contains definition of a protective equipment
 */
@Entity
@Table(name = "protective_equipment")
public class ProtectiveEquipment implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false)
    @NotBlank(message = "Prosím vyplňte název ochranné pomůcky!")
    @Size(max = 50, message = "Název ochranné pomůcky nesmí být větší než 50 znaků!")
    private String name;

    @Column(name = "description", nullable = false)
    @NotBlank(message = "Prosím vyplňte popis ochranné pomůcky.")
    @Size(max = 100, message = "Popis ochranné pomůcky nesmí překračovat 100 znaků!")
    private String description;


    public ProtectiveEquipment() {
    }

    public ProtectiveEquipment(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
