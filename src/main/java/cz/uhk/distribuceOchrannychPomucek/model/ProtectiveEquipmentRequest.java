package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Contains information about the protective equipment requests made by the organizations.
 */
@Entity
@Table(name = "protective_equipment_request")
public class ProtectiveEquipmentRequest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "message")
    @Size(max = 500, message = "Zpráva smí mít maximálně 500 znaků. Prosím, zkraťte ji.")
    private String message;
    /**
     * Response from a region in form of a message.
     */
    @Column(name = "region_message")
    @Size(max = 500, message = "Zpráva smí mít maximálně 500 znaků. Prosím, zkraťte ji.")
    private String regionMessage;
    /**
     * Organization sending the request.
     */
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;
    /**
     * User who created the request.
     */
    @ManyToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;
    /**
     * Status of the request.
     */
    @ManyToOne
    @JoinColumn(name = "request_status_id", nullable = false)
    private ProtectiveEquipmentRequestStatus protectiveEquipmentRequestStatus;

    public ProtectiveEquipmentRequest(final Organization organization, final AppUser appUser, final ProtectiveEquipmentRequestStatus protectiveEquipmentRequestStatus) {
        this.organization = organization;
        this.appUser = appUser;
        this.protectiveEquipmentRequestStatus = protectiveEquipmentRequestStatus;
    }

    public ProtectiveEquipmentRequest() {
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(final AppUser appUser) {
        this.appUser = appUser;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(final Organization organization) {
        this.organization = organization;
    }

    public ProtectiveEquipmentRequestStatus getProtectiveEquipmentRequestStatus() {
        return protectiveEquipmentRequestStatus;
    }

    public void setProtectiveEquipmentRequestStatus(final ProtectiveEquipmentRequestStatus protectiveEquipmentRequestStatus) {
        this.protectiveEquipmentRequestStatus = protectiveEquipmentRequestStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getRegionMessage() {
        return regionMessage;
    }

    public void setRegionMessage(final String regionMessage) {
        this.regionMessage = regionMessage;
    }
}
