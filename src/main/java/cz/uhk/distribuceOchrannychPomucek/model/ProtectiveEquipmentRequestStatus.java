package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Contains information about a status of protective equipment requests.
 */
@Entity
@Table(name = "protective_equipment_request_status")
public class ProtectiveEquipmentRequestStatus implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}