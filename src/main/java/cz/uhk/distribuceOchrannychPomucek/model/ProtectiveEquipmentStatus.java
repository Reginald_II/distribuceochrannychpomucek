package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * Contains information of a protective equipment status in each region or organization.
 */
@Entity
@Table(name = "protective_equipment_status")
public class ProtectiveEquipmentStatus implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    /**
     * Quantity of the protective equipments.
     */
    @Column(name = "quantity", nullable = false)
    @Min(value = 0, message = "Množství ochranných pomůcek nesmí být menší než 0.")
    private int quantity;
    /**
     * Protective equipment, which the status references.
     */
    @ManyToOne
    @JoinColumn(name = "protective_equipment_id", nullable = false)
    private ProtectiveEquipment protectiveEquipment;
    /**
     * Region, to which this status belongs.
     */
    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
    /**
     * Organization, to which this status belongs.
     */
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;

    /**
     * Constructs a default protective equipment status for a region and protective equipment.
     *
     * @param protectiveEquipment Protective equipment which is going to be referred in this instance.
     * @param region              Referred region.
     */
    public ProtectiveEquipmentStatus(final ProtectiveEquipment protectiveEquipment, final Region region) {
        this.protectiveEquipment = protectiveEquipment;
        this.region = region;
        quantity = 0;
    }

    /**
     * Constructs a default protective equipment status for a region and protective equipment.
     *
     * @param protectiveEquipment - Protective equipment which is going to be referenced in this instance.
     * @param organization        - Referred region.
     */
    public ProtectiveEquipmentStatus(final ProtectiveEquipment protectiveEquipment, final Organization organization) {
        this.protectiveEquipment = protectiveEquipment;
        this.organization = organization;
        quantity = 0;
    }

    /**
     * Default constructor for the frameworks.
     */
    public ProtectiveEquipmentStatus() {
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }


    public ProtectiveEquipment getHealthEquipment() {
        return protectiveEquipment;
    }

    public void setProtectiveEquipment(ProtectiveEquipment protectiveEquipment) {
        this.protectiveEquipment = protectiveEquipment;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ProtectiveEquipment getProtectiveEquipment() {
        return protectiveEquipment;
    }

    /**
     * Adds the amount from the parameter to the quantity variable.
     *
     * @param amount Amount of the protective equipments to either add or deduced from the amount depending whenever it's positive or negative number.
     * @return Returns true if the quantity is positive number or 0, returns false if it's the negative one instead.
     */
    public boolean addAmount(final int amount) {
        final int quantityAfterAdded = quantity + amount;
        if (quantityAfterAdded > -1) {
            quantity = quantityAfterAdded;
            return true;
        } else {
            return false;
        }
    }
}
