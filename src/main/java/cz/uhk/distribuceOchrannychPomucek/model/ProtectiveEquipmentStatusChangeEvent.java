package cz.uhk.distribuceOchrannychPomucek.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Contains information about an event of a protective equipment status change.
 */
@Entity
@Table(name = "protective_equipment_status_change_event")
public class ProtectiveEquipmentStatusChangeEvent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "previous_quantity", nullable = false)
    private int previousQuantity;
    @Column(name = "new_quantity", nullable = false)
    private int newQuantity;
    @Column(name = "comment")
    @NotBlank(message = "Prosím vyplňte důvod změny.")
    @Length(max = 500, message = "Důvod změny smí mít maximálně 500 znaků. Prosím zkraťte ho.")
    private String comment;

    @Column(name = "time_of_event", nullable = false)
    private Date timeOfEvent;

    /**
     * Id of bill or other document connected with the change.
     */
    @Column(name = "billId")
    private long billId;
    /**
     * Protective equipment status, which was changed during the event.
     */
    @ManyToOne
    @JoinColumn(name = "protective_equipment_status_id")
    private ProtectiveEquipmentStatus protectiveEquipmentStatus;
    /**
     * The user who did the event.
     */
    @ManyToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;
    /**
     * The type of the event.
     */
    @ManyToOne
    @JoinColumn(name = "protective_equipment_status_change_event_type_id")
    private ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType;
    /**
     * The region, to which the protective equipment status belongs.
     */
    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
    /**
     * The organization, to which the protective equipment status belongs.
     */
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(final Organization organization) {
        this.organization = organization;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }

    public ProtectiveEquipmentStatusChangeEventType getProtectiveEquipmentStatusChangeEventType() {
        return protectiveEquipmentStatusChangeEventType;
    }

    public ProtectiveEquipmentStatusChangeEvent() {
    }

    /**
     * Creates a new instance of the class for an organization.
     *
     * @param timeOfEvent
     * @param previousQuantity
     * @param newQuantity
     * @param comment
     * @param protectiveEquipmentStatus
     * @param appUser
     * @param protectiveEquipmentStatusChangeEventType
     * @param organization
     * @param billId
     */
    public ProtectiveEquipmentStatusChangeEvent(final Date timeOfEvent, final int previousQuantity, final int newQuantity, final String comment, final ProtectiveEquipmentStatus protectiveEquipmentStatus, final AppUser appUser, final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType, final Organization organization, final long billId) {
        this.timeOfEvent = timeOfEvent;
        this.previousQuantity = previousQuantity;
        this.newQuantity = newQuantity;
        this.comment = comment;
        this.protectiveEquipmentStatus = protectiveEquipmentStatus;
        this.appUser = appUser;
        this.protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventType;
        this.organization = organization;
        this.billId = billId;
    }

    /**
     * Creates a new instance of the class for a region.
     *
     * @param timeOfEvent
     * @param previousQuantity
     * @param newQuantity
     * @param comment
     * @param protectiveEquipmentStatus
     * @param appUser
     * @param protectiveEquipmentStatusChangeEventType
     * @param region
     * @param billId
     */
    public ProtectiveEquipmentStatusChangeEvent(final Date timeOfEvent, final int previousQuantity, final int newQuantity, final String comment, final ProtectiveEquipmentStatus protectiveEquipmentStatus, final AppUser appUser, final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType, final Region region, final long billId) {
        this.timeOfEvent = timeOfEvent;
        this.previousQuantity = previousQuantity;
        this.newQuantity = newQuantity;
        this.comment = comment;
        this.protectiveEquipmentStatus = protectiveEquipmentStatus;
        this.appUser = appUser;
        this.protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventType;
        this.region = region;
        this.billId = billId;
    }

    /**
     * Creates a new instance of the class for a region with preset comment. Used for case of accepting of the protective equipment request by a region.
     *
     * @param timeOfEvent
     * @param previousQuantity
     * @param newQuantity
     * @param protectiveEquipmentStatus
     * @param appUser
     * @param protectiveEquipmentStatusChangeEventType
     * @param region
     * @param billId
     */
    public ProtectiveEquipmentStatusChangeEvent(final Date timeOfEvent, final int previousQuantity, final int newQuantity, final ProtectiveEquipmentStatus protectiveEquipmentStatus, final AppUser appUser, final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType, final Region region, final long billId) {
        comment = "Odeslání v rámci přijetí požadavku organizace.";
        this.timeOfEvent = timeOfEvent;
        this.previousQuantity = previousQuantity;
        this.newQuantity = newQuantity;
        this.protectiveEquipmentStatus = protectiveEquipmentStatus;
        this.appUser = appUser;
        this.protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventType;
        this.region = region;
        this.billId = billId;
    }

    /**
     * Creates a new instance of the class for an organization with preset comment. Used for case of accepting a protective equipment transfer by an organization.
     *
     * @param timeOfEvent
     * @param previousQuantity
     * @param newQuantity
     * @param protectiveEquipmentStatus
     * @param appUser
     * @param protectiveEquipmentStatusChangeEventType
     * @param organization
     * @param billId
     */
    public ProtectiveEquipmentStatusChangeEvent(final Date timeOfEvent, final int previousQuantity, final int newQuantity, final ProtectiveEquipmentStatus protectiveEquipmentStatus, final AppUser appUser, final ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType, final Organization organization, final Long billId) {
        comment = "Přijetí od dodávky kraje.";
        this.timeOfEvent = timeOfEvent;
        this.previousQuantity = previousQuantity;
        this.newQuantity = newQuantity;
        this.protectiveEquipmentStatus = protectiveEquipmentStatus;
        this.appUser = appUser;
        this.protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventType;
        this.organization = organization;
        this.billId = billId;
    }

    public void setProtectiveEquipmentStatusChangeEventType(ProtectiveEquipmentStatusChangeEventType protectiveEquipmentStatusChangeEventType) {
        this.protectiveEquipmentStatusChangeEventType = protectiveEquipmentStatusChangeEventType;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(final AppUser appUser) {
        this.appUser = appUser;
    }

    public ProtectiveEquipmentStatus getProtectiveEquipmentStatus() {
        return protectiveEquipmentStatus;
    }

    public void setProtectiveEquipmentStatus(final ProtectiveEquipmentStatus protectiveEquipmentStatus) {
        this.protectiveEquipmentStatus = protectiveEquipmentStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getPreviousQuantity() {
        return previousQuantity;
    }

    public void setPreviousQuantity(final int previousQuantity) {
        this.previousQuantity = previousQuantity;
    }

    public int getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(final int newQuantity) {
        this.newQuantity = newQuantity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }

    public Date getTimeOfEvent() {
        return timeOfEvent;
    }

    public void setTimeOfEvent(final Date timeOfEvent) {
        this.timeOfEvent = timeOfEvent;
    }

    public long getBillId() {
        return billId;
    }

    public void setBillId(final long billId) {
        this.billId = billId;
    }

    /**
     * Turn the important attributes to the string. The attributes are divided by semicolons.
     *
     * @return Attributes converted into one String.
     */
    @Override
    public String toString() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        final String date = simpleDateFormat.format(timeOfEvent);
        simpleDateFormat.applyPattern("HH:mm:ss");
        final String time = simpleDateFormat.format(timeOfEvent);
        return id + ";" + date + ";" + time + ";" + protectiveEquipmentStatus.getProtectiveEquipment().getName() + ";" + comment + ";" + billId + ";" + previousQuantity + ";" + (newQuantity - previousQuantity) + ";" + newQuantity + ";" + protectiveEquipmentStatusChangeEventType.getName();
    }

    /**
     * Turn the important attributes to the string. The attributes are divided by semicolons. Unlike the normal toString method, this one also contains a login of user doing the event.
     *
     * @return Attributes converted into one String.
     */
    public String toStringAdmin() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        final String date = simpleDateFormat.format(timeOfEvent);
        simpleDateFormat.applyPattern("HH:mm:ss");
        final String time = simpleDateFormat.format(timeOfEvent);
        return id + ";" + date + ";" + time + ";" + protectiveEquipmentStatus.getProtectiveEquipment().getName() + ";" + comment + ";" + appUser.getLogin() + ";" + billId + ";" + previousQuantity + ";" + (newQuantity - previousQuantity) + ";" + newQuantity + ";" + protectiveEquipmentStatusChangeEventType.getName();
    }
}

