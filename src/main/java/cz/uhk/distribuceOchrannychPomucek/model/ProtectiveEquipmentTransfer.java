package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Contains information about the protective equipment transfers.
 */
@Entity
@Table(name = "protective_equipment_transfer")
public class ProtectiveEquipmentTransfer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    /**
     * The region, which is sending the transfer.
     */
    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
    /**
     * Organization, which is going to receive the transfer.
     */
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;
    /**
     * Status of the transfer.
     */
    @ManyToOne
    @JoinColumn(name = "transfer_status_id")
    private ProtectiveEquipmentTransferStatus protectiveEquipmentTransferStatus;
    /**
     * Type of the transfer.
     */
    @ManyToOne
    @JoinColumn(name = "transfer_type_id")
    private ProtectiveEquipmentTransferType protectiveEquipmentTransferType;

    public ProtectiveEquipmentTransferType getProtectiveEquipmentTransferType() {
        return protectiveEquipmentTransferType;
    }

    public void setProtectiveEquipmentTransferType(final ProtectiveEquipmentTransferType protectiveEquipmentTransferType) {
        this.protectiveEquipmentTransferType = protectiveEquipmentTransferType;
    }

    public ProtectiveEquipmentTransfer(final Region region, final Organization organization, final ProtectiveEquipmentTransferStatus protectiveEquipmentTransferStatus, final ProtectiveEquipmentTransferType protectiveEquipmentTransferType) {
        this.region = region;
        this.organization = organization;
        this.protectiveEquipmentTransferStatus = protectiveEquipmentTransferStatus;
        this.protectiveEquipmentTransferType = protectiveEquipmentTransferType;
    }

    public ProtectiveEquipmentTransfer() {
    }

    public ProtectiveEquipmentTransferStatus getProtectiveEquipmentTransferStatus() {
        return protectiveEquipmentTransferStatus;
    }

    public void setProtectiveEquipmentTransferStatus(final ProtectiveEquipmentTransferStatus protectiveEquipmentTransferStatus) {
        this.protectiveEquipmentTransferStatus = protectiveEquipmentTransferStatus;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(final Organization organization) {
        this.organization = organization;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }


}
