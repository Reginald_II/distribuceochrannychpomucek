package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Contains information about the defined types of the protective equipment transfers.
 */
@Entity
@Table(name = "protective_equipment_transfer_type")
public class ProtectiveEquipmentTransferType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
