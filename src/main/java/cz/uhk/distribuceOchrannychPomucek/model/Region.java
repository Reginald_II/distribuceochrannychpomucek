package cz.uhk.distribuceOchrannychPomucek.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Contains information about a region.
 */
@Entity
@Table(name = "region")
public class Region implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "name", nullable = false, unique = true)
    @Length(max = 100, message = "Název kraje smí mít maximálně 100 znaků!")
    @NotBlank(message = "Název kraje musí být vyplněný!")
    private String name;
    /**
     * The street of the region office.
     */
    @Column(name = "street", nullable = false)
    @Length(max = 100, message = "Jméno ulice smí mít maximálně 100 znaků!")
    @NotBlank(message = "Název ulice nesmí být prázdný!")
    private String street;
    /**
     * The capital city of the region.
     */
    @ManyToOne
    @JoinColumn(name = "city_psc", nullable = false)
    @Valid
    private City city;

    public City getCity() {
        return city;
    }

    public void setCity(final City city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }
}
