package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * Contains information about a protective equipment added to a protective equipment request.
 */
@Entity
@Table(name = "request_protective_equipment")
public class RequestProtectiveEquipment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    /**
     * Quantity requested by an organization.
     */
    @Column(name = "quantity", nullable = false)
    private int quantity;
    /**
     * Approved quantity by the region.
     * Shouldn't be higher than the quantity variable.
     */
    @Column(name = "approved_quantity", nullable = false)
    @Min(value = 0, message = "Schválené množství nesmí být záporné.")
    private int approvedQuantity;
    /**
     * Protective equipment request, to which this instance of a request protective equipment belongs.
     */
    @ManyToOne
    @JoinColumn(name = "request_id")
    private ProtectiveEquipmentRequest protectiveEquipmentRequest;
    /**
     * The referenced protective equipment.
     */
    @ManyToOne
    @JoinColumn(name = "protective_equipment_id")
    private ProtectiveEquipment protectiveEquipment;

    public RequestProtectiveEquipment(final int quantity, final int approvedQuantity, final ProtectiveEquipmentRequest protectiveEquipmentRequest, final ProtectiveEquipment protectiveEquipment) {
        this.quantity = quantity;
        this.approvedQuantity = approvedQuantity;
        this.protectiveEquipmentRequest = protectiveEquipmentRequest;
        this.protectiveEquipment = protectiveEquipment;
    }

    public RequestProtectiveEquipment() {
    }

    public ProtectiveEquipment getProtectiveEquipment() {
        return protectiveEquipment;
    }

    public void setProtectiveEquipment(final ProtectiveEquipment protectiveEquipment) {
        this.protectiveEquipment = protectiveEquipment;
    }

    public ProtectiveEquipmentRequest getProtectiveEquipmentRequest() {
        return protectiveEquipmentRequest;
    }

    public void setProtectiveEquipmentRequest(final ProtectiveEquipmentRequest protectiveEquipmentRequest) {
        this.protectiveEquipmentRequest = protectiveEquipmentRequest;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    /**
     * Adds quantity amount from the parameter.
     *
     * @param quantity - Quantity which is going to be added to the quantity of the instance.
     */
    public void addQuantity(final int quantity) {
        this.quantity += quantity;
    }

    public int getApprovedQuantity() {
        return approvedQuantity;
    }

    public void setApprovedQuantity(final int approvedQuantity) {
        this.approvedQuantity = approvedQuantity;
    }
}
