package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Contains information about a protective equipment in a transfer.
 */
@Entity
@Table(name = "transfer_protective_equipment")
public class TransferProtectiveEquipment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "quantity")
    private int quantity;
    /**
     * Referenced protective equipment which is transferred.
     */
    @ManyToOne
    @JoinColumn(name = "protective_equipment_id")
    private ProtectiveEquipment protectiveEquipment;
    /**
     * Transfer, to which this instance belongs.
     */
    @ManyToOne
    @JoinColumn(name = "transfer_id")
    private ProtectiveEquipmentTransfer protectiveEquipmentTransfer;

    public TransferProtectiveEquipment(final int quantity, final ProtectiveEquipment protectiveEquipment, final ProtectiveEquipmentTransfer protectiveEquipmentTransfer) {
        this.quantity = quantity;
        this.protectiveEquipment = protectiveEquipment;
        this.protectiveEquipmentTransfer = protectiveEquipmentTransfer;
    }

    public TransferProtectiveEquipment() {
    }

    public ProtectiveEquipmentTransfer getProtectiveEquipmentTransfer() {
        return protectiveEquipmentTransfer;
    }

    public void setProtectiveEquipmentTransfer(final ProtectiveEquipmentTransfer protectiveEquipmentTransfer) {
        this.protectiveEquipmentTransfer = protectiveEquipmentTransfer;
    }

    public ProtectiveEquipment getProtectiveEquipment() {
        return protectiveEquipment;
    }

    public void setProtectiveEquipment(final ProtectiveEquipment protectiveEquipment) {
        this.protectiveEquipment = protectiveEquipment;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }
}
