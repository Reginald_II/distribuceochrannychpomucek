package cz.uhk.distribuceOchrannychPomucek.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Contains additional information about a user.
 */
@Entity
@Table(name = "user_info")
public class UserInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "name", nullable = false)
    @Size(max = 50, message = "Vaše jméno smí mít maximálně 50 znaků. Prosím zkraťte ho.")
    @NotBlank(message = "Prosím vyplňte vaše jméno.")
    private String name;
    @Column(name = "surname", nullable = false)
    @Size(max = 50, message = "Vaše přijmení smí mít maximálně 50 znaků. Prosím zkraťte ho.")
    @NotBlank(message = "Prosím vyplňte vaše přijmení.")
    private String surname;

    @Column(name = "email", nullable = false)
    @NotBlank(message = "Prosím vyplňte vaši emailovou adresu.")
    @Size(max = 50, message = "Váš email smí mít maximálně 50 znaků. Prosím zkraťte ho.")
    @Email(message = "Vaše emailová adresa je zadána špatně. Zadejte ji prosím znova.")
    private String email;
    /**
     * Reference to the AppUser class, which contains the basic, authentication and authorization necessary information of the user.
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private AppUser user;

    public UserInfo() {
    }

    public UserInfo(final String name, final String surname, final String email, final AppUser user) {
        this.name = name;
        this.surname = surname;
        this.user = user;
        this.email = email;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(final AppUser user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(final String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Updates the non-reference attributes of the instance. Also doesn't update the id.
     *
     * @param update Instance of the class with updates values.
     */
    public void updateUserInfo(final UserInfo update) {
        email = update.getEmail();
        name = update.getName();
        surname = update.getSurname();
    }
}
