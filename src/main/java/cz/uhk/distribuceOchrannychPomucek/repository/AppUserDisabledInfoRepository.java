package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.AppUserDisabledInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserDisabledInfoRepository extends JpaRepository<AppUserDisabledInfo, Integer> {
    AppUserDisabledInfoRepository findById(final int id);
}
