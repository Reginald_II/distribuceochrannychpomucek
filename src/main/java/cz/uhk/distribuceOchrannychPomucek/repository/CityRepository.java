package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {
    City findByPsc(final int psc);

    boolean existsByPsc(final int psc);

    City findByName(final String name);
}
