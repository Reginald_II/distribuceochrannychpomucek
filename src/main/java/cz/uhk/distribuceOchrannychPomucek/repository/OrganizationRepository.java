package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Integer> {
    Organization findById(final int id);

    List<Organization> findByRegion(final Region region);

    List<Organization> findAllByRegion(final Region region, final Sort sort);

    Page<Organization> findAllByRegion(final Region region, final Pageable pageable);

    Page<Organization> findAllByRegionAndCityNameContaining(final Region region, final String cityName, final Pageable pageable);

    Page<Organization> findAllByRegionAndNameContaining(final Region region, final String organizationName, final Pageable pageable);
}
