package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProtectiveEquipmentRepository extends JpaRepository<ProtectiveEquipment, Integer> {
    ProtectiveEquipment findById(final int id);

    List<ProtectiveEquipment> findAllByIdIsNotIn(final List<Integer> ids);

    List<ProtectiveEquipment> findAllByNameContaining(final String name);

    Page<ProtectiveEquipment> findAllByNameContaining(final String name, Pageable pageable);

    boolean existsByName(final String name);
}
