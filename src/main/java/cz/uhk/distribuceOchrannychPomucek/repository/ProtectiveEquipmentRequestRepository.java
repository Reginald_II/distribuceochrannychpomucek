package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentRequest;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProtectiveEquipmentRequestRepository extends JpaRepository<ProtectiveEquipmentRequest, Integer> {
    ProtectiveEquipmentRequest findById(final int id);

    List<ProtectiveEquipmentRequest> findByOrganization(final Organization organization);

    ProtectiveEquipmentRequest findByAppUser(final AppUser user);

    Page<ProtectiveEquipmentRequest> findAllByAppUser(final AppUser user, final Pageable pageable);

    Page<ProtectiveEquipmentRequest> findAllByAppUserAndProtectiveEquipmentRequestStatusId(final AppUser user, final int protectiveEquipmentRequestStatusId, final Pageable pageable);

    boolean existsByAppUserAndProtectiveEquipmentRequestStatusId(final AppUser user, final int protectiveEquipmentStatusId);

    Page<ProtectiveEquipmentRequest> findAllByOrganizationRegionAndProtectiveEquipmentRequestStatusIdGreaterThan(final Region region, final int protectiveEquipmentRequestStatusId, final Pageable pageable);

    Page<ProtectiveEquipmentRequest> findAllByOrganizationRegionAndProtectiveEquipmentRequestStatusId(final Region region, final int protectiveEquipmentRequestStatusId, final Pageable pageable);

    Page<ProtectiveEquipmentRequest> findAllByOrganization(final Organization organization, final Pageable pageable);

    Page<ProtectiveEquipmentRequest> findAllByOrganizationAndProtectiveEquipmentRequestStatusId(final Organization organization, final int protectiveEquipmentRequestStatusId, final Pageable pageable);
}
