package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentRequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProtectiveEquipmentRequestStatusRepository extends JpaRepository<ProtectiveEquipmentRequestStatus, Integer> {
    ProtectiveEquipmentRequestStatus findById(final int id);

    List<ProtectiveEquipmentRequestStatus> findByIdBetween(final int idMin, final int idMax);
}
