package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentStatusChangeEvent;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProtectiveEquipmentStatusChangeEventRepository extends JpaRepository<ProtectiveEquipmentStatusChangeEvent, Integer> {
    ProtectiveEquipmentStatusChangeEvent findById(final int id);

    Page<ProtectiveEquipmentStatusChangeEvent> findByRegion(final Region region, final Pageable pageable);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByRegion(final Region region, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByRegionAndTimeOfEventBefore(final Region region, final Date dateBefore, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByRegionAndTimeOfEventAfterAndTimeOfEventBefore(final Region region, final Date dateAfter, final Date dateBefore, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByRegionAndTimeOfEventAfter(final Region region, final Date dateAfter, final Sort sort);

    Page<ProtectiveEquipmentStatusChangeEvent> findByOrganization(final Organization organization, final Pageable pageable);

    Page<ProtectiveEquipmentStatusChangeEvent> findAllByRegionAndProtectiveEquipmentStatusChangeEventTypeId(final Region region, final int protectiveEquipmentStatusChangeEventTypeId, final Pageable pageable);

    List<ProtectiveEquipmentStatusChangeEvent> findByRegionAndProtectiveEquipmentStatusChangeEventTypeId(final Region region, final int protectiveEquipmentStatusChangeEventTypeId, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByRegionAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfterAndTimeOfEventBefore(final Region region, final int id, final Date dateAfter, final Date dateBefore, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByRegionAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfter(final Region region, final int id, final Date dateAfter, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByRegionAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventBefore(final Region region, final int id, final Date dateBefore, final Sort sort);

    Page<ProtectiveEquipmentStatusChangeEvent> findAllByOrganizationAndProtectiveEquipmentStatusChangeEventTypeId(final Organization organization, final int protectiveEquipmentStatusChangeEventTypeId, final Pageable pageable);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByOrganization(final Organization userOrganization, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByOrganizationAndTimeOfEventAfterAndTimeOfEventBefore(final Organization userOrganization, final Date dateAfter, final Date dateTo, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByOrganizationAndTimeOfEventBefore(final Organization userOrganization, final Date dateTo, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findAllByOrganizationAndTimeOfEventAfter(final Organization userOrganization, final Date dateAfter, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeId(final Organization userOrganization, final int protectiveEquipmentRequestStatusChangeEventTypeId, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfterAndTimeOfEventBefore(final Organization userOrganization, final int protectiveEquipmentRequestStatusChangeEventTypeId, final Date dateAfter, final Date dateTo, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventBefore(final Organization userOrganization, final int protectiveEquipmentRequestStatusChangeEventTypeId, final Date dateTo, final Sort sort);

    List<ProtectiveEquipmentStatusChangeEvent> findByOrganizationAndProtectiveEquipmentStatusChangeEventTypeIdAndTimeOfEventAfter(final Organization userOrganization, final int protectiveEquipmentRequestStatusChangeEventTypeId, final Date dateAfter, final Sort sort);
}
