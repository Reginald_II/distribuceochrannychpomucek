package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentStatusChangeEventType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProtectiveEquipmentStatusChangeEventTypeRepository extends JpaRepository<ProtectiveEquipmentStatusChangeEventType, Integer> {
    ProtectiveEquipmentStatusChangeEventType findById(final int id);
}
