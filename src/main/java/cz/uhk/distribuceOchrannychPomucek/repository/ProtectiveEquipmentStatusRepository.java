package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipment;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentStatus;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProtectiveEquipmentStatusRepository extends JpaRepository<ProtectiveEquipmentStatus, Integer> {
    boolean existsByProtectiveEquipmentId(final int id);

    List<ProtectiveEquipmentStatus> findByRegion(final Region region);

    Page<ProtectiveEquipmentStatus> findAllByRegion(final Region region, final Pageable pageable);

    Page<ProtectiveEquipmentStatus> findAllByRegionAndProtectiveEquipmentNameContaining(final Region region, final String protectiveEquipmentName, final Pageable pageable);

    int countAllByRegion(final Region region);

    int countAllByOrganization(final Organization organization);

    List<ProtectiveEquipmentStatus> findByOrganization(final Organization organization);

    ProtectiveEquipmentStatus findById(final int id);

    ProtectiveEquipmentStatus findByProtectiveEquipmentAndRegion(final ProtectiveEquipment protectiveEquipment, final Region region);

    ProtectiveEquipmentStatus findByProtectiveEquipmentAndOrganization(final ProtectiveEquipment protectiveEquipment, final Organization organization);

    Page<ProtectiveEquipmentStatus> findAllByOrganization(final Organization organization, final Pageable pageable);

    Page<ProtectiveEquipmentStatus> findAllByOrganizationAndProtectiveEquipmentNameContaining(Organization organization, String protectiveEquipmentName, final Pageable pageable);
}
