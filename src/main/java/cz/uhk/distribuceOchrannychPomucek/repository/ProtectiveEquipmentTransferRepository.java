package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentTransfer;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProtectiveEquipmentTransferRepository extends JpaRepository<ProtectiveEquipmentTransfer, Integer> {
    ProtectiveEquipmentTransfer findById(final int id);

    Page<ProtectiveEquipmentTransfer> findByRegion(final Region region, final Pageable pageable);

    Page<ProtectiveEquipmentTransfer> findByOrganization(final Organization organization, final Pageable pageable);

    Page<ProtectiveEquipmentTransfer> findByRegionAndProtectiveEquipmentTransferStatusId(final Region region, final int protectiveEquipmentTransferStatusId, final Pageable pageable);

    Page<ProtectiveEquipmentTransfer> findByOrganizationAndProtectiveEquipmentTransferStatusId(final Organization organization, int protectiveEquipmentTransferStatusId, final Pageable pageable);
}
