package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentTransferStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProtectiveEquipmentTransferStatusRepository extends JpaRepository<ProtectiveEquipmentTransferStatus, Integer> {
    ProtectiveEquipmentTransferStatus findById(final int id);
}
