package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentTransferType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProtectiveEquipmentTransferTypeRepository extends JpaRepository<ProtectiveEquipmentTransferType, Integer> {
    ProtectiveEquipmentTransferType findById(final int id);
}
