package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> {
    Region findById(final int id);

    Page<Region> findAllByNameContaining(final String name, final Pageable pageable);

    Page<Region> findAllByCityNameContaining(final String cityName, final Pageable pageable);
}
