package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipment;
import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentRequest;
import cz.uhk.distribuceOchrannychPomucek.model.RequestProtectiveEquipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestProtectiveEquipmentRepository extends JpaRepository<RequestProtectiveEquipment, Integer> {
    RequestProtectiveEquipment findById(final int id);

    List<RequestProtectiveEquipment> findByProtectiveEquipmentRequest(final ProtectiveEquipmentRequest protectiveEquipmentRequest);

    boolean existsByProtectiveEquipmentRequestAndProtectiveEquipment(final ProtectiveEquipmentRequest protectiveEquipmentRequest, final ProtectiveEquipment protectiveEquipment);

    boolean existsByProtectiveEquipmentRequest(final ProtectiveEquipmentRequest protectiveEquipmentRequest);

    RequestProtectiveEquipment findByProtectiveEquipmentRequestAndProtectiveEquipment(final ProtectiveEquipmentRequest protectiveEquipmentRequest, final ProtectiveEquipment protectiveEquipment);
}

