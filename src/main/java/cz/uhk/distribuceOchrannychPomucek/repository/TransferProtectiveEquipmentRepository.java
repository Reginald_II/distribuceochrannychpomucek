package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.ProtectiveEquipmentTransfer;
import cz.uhk.distribuceOchrannychPomucek.model.TransferProtectiveEquipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferProtectiveEquipmentRepository extends JpaRepository<TransferProtectiveEquipment, Integer> {
    TransferProtectiveEquipment findById(final int id);

    List<TransferProtectiveEquipment> findByProtectiveEquipmentTransfer(final ProtectiveEquipmentTransfer protectiveEquipmentTransfer);
}
