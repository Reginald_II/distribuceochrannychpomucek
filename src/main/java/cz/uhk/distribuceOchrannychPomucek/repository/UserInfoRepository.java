package cz.uhk.distribuceOchrannychPomucek.repository;

import cz.uhk.distribuceOchrannychPomucek.model.AppUser;
import cz.uhk.distribuceOchrannychPomucek.model.Organization;
import cz.uhk.distribuceOchrannychPomucek.model.Region;
import cz.uhk.distribuceOchrannychPomucek.model.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Integer> {
    UserInfo findByUser(final AppUser user);

    UserInfo findById(final int id);

    boolean existsByEmail(final String email);

    boolean existsByEmailAndIdNot(final String email, final int id);

    List<UserInfo> findByUserRegion(final Region region);

    List<UserInfo> findByUserOrganization(final Organization organization);

    Page<UserInfo> findAllByUserOrganization(final Organization organization, final Pageable pageable);

    Page<UserInfo> findByUserOrganizationAndNameContaining(final Organization organization, final String name, final Pageable pageable);

    Page<UserInfo> findByUserOrganizationAndSurnameContaining(final Organization organization, final String surname, final Pageable pageable);

    Page<UserInfo> findByUserOrganizationAndNameContainingAndSurnameContaining(final Organization organization, final String name, final String surname, final Pageable pageable);

    Page<UserInfo> findByNameContaining(final String name, final Pageable pageable);

    Page<UserInfo> findBySurnameContaining(final String name, final Pageable pageable);

    Page<UserInfo> findByNameContainingAndSurnameContaining(final String name, final String surname, final Pageable pageable);

    Page<UserInfo> findByUserRegion(final Region region, final Pageable pageable);

    Page<UserInfo> findByUserRegionAndNameContaining(final Region region, final String name, final Pageable pageable);

    Page<UserInfo> findByUserRegionAndSurnameContaining(final Region region, final String surname, final Pageable pageable);

    Page<UserInfo> findByUserRegionAndNameContainingAndSurnameContaining(final Region region, final String name, final String surname, final Pageable pageable);
}
