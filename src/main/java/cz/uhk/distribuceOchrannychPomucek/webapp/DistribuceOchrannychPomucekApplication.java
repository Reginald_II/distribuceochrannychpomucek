package cz.uhk.distribuceOchrannychPomucek.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("cz.uhk.distribuceOchrannychPomucek.controller")
@ComponentScan("cz.uhk.distribuceOchrannychPomucek.config")
@ComponentScan("cz.uhk.distribuceOchrannychPomucek.failureHandler")
@EntityScan("cz.uhk.distribuceOchrannychPomucek.model")
@EnableJpaRepositories("cz.uhk.distribuceOchrannychPomucek.repository")
public class DistribuceOchrannychPomucekApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistribuceOchrannychPomucekApplication.class, args);
    }

}
