$('#protectiveEquipmentAmount').change(function () {
    var oldValue = parseInt($('#oldAmount').text());
    var amountToChange = parseInt(this.value);
    var newValue = oldValue + amountToChange;
    if (isNaN(amountToChange) || isNaN(newValue)) {
        $('#newAmount').text(oldValue);
        return;
    }
    $('#newAmount').text(newValue);
});
$('#amountUsed').change(function () {
    var oldValue = parseInt($('#oldAmount').text());
    var amountToChange = parseInt(this.value);
    var newValue = oldValue - amountToChange;
    if (isNaN(amountToChange) || isNaN(newValue)) {
        $('#newAmount').text(oldValue);
        return;
    }
    $('#newAmount').text(newValue);
});

$('#approvedQuantity').change(function () {
    var oldValue = parseInt($('#oldAmount').text());
    var approvedQuantity = parseInt(this.value);
    var newRegionProtectiveEquipmentQuantity = oldValue - approvedQuantity;
    if (isNaN(approvedQuantity) || isNaN(newRegionProtectiveEquipmentQuantity)) {
        $('#remainingProtectiveEquipmentStatusQuantity').text(+oldValue);
        return;
    }
    $('#remainingProtectiveEquipmentStatusQuantity').text(newRegionProtectiveEquipmentQuantity);
});
