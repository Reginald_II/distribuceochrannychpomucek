$('#sendToOrganization').change(function () {
    if (this.checked) {
        $('#organizationChoice').show('slow');
    }
});
$('#ownUse').change(function () {
    if (this.checked) {
        $('#organizationChoice').hide('slow');
    }
});
$("#otherUse").change(function () {
    if (this.checked) {
        $('#organizationChoice').hide('slow');
    }
});